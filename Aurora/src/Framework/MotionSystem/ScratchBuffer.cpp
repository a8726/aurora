#include "aurora_pch.h"
#include "Framework/MotionSystem/ScratchBuffer.h"
#include "ozz/base/memory/allocator.h"

namespace ozz
{
	namespace sample
	{
		ScratchBuffer::ScratchBuffer() : buffer_(nullptr), size_(0) {}

		ScratchBuffer::~ScratchBuffer() {
			memory::default_allocator()->Deallocate(buffer_);
		}

		void* ScratchBuffer::Resize(size_t _size) {
			if (_size > size_) {
				size_ = _size;
				ozz::memory::default_allocator()->Deallocate(buffer_);
				buffer_ = ozz::memory::default_allocator()->Allocate(_size, 16);
			}
			return buffer_;
		}
	}
}