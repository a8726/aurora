#include "aurora_pch.h"

#include "Texture3D.h"

//#include "../src/Functions/TerrainMaker.h"

// we use glm::vec3 for 3d vectors (3x1)


namespace Aurora
{
    Texture3D::Texture3D()
    {
    }

    void Texture3D::bind()
    {
        glActiveTexture(GL_TEXTURE0 + m_type);
        glBindTexture(GL_TEXTURE_3D, m_ID);
    }

    void Texture3D::generate()
    {
        // unbind any current texture:
        glBindTexture(GL_TEXTURE_3D, 0);
        glGenTextures(1, &this->m_ID);

        glBindTexture(GL_TEXTURE_3D, this->m_ID);

        // target, level, internal format, dimensions, border, format, type, data
        // pass data as NULL
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
        glTexImage3D(GL_TEXTURE_3D, 0, GL_R32F, this->getWidth(), this->getHeight(), this->getDepth(), 0, GL_RED, GL_FLOAT, NULL);
        //glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA32F, this->getWidth(), this->getHeight(), this->getDepth(), 0, GL_RGBA, GL_FLOAT, NULL);
        

        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, this->m_wrapping);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, this->m_wrapping);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, this->m_wrapping);    // 3D texture also has R
                                                                                // not sure why tho
                                                                                // GLSL wiki says 4D texture coords usually use .stpq
                                                                                // which is almost STR but not
        // [Cloud Settings - Quality]
        // nearest runs faster
        // linear looks better
        //glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        //glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        if (!texGenerated) {
            // set texGenerated True
            this->texGenerated = true;
            // then load just bc idk what order these usually happen in
            this->load();
        }

        glBindTexture(GL_TEXTURE_2D, 0);
    }


    bool Texture3D::load(const std::string path)
    {
        // don't want to load image to 3D texture
        // but we do wanna use a comp shader which can exist at a path
        // but can't populate the texture unless it already exists, so:
        if (!texGenerated) return true;
        
        // start by prepping texture
        this->bind();
        // bind to image unit for writing
        glBindImageTexture(0, this->m_ID, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32F);
        //glBindImageTexture(0, this->m_ID, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);

        // use the handy comp shader loader I put in TerrainMaker
        GLuint texCompProgram = loadShader(m_texture_path.c_str());

        glUseProgram(texCompProgram);
        glDispatchCompute(this->m_width, this->m_height, this->m_depth);

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
        // wait until shader access is over

        return true;
    }

    void Texture3D::load()
    {
        if (!texGenerated) return;
        // load runs a compute shader which populates the texture
        this->load(m_texture_path.c_str());
        return;
    }

	/// SHADER LOADING
	// taken from my 3rd year final project
    // same as in Terrain.h
	GLuint Texture3D::loadShader(const char* path, bool safe) { // safe defaults to true
		GLuint shader = glCreateShader(GL_COMPUTE_SHADER);

		std::ifstream shader_file(path, std::ifstream::in);

		if (!shader_file.is_open()) {
			std::cerr << "File " << path << " failed to open \n";
			return 0;
		}
		// seek to end of file
		shader_file.seekg(0, shader_file.end);
		// report int position of end of file
		int fileLength = shader_file.tellg();
		// seek back to start
		shader_file.seekg(0, shader_file.beg);

		//read file to char array
		char* shaderSource = new char[fileLength + 1];
		shader_file.read(shaderSource, fileLength);
		shader_file.close();


		// ensure last char is null terminator
		shaderSource[fileLength] = '\0';

		//// NB: if getting errors with shader compilation 
		// (c0000 unexpected undefined at unkown) kinda thing 
		// change comp format to LF from CRLF

		// debug things
		GLint success = GL_FALSE;
		int logLength;

		// compiling shader
		glShaderSource(shader, 1, &shaderSource, NULL);
		glCompileShader(shader);

		//check shader
		if (safe) {
			glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
			if (!success) {
				glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
				char* shaderErrorLog = (char*)alloca(logLength * sizeof(char));
				glGetShaderInfoLog(shader, logLength, NULL, &shaderErrorLog[0]);
				printf("Error compiling %s \n%s \n", path, shaderErrorLog);
				exit(EXIT_FAILURE);
			}
		}

		// linking program
		GLuint program = glCreateProgram();
		glAttachShader(program, shader);
		glLinkProgram(program);

		// check program linking
		if (safe) {
			glGetProgramiv(program, GL_LINK_STATUS, &success);
			if (!success) {
				glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
				char* programErrorLog = (char*)alloca(logLength * sizeof(char));
				glGetProgramInfoLog(program, logLength, NULL, &programErrorLog[0]);
				printf("Error in program \n%s\n", programErrorLog);
				exit(EXIT_FAILURE);
			}
		}

		// shader exists as part of program, do not need to store it elsewhere
		glDeleteShader(shader);

		return program;
	}


    Texture3D::~Texture3D()
    {
        delete m_data;
    }
}
