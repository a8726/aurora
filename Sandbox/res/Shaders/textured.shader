#shader vertex
#version 450 core

layout(location = 0) in vec4 in_position;
layout(location = 1) in vec4 in_normal;
layout(location = 2) in vec4 in_tangent;
layout(location = 3) in vec2 in_texcoords;
layout(location = 4) in vec4 in_color;

out vec3 vertout_normal;
out vec3 vertout_fragment_position;
out vec2 vertout_texture_coords;

//uniform mat4 u_modelviewprojection; // MVP
uniform mat4 u_model;		// M
uniform mat4 u_view;		// V
uniform mat4 u_projection;	// P


void main()
{

    gl_Position = u_projection * u_view * u_model * vec4(in_position);
    gl_PointSize = 10.0;

    vertout_fragment_position = vec3(u_model * in_position);
    vertout_normal = mat3(transpose(inverse(u_model))) * vec3(in_normal.x, in_normal.y, in_normal.z);

    vertout_texture_coords = in_texcoords;

};




















#shader fragment
#version 450 core

layout(binding = 0, std140) uniform u_material
{
    vec4	u_ambient_col;
    vec4	u_diffuse_col;
    vec4	u_specular_col;
    vec4	u_emission_col;
    float 	u_specular_coef;
    float 	u_optical_density;
    float 	u_opacity;
    float 	u_illum;
};

layout(binding = 0) uniform sampler2D colortexture;

// output color per pixel (fragment):
layout(location = 0) out vec4 out_color;

in vec3 vertout_normal;
in vec3 vertout_fragment_position;
in vec2 vertout_texture_coords;

uniform vec3 u_light_position;
uniform vec3 u_camera_position;

#include "../Sandbox/res/Shaders/illumination.hlsli"

void main()
{

    float dist_from_camera = distance(u_camera_position, vertout_fragment_position);

    // Normal vector:
    vec3 N = normalize(vertout_normal.rgb);
    // Light direction (from current ray position)
    vec3 L = normalize(u_light_position - vertout_fragment_position);
    // Camera direction (from current ray position)
    vec3 C = normalize(u_camera_position - vertout_fragment_position);
    // add illumination to the colour:
    vec3 illuminated_color = add_local_illumination(
        /* diffuse color */ u_diffuse_col.rgb,
        /* ambient color */ u_ambient_col.rgb,
        /* specular color*/ u_specular_col.rgb,
        /* opacity       */ u_opacity,
        /* shine         */ u_specular_coef,
        /* normal, light direction, camera direction */ N, L, C,
        /* turn individual lights on                 */ u_specular_col.a, u_diffuse_col.a, u_ambient_col.a);

    out_color = vec4(illuminated_color, 1.f);
    // modulate computed illumination color with texture:
    out_color = out_color * vec4(texture(colortexture, vertout_texture_coords).rgba) + dist_from_camera/100.f * vec4(0.51f, 0.51f, 0.78f, 1.f);
    // to display just the surface normals as colours - comment out next line:
    //out_color = vec4(N/2.f+0.5f, 1.f);

};
