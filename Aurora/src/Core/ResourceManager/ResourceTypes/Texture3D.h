#pragma once

#include "Core/ResourceManager/Resource.h"
#include "Core/ResourceManager/ResourceTypes/Texture.h"
#include <iostream>
#include <fstream>
#include "../thirdparty/glm/glm.hpp"
#include "glad/glad.h"

namespace Aurora
{
	class Texture3D : public Texture
	{
	public:
		Texture3D();
		~Texture3D();

		// add get depth function
		unsigned int getDepth() const { return m_depth; }
		// OVERRIDE bind and generate
		void bind();// override;
		void generate();// override;

		unsigned m_ID = NULL; // different default m_ID so I can check it before generating
		unsigned m_type = AURORA_TEXTURE_TYPE_OTHER; // different default type

		void setWidth(int w) { m_width = w; }
		void setHeight(int h) { m_height = h; }
		void setDepth(int d) { m_depth = d; }
		void setAllDim(int d) { m_width = m_height = m_depth = d; }

		bool load(const std::string path) override;
		void load();// override;

	private:
		// texture path becomes a path to a comp shader which populates texture values
		std::string m_texture_path = "../Sandbox/res/Shaders/cloudnoise.comp";

		int m_depth;

		bool texGenerated = false;

		GLuint loadShader(const char* path, bool safe = true);

	};
}