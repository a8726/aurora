#include "aurora_pch.h"
#include "Cube.h"

Aurora::Cube::Cube()
{
}

Aurora::Cube::~Cube()
{
}

Aurora::Cube::Cube(PrimitiveObjectProperties properties)
{
    // Get vertex layout (what components the vertex holds ...
    // ... in addition to position (colour, normal, etc...)
    VertexBufferLayout layout = PrimitiveObject::setVertexBufferLayout();
    // Generate the Primitive surface itself:
    m_primitive_surface = this->generateSurface(properties);

    m_vertex_array = new VertexArray();
    // passing parameters: size, data
    m_vertex_buffer = new VertexBuffer(&m_primitive_surface.point_vector[0],
        m_primitive_surface.point_vector.size() * sizeof(float));

    // add the vertex buffer we obtain from the Surface points ...
    // ... and also the associated layout ...
    // ... to our VertexArray:
    m_vertex_array->addBuffer(*m_vertex_buffer, layout);

    // passing parameters: data, count
    m_index_buffer = new IndexBuffer(&m_primitive_surface.index_vector[0],
        m_primitive_surface.index_vector.size());
}

Surface Aurora::Cube::generateSurface(PrimitiveObjectProperties properties)
{
    float wall_offsets[6] = { -properties.radius*properties.scale_y, properties.radius*properties.scale_y,
                              -properties.radius*properties.scale_z, properties.radius*properties.scale_z,
                              -properties.radius*properties.scale_x, properties.radius*properties.scale_x };

    float wall_heights[6] = { properties.radius*properties.scale_z, properties.radius*properties.scale_z,
                              properties.radius*properties.scale_y, properties.radius*properties.scale_y,
                              properties.radius*properties.scale_z, properties.radius*properties.scale_z };

    float wall_widths[6] = { properties.radius*properties.scale_x, properties.radius*properties.scale_x,
                             properties.radius*properties.scale_x, properties.radius*properties.scale_x,
                             properties.radius*properties.scale_y, properties.radius*properties.scale_y };

    int wall_flat_indices[6] = {
        1, 1, 2, 2, 0, 0
    };

    std::vector<float>    point_vector;
    std::vector<unsigned> index_vector;

    int num_points = 0;
    int element_count = 0;
    int index_counter = 0;

    for (uint32_t i = 0; i < 6; i++)
    {
        Surface face = Plane::generatePlane(wall_offsets[i], 1, 1, //properties.segments_x, properties.segments_y,
            wall_widths[i], wall_heights[i], properties.tex_width, properties.tex_height, wall_flat_indices[i]);


        // concatenate vectors
        for (unsigned j = 0; j < face.point_vector.size(); j++)
        {
            point_vector.push_back(face.point_vector[j]);
        }
        for (unsigned j = 0; j < face.index_vector.size(); j++)
        {
            index_vector.push_back(num_points + face.index_vector[j]);
        }
        // raise triangle brush (glEnable(GL_PRIMITIVE_RESTART); set by glPrimitiveRestartIndex(65535); )
        index_vector.push_back(65535);

        num_points += face.num_points;
        element_count += face.num_elements;
        index_counter += face.num_triangles;
    }

    surface_data.point_vector = point_vector;
    surface_data.index_vector = index_vector;
    surface_data.num_points = num_points; // point_count;
    surface_data.num_elements = element_count;
    surface_data.num_triangles = index_counter;

    return surface_data;
}

