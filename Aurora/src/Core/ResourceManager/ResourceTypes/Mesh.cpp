#include "aurora_pch.h"

#include "Mesh.h"

// Third Party
#include <glad/glad.h>

// Custom
#include "Core/Renderer/VertexBufferLayout.h"
#include "Core/ResourceManager/ResourceTypes/MaterialProperties.h"

namespace Aurora
{

	Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, MaterialProperties materialProperties, bool isFbx)
	{
		// Initialise the mesh and sent it to the GPU
		this->m_Vertices = vertices;
		this->m_Indices = indices;
		this->m_Material = new Material(materialProperties);

		if (isFbx)
		{
			sendToGPU4FBX();
		}
		else
		{
			sendToGPU();
		}
	}

	Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, Material* material, bool isFbx)
	{
		// Initialise the mesh and sent it to the GPU
		this->m_Vertices = vertices;
		this->m_Indices = indices;
		this->m_Material = material;

		if (isFbx)
		{
			sendToGPU4FBX();
		}
		else
		{
			sendToGPU();
		}
	}

	void Mesh::render(Transform* transform)
	{
	}

	RenderParameters* Mesh::getRenderParameters()
	{
		RenderParameters *renderParameters = new RenderParameters(m_VertexArray, m_IndexBuffer, m_Material);
		return renderParameters;
	}

	Mesh::~Mesh()
	{

	}

	void Mesh::sendToGPU4FBX()
	{
		m_Material->setShader("./res/Shaders/fbx.shader");
		// Bind the shader
		m_Material->m_shader->bind();

		// Define the VBO layout. Only vertices and vertex normals for now.
		VertexBufferLayout layout;
		layout.push<float>(3);
		layout.push<float>(3);
		layout.push<float>(3);
		layout.push<float>(2);
		layout.push<float>(3);

		// Buffer the data
		m_VertexArray = new VertexArray();
		m_VertexBuffer = new VertexBuffer(&m_Vertices[0], m_Vertices.size() * 14 * sizeof(float));

		// Vertex Array
		m_VertexArray->addBuffer(*m_VertexBuffer, layout);

		// Element Array Buffer
		m_IndexBuffer = new IndexBuffer(&m_Indices[0], m_Indices.size());

		// Unbind shader, we are done
		m_Material->m_shader->unbind();
	}

	void Mesh::sendToGPU()
	{
		// Set up material
		bool diffuse, normal, specular;
		diffuse = normal = specular = false;
		for (auto& texture : m_Material->m_textures)
		{
			auto type = texture->m_type;
			switch (type)
			{
			case MaterialProperties::TEXTURE::DIFFUSE:
				diffuse = true;
				break;
			case MaterialProperties::TEXTURE::NORMAL:
				normal = true;
				break;
			case MaterialProperties::TEXTURE::SPECULAR:
				specular = true;
				break;
			default:
				break;
			}
		}
		if (normal && diffuse)
		{
			m_Material->setShader("./res/Shaders/textured_normal.shader");
		}
		else if (diffuse && !normal)
		{
			m_Material->setShader("./res/Shaders/textured.shader");
			//m_Material->setShader("./res/Shaders/texture_debug.shader");
		}
		else 
		{
			m_Material->setShader("./res/Shaders/default.shader");
		}

		
		// Bind the shader
		m_Material->m_shader->bind();

		// Define the VBO layout. Only vertices and vertex normals for now.
		VertexBufferLayout layout;
		layout.push<float>(3);
		layout.push<float>(3);
		layout.push<float>(3);
		layout.push<float>(2);
		layout.push<float>(3);

		// Buffer the data
		m_VertexArray = new VertexArray();
		m_VertexBuffer = new VertexBuffer(&m_Vertices[0], m_Vertices.size() * 14 * sizeof(float));

		// Vertex Array
		m_VertexArray->addBuffer(*m_VertexBuffer, layout);

		// Element Array Buffer
		m_IndexBuffer = new IndexBuffer(&m_Indices[0], m_Indices.size());

		// Unbind shader, we are done
		m_Material->m_shader->unbind();
	}

	void Mesh::updateBuffers()
	{
		m_VertexBuffer->update(&m_Vertices[0], m_Vertices.size() * 11 * sizeof(float));
	}

}