#pragma once

#include<string>
#include<vector>
#include "../thirdparty/glm/glm.hpp"
#include "Core/ResourceManager/ResourceTypes/Texture.h"

#include "Core/Renderer/openglMacros.h"

namespace Aurora
{

	//struct TextureStorage
	//{
	//	enum TYPE { DIFFUSE, NORMAL, SPECULAR, BUMP };
	//	TYPE	 type;
	//	Texture* data;
	//};

	struct MaterialProperties
	{
		//			 0		 1			  2			  3				4
		enum ASSET   { SHADER };
		enum TEXTURE { DIFFUSE, NORMAL, SPECULAR, BUMP, OTHER };

		struct AssetSource
		{
			ASSET		asset_type;
			std::string	asset_source;
		};

		struct TextureSource
		{
			TEXTURE		texture_type;
			std::string	texture_source;
		};

		std::string name = "";
		std::string matPath = "";
		glm::vec4	base_colour     = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
		glm::vec4	ambient_colour  = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
		glm::vec4	specular_colour = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
		glm::vec4	emission_colour    = glm::vec4(0.f, 0.f, 0.f, 1.f);
		float 		specular_coef = 500.f; // 0 to 900
		float		optical_density = 1.f;
		float 		opacity = 1.f;
		float 		illum = 1.f; // not used anywhere at the moment
		// example of a .mtl file:
		// ID value							# Explanation							# Blender 
		// Ns 0.000000						# specular exponent						Roughness [0->1 maps to 900->0]
		// Ka 1.000000 1.000000 1.000000	# ambient_albedo (ambient)				?
		// Kd 0.800000 0.022277 0.057274	# lambert_albedo (diffuse)				Base Color
		// Ks 1.000000 1.000000 1.000000	# glossy_albedo (specular)				Specular + (ignores Specular Tint)
		// Ke 0.0 0.0 0.0					# emission								Emission
		// Ni 0.000000						# optical density (index of refraction)	?
		// d 1.000000						# dissolve (0.0 glass-like 1.0 opaque)	Alpha
		// illum 2							# can ignore this actually (explanation http://paulbourke.net/dataformats/mtl/)	
		bool		is_dynamic_colour = false;
		unsigned	min_filter = GL_NEAREST; //0x2600; // GL_NEAREST
		unsigned	mag_filter = GL_NEAREST; //0x2600; // GL_NEAREST
		unsigned	texture_wrapping = 0x2901; //GL_REPEAT;
		float		tex_width = 1.f;
		float		tex_height = 1.f;
		std::vector<AssetSource>	  asset_sources = {
			{ASSET::SHADER, "./res/Shaders/basic.shader"},
		};
		std::vector<TextureSource>	  texture_sources = {
			{TEXTURE::DIFFUSE, "texdefault"},
		};

	};
}