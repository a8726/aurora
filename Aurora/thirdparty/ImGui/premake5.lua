project "ImGui"
    kind "StaticLib"
    language "C++"
    staticruntime "off"

    location "%{wks.location}/build"

    targetdir (targetOutputDir .. "/%{prj.name}")
	objdir (objOutputDir .. "/%{prj.name}")

    files
	{
        "imgui.h",
		"imgui.cpp",
        "imconfig.h",
        "imgui_demo.cpp",
		"imgui_draw.cpp",
		"imgui_internal.h",
        "imgui_tables.cpp",
		"imgui_widgets.cpp",
		"imstb_rectpack.h",
		"imstb_textedit.h",
		"imstb_truetype.h",
        "imgui_impl_opengl3.h",
        "imgui_impl_opengl3.cpp",
        "imgui_impl_opengl3_loader.h",
        "imgui_impl_glfw.h",
        "imgui_impl_glfw.cpp",

        "**.lua",
	}

    includedirs
    {
        thirdparty .. "/GLFW/include",
    }

    filter "system:windows"
		systemversion "latest"
		cppdialect "C++17"

    filter "configurations:Debug"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		runtime "Release"
		optimize "off"
