#pragma once

#include "HierarchyPanel.h"
#include "Framework/SampleECS/System/Global.h"

namespace Aurora
{

	class InspectorPanel
	{
	public:
		InspectorPanel() {};
		virtual ~InspectorPanel() = default;

		void draw(ImGuiContext* imguiContext, HierarchyPanel* hierarchy);
		void SwitchComponent(ComponentBase* comp);
	};
}
