#pragma once

// Third party
#include <assimp/scene.h>

// Custom
#include <Core/ResourceManager/Resource.h>
#include <Core/ResourceManager/ResourceTypes/Mesh.h>
#include "Framework/SampleECS/System/RenderSystem/RenderSystem.h"

namespace Aurora
{

    class Model :
        public Resource
    {
    public:
        Model() = default;
        // Overrides
        ~Model();

        bool load(const std::string path);

        void render(Transform* transform);

        std::vector<RenderParameters*> getRenderParameters();

        Model* clone() const;
    private:
        // Meshes
        std::vector<Mesh> m_Meshes;
        std::string m_Path;

        // Model processing routines
        void processNode(aiNode* node, const aiScene *scene);
        Mesh processMesh(aiMesh* mesh, const aiScene *scene);
    }; 

}

