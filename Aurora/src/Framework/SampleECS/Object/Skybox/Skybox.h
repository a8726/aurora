#include "Framework/SampleECS/Object/Object.h"
#include "Framework/SampleECS/Component/ComponentBase.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/Plane.h"
#include "../Core/ResourceManager/ResourceTypes/Texture.h"
#include "../Core/ResourceManager/ResourceTypes/Texture3D.h"

namespace Aurora
{
	class Skybox : public Object
	{
	public:
		Skybox();
			
		//property_vtable() - don't need vtable when using method 2

		//std::vector<PrimitiveObject*> m_walls;
		std::vector<Plane*> m_walls;
	private:
		virtual void render() override;

		virtual const char* getTypeDes() override
		{
			return "Skybox";
		}

		std::vector<Texture*> m_textures;
		//std::string m_texsource = "../Sandbox/res/Shaders/cloudnoise.comp";
		std::string m_texsource = "../Sandbox/res/Textures/Skybox/";

		float m_skybox_size = 150.f;

		 	float sunrise_duration = 3000.f;
			float sunrise_persistance = 2.f;
			float sunrise_vibrance = 100.f;
			float twilight_duration = 10.f;
			float horizon_cutoff_height = 0.1f;
			float atmosphere_scaling = 0.001f;
			float haziness = 100.f;
			float sunlight_strength = 2.f;
			float sundisk_scaling = 0.4f;
			float night_ambiance = 0.1f;
			float sundisk_pureness = 0.7f;
			float sunset_smoothness_offset = 0.5f;

	};

}