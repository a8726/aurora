// Minimal mesh class
// Meshes form part of a Model. Only Models are managed by the resource manager.

#pragma once

// Third Party
#include <glm.hpp>

// Custom
#include "Core/Renderer/Shader.h"
#include "Core/Renderer/IndexBuffer.h"
#include "Core/Renderer/VertexBuffer.h"
#include "Core/Renderer/VertexArray.h"
#include <Framework/SampleECS/Component/DefaultComponents/Transform.h>
#include "Core/ResourceManager/ResourceTypes/Texture.h"
#include "Core/ResourceManager/ResourceTypes/Material.h"
#include "Framework/SampleECS/System/RenderSystem/RenderSystem.h"

namespace Aurora
{

	struct Vertex
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec3 tangent;
		glm::vec2 texCoords;
		glm::vec3 color;
	};
}


namespace Aurora
{

	class Mesh
	{
	public:
		// Raw data
		std::vector<Vertex>			m_Vertices;
		std::vector<unsigned int>	m_Indices;
		Material*					m_Material;

		void* m_vertices_void;
		const uint16_t* m_indices_void;
		size_t m_vertices_size;
		size_t m_indices_size;

		Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, MaterialProperties materialProperties, bool isFbx = false);
		Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, Material* material, bool isFbx = false);

		// Put on render queue
		void render(Transform* transform);

		// Get render paramaters for making render components
		RenderParameters* getRenderParameters();

		// Updates the buffers on the GPU
		void updateBuffers();

		~Mesh();
	private:
		VertexArray* m_VertexArray;
		VertexBuffer* m_VertexBuffer;
		IndexBuffer* m_IndexBuffer;

		// Creates buffers on GPU
		void sendToGPU();
		void sendToGPU4FBX();
	};

}