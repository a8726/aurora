#include "aurora_pch.h"
#include "glad/glad.h"

#include "Camera.h"
#include "Framework/SampleECS/System/RenderSystem/RenderSystem.h"
#include <iostream>

namespace Aurora
{
    void Camera::OnEnable(Object* obj)
    {

    }

    void Camera::OnDisable(Object* obj)
    {
    }

    void Camera::OnStart(Object* obj)
    {
        transform = obj->GetComponent<Transform>(0);
    }

    void Camera::Update(Object* obj)
    {
        uint32_t width, height;
        RenderSystem::GetInstance().GetFrameBufferSize(&width, &height);
        Aspect = (float)width / (float)height;
    }

    void Camera::OnShutdown(Object* obj)
    {
    }

}