#include "aurora_pch.h"
#include "EventSystem.h"

namespace Aurora
{
	unsigned long long Event::eventCount = 1;

	uint64 EventManager::eventTypeManagerCount = 1;
	unsigned long long EventManager::RegisterEvent(Event* event)
	{
		events.insert({ event->getId(), event });
		return event->getId();
	}

	void EventManager::UnregisterEvent(uint64 id)
	{
		events.erase(id);
	}

	void EventManager::TriggerEvent(uint64 id)
	{
		if (events.count(id) > 0)
		{
			events.at(id)->invoke();
		}
	}

	void EventManager::TriggerEvent(uint64 id, EventResponseTypeBase* response)
	{
		if (events.count(id) > 0)
		{
			events.at(id)->invoke(response);
		}
	}

	void EventManager::BroadcastEvent()
	{
		for each (auto event in events)
		{
			event.second->invoke();
		}
	}

	void EventManager::BroadcastEvent(EventResponseTypeBase* response)
	{
		for each (auto event in events)
		{
			event.second->invoke(response);
		}
	}

	void EventSystem::init()
	{

	}

	uint64 EventSystem::RegisterEvent(uint64 eventType, Event* event)
	{
		EventManager* eventManager = GetInstance().getEventManager(eventType);

		if (eventManager != nullptr)
		{
			return eventManager->RegisterEvent(event);
		}

		eventManager = new EventManager(eventType);

		GetInstance().eventTypes.insert({ (eventType), eventManager });

		return eventManager->RegisterEvent(event);
	}

	void EventSystem::UnregisterEvent(uint64 eventType, uint64 id)
	{
		EventManager* eventManager = GetInstance().getEventManager(eventType);

		if (eventManager != nullptr)
		{
			eventManager->UnregisterEvent(id);
		}
	}

	void EventSystem::TriggerEvent(uint64 eventType, uint64 id)
	{
		EventManager* eventManager = GetInstance().getEventManager(eventType);

		if (eventManager != nullptr)
		{
			eventManager->TriggerEvent(id);
		}
	}

	void EventSystem::TriggerEvent(uint64 eventType, uint64 id, EventResponseTypeBase* response)
	{
		EventManager* eventManager = GetInstance().getEventManager(eventType);

		if (eventManager != nullptr)
		{
			eventManager->TriggerEvent(id, response);
		}
	}

	void EventSystem::BroadcastEvent(uint64 eventType)
	{
		EventManager* eventManager = GetInstance().getEventManager(eventType);

		if (eventManager != nullptr)
		{
			eventManager->BroadcastEvent();
		}
	}

	void EventSystem::BroadcastEvent(uint64 eventType, EventResponseTypeBase* response)
	{
		EventManager* eventManager = GetInstance().getEventManager(eventType);

		if (eventManager != nullptr)
		{
			eventManager->BroadcastEvent(response);
		}
	}

	std::string EventSystem::getTypeDes(uint64 eventType)
	{
		EventManager* eventManager = GetInstance().getEventManager(eventType);

		if (eventManager != nullptr)
		{
			return eventManager->getTypeDes();
		}

		return "[EventSystem]: nothing about " + eventType;
	}

	EventManager* EventSystem::getEventManager(uint64 eventType)
	{
		for each (auto var in eventTypes)
		{
			var.second;
		}

		if (eventTypes.count((eventType)) > 0)
		{
			return eventTypes.at((eventType));
		}

		return nullptr;
	}

}
