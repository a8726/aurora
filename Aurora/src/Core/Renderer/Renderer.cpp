#include "aurora_pch.h"

#include "Renderer.h"
#include "glad/glad.h"
#include <iostream>

#include <variant>

// for GL_FLAT
#define GL_GLEXT_PROTOTYPES

namespace Aurora
{
    /*  Reference:
     *  Title: Renderer
     *  Author: Studio Cherno
     *  Date: 2021
     *  Availability: https://github.com/TheCherno/Hazel/tree/master/Hazel/src/Hazel/Renderer
     *  Type: Tutorial/Notes
     *  Renderer System was based on and extended upon the Hazel Game Engine Renderer ...
     *  ... includes: Renderer, VertexArray, VertexBuffer, VertexBufferLayout, IndexBuffer and Shader
     */

    void GLClearError()
    {
        // Clear the OpenGL Error queue
        while (glGetError() != GL_NO_ERROR);
    }

    bool GLFunctionLog(const char* function, const char* file, int line)
    {
        /*
            When OpenGL fails and displays a blank black screen ...
            ... under the hood it gathers all the existing errors ...
            ... and puts it into an error queue.

            GlFunctionLog goes through the queue ...
            ... and displays each error ...
            ... using our Aurora logging system
        */

        while (GLenum error = glGetError())
        {
            std::stringstream ss;
            ss << "[OpenGL Error in " << file << ": " << function << "-ln" << line << "]\n";
            const std::string error_header = ss.str();
            ss << "(Error Code in decimal:" << error << "; Error Code in hex:" << std::hex << error << ")";
            const std::string error_text = ss.str();

            AURORA_LOG_ERROR(error_header);
            AURORA_LOG_ERROR(error_text);
            AURORA_LOG_ERROR("See Error Codes here: https://www.khronos.org/opengl/wiki/OpenGL_Error");
        
            // returns after one error ...
            // ... can tweak that here ...
            // ... if inconvenient
            return false;
        }

        // if no errors, just return true
        return true;
    }

    Renderer::Renderer()
        :m_drawing_mode(GL_TRIANGLES),
         m_RendererID(0),
         m_ColorAttachment(0),
         m_DepthAttachment(0),
         m_framebuffer_width(1920), 
         m_framebuffer_height(1080)
    {}


    void Renderer::clear() const
    {
        // clear the screen and depth buffer
        GLCall(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
        GLCall(glEnable(GL_DEPTH_TEST));
    }

    void Renderer::initFramebuffer()
    {

        if (m_RendererID)
        {
            glDeleteFramebuffers(1, &m_RendererID);
            glDeleteTextures(1, &m_ColorAttachment);
            glDeleteTextures(1, &m_DepthAttachment);

            glClear(GL_DEPTH_BUFFER_BIT);
        }


        // function to initialise the framebuffer ...
        // ... since we render to it

        glCreateFramebuffers(1, &m_RendererID);

        glBindFramebuffer(GL_FRAMEBUFFER, m_RendererID);

        glCreateTextures(GL_TEXTURE_2D, 1, &m_ColorAttachment);
        glBindTexture(GL_TEXTURE_2D, m_ColorAttachment);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, m_framebuffer_width, m_framebuffer_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_ColorAttachment, 0);

        glCreateTextures(GL_TEXTURE_2D, 1, &m_DepthAttachment);
        glBindTexture(GL_TEXTURE_2D, m_DepthAttachment);
        //glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH24_STENCIL8, m_framebuffer_width, m_framebuffer_height);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, m_framebuffer_width, m_framebuffer_height, 0,
     	    GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, m_DepthAttachment, 0);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            AURORA_LOG_DEBUG("Framebuffer is incomplete!");
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void Renderer::resizeFramebuffer(uint32_t new_width, uint32_t new_height)
    {
        this->m_framebuffer_width = new_width;
        this->m_framebuffer_height = new_height;
        initFramebuffer();

        // Finally, resize the actual OpenGL Viewport
        // (NOTE: we want to resize it to match the FRAMEBUFFER size, ...
        //  ... and NOT the entire new Window height/width, ...
        //  ... because our window contains panels and menus ...
        //  ... which block some of the Viewport!
        // In other words, using the glfwSetWindowSizeCallback ....
        // ... would not work as expected here
        glViewport(0, 0, this->m_framebuffer_width, this->m_framebuffer_height);

        AURORA_LOG_DEBUG("Viewport Resized to: " + std::to_string(this->m_framebuffer_width) + "x" + std::to_string(this->m_framebuffer_height));

    }

    void Renderer::setDrawingMode(int mode)
    {
        // accepts modes:
        // https://www.khronos.org/opengl/wiki/Primitive

        m_drawing_mode = mode;
    }

    void Renderer::processUniform(Uniform& uniform_wrapper, Shader& shader_instance)
    {
        // call corresponding setUniform depending on the type:
        std::visit(overloaded{
                [&shader_instance, uniform_wrapper](int       arg) { shader_instance.setUniform1i(uniform_wrapper.m_identifier, arg); },
                [&shader_instance, uniform_wrapper](float     arg) { shader_instance.setUniform1f(uniform_wrapper.m_identifier, arg); },
                [&shader_instance, uniform_wrapper](glm::mat4 arg) { shader_instance.setUniformMat4f(uniform_wrapper.m_identifier, arg); }
            }, uniform_wrapper.m_data);

    }

    //void Renderer::draw(const VertexArray& va, const IndexBuffer& ib, Shader& shader) const
    //void Renderer::draw(const VertexArray& va, const IndexBuffer& ib, Material& material) const
    void Renderer::draw(const VertexArray& va, const IndexBuffer& ib, Material& material, UniformStorage& us)
    {
        /*
            Renderer::draw
            returns void

            given vertex array, index buffer and associated shader ...
            ... binds everything and calls glDrawElements
        */

        //shader.bind();
        material.m_shader->bind();
        for (Uniform uniform_instance : us.m_uniforms)
        {
            processUniform(uniform_instance, *material.m_shader);
        }
        //for (auto uniform_instance : us.m_unif)
        //{
        //    processUniform(uniform_instance, *material.m_shader);
        //}
        //material.m_shader->setUniformMat4f("u_model", us.u_model);
        
        // bind vertex array:
        va.bind();
        // bind the index buffer:
        ib.bind();

        // pass the shader resolution info
        material.m_shader->setUniform3f("iResolution", this->m_framebuffer_width, this->m_framebuffer_height, 1.f);

        // Check which texture was bound last:
        //GLint whichID;
        //glGetIntegerv(GL_TEXTURE_BINDING_2D, &whichID);

        // Bind the textures needed for this object:
        unsigned active_textureID = 0;
        //while (!shader.m_textureQ.empty())
        //while (!material.m_shader->m_textureQ.empty())
        //for (int i = 0; i < 1; i++) //material.m_textures.size(); i++)

        //std::cout << material.m_textures.size() << " -> ";// << std::endl;

        // Unbind any previous texture:
        GLCall(glBindTexture(GL_TEXTURE_2D, 0));

        for (int i = 0; i < material.m_textures.size(); i++)
        {
            //unsigned textureID = material.m_textures[i]->m_ID; 

            // NOTE: The default Shader assumes that:
            // location 0 - diffuse texture (glActiveTexture(GL_TEXTURE0)
            // location 1 - normal texture  (glActiveTexture(GL_TEXTURE1)
            // ... more to be added

            material.m_textures[i]->bind();
        }

        //std::cout << std::endl;
        // bind the material data to the shader as one block
        material.m_shader->setUniformBlock("u_material", &material.m_data);

        GLCall(glPointSize(10));
        GLCall(glDisable(GL_CULL_FACE));
        //GLCall(glShadeModel(GL_SMOOTH));// GL_FLAT));
        //GLCall(glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX));
        glEnable(GL_PRIMITIVE_RESTART);
        glPrimitiveRestartIndex(65535);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        //GLCall(glEnable(GL_SCISSOR_TEST));

        GLCall(glDrawElements(/* mode                        */ m_drawing_mode,//GL_TRIANGLES,
                              /* number of indices to render */ ib.getCount(),
                              /* type                        */ GL_UNSIGNED_INT,
                              /* pointer to the index buffer */ nullptr));
    }
}
