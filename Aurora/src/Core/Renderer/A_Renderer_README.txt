-------------------------- The Renderer Class -------------------------- 

Provides interface to draw any object, given:
	- Vertex Array [const VertexArray& va]
	- Index Buffer [const IndexBuffer& ib]
	- Shader	   [const Shader& shader]

	use function Renderer::draw(const VertexArray& va,
								const IndexBuffer& ib,
								const Shader& shader)

	to initiate the draw call.

	Next section will summarise how to obtain ...
	... VertexArray and IndexBuffer ...
	... from arbitrary objects


In other words, Renderer connects Vertices (from Vertex Array) ...
... using the connectivity Index (from Index Buffer) ...
... and draws them following the rules of a specified Shader


------------------------ Individual Components -------------------------

The following code follows the Sphere Example

---------------- VertexArray, VertexBuffer, IndexBuffer ----------------

VertexArray contains VertexBuffers, and also have a Layout

So we first create VertexArray va;
then the VertexBuffer:
VertexBuffer vb(/* address of data */ &array_with_vertices, 
				/* size in bytes   */ array_with_vertices.size() * sizeof(float));

Then create the Layout:

	VertexBufferLayout layout;
	
	// layout can have: position, normals, color etc ...
	// which contain data in form:
	float x, float y, float z = ...
	... in this case <float>, and DIMENSION = 3

	if int x, int y = ...
	... then <int>, and DIMENSION = 2 ...
	... and so on

	So, if our vertices contain position and normal data ...
	... we need to push to layout twice:
	layout.push<float>(DIMENSION);
	layout.push<float>(DIMENSION);

	and then finally add the buffer with the layout ...
	... to the vertex array:
	va.addBuffer(vb, layout);



Now that we have the vertices, we need to tell how they are connected:
	IndexBuffer ib(/* data address */ &array_containing_indices, 
				   /* count        */ array_containing_indices.size());


-------------------------------- Shader ---------------------------------

To compile shader, we just initialize the Shader object:

Shader shader(path_to_shader);

Then, after setting the VertexArrays and IndexBuffers, bind the shader:
shader.bind();



And finally, if your shader has some uniforms (variables to pass to shader)
... set those uniforms as such:
basic_shader.setUniform4f("u_color", 0.3f, 0.3f, 0.8f, 1.0f);
NOTE this sets a vec4 RGBA variable called u_color inside the shader





------------------------------------ DRAWING -----------------------------------

When the Renderer object is defined Renderer renderer, ...
... call the draw command as such:

renderer->draw(vertex_array, index_buffer, shader);



