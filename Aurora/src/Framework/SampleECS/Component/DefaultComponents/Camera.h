#ifndef CAMERA_H
#define CAMERA_H

#include "../thirdparty/glm/glm.hpp"
#include "../thirdparty/glm/gtc/matrix_transform.hpp"
#include "Framework/SampleECS/Component/ComponentBase.h"
#include "Transform.h"
#include "Framework/SampleECS/Object/Object.h"

namespace Aurora
{

	// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
	enum Camera_Movement {
		FORWARD,
		BACKWARD,
		LEFT,
		RIGHT
	};

	// ------------------------- //
	//// Default camera values ////
	// ------------------------- //

    const float YAW = -90.0f;
    const float PITCH = 0.0f;
    const float SPEED = 0.5f;
    const float SENSITIVITY = 0.1f;
    const float ZOOM = 45.0f;
    const float FOV = 45.0;
    const float ASPECTRATIO = 4 / 3;
    const float ZNEAR = 0.1;
    const float ZFAR = 1000;

	// Camera class that stores camera properties and calcuates matrices based on input and updates itself
	class Camera : public ComponentBase
	{
	private:
		// ---------------- //
		//// Data Members ////
		// ---------------- //

		// Camera Attributes
		DEFINE_PROPERTIES
		(
			// Camera options
			(float) Zoom,             // Zoom z

			// Projection properties
			(float) Aspect,           // Aspect ratio of the screen
			(float) Fov,              // Field of view
			(float) ZNear,            // Near clipping plane
			(float) ZFar              // Far clipping plane
		)

	public:
		// ---------------- //
		//// Constructors ////
		// ---------------- //

		// Constructor with vectors
		Camera(float fov = FOV, float znear = ZNEAR, float zfar = ZFAR)
			: Zoom(ZOOM)
		{
			Fov = fov;
			ZNear = znear;
			ZFar = zfar;
		}

		~Camera()
		{
		}

		// -------------------------- //
		//// Get matrices functions ////
		// -------------------------- //

		// Returns the view matrix calculated using Euler Angles and the LookAt Matrix
		inline glm::mat4 GetViewMatrix() { return glm::lookAt(transform->Position(), transform->Position() + transform->Front(), transform->Up()); }

		// Returns the projection matrix using the field of view, aspect ration, and clipping planes using perspective matrix
		inline glm::mat4 GetProjectionMatrix() { return glm::perspective(glm::radians(Fov), Aspect, ZNear, ZFar); }

		// --------------------------------------------------------- //
		//// Getter functions for reading stored camera properties ////
		// --------------------------------------------------------- //

		// Get the position of the camera
		inline glm::vec3 GetPosition() { return transform->Position(); }

		// Get the zoom of the camera
		inline float GetZoom() { return Zoom; }

		// Get the field of view of the camera
		inline float GetFov() { return Fov; }

		// Get the aspect ratio of the camera
		inline float GetAspect() { return Aspect; }

		// Get the near clipping plane of the camera 
		inline float GetNear() { return ZNear; }

		// Get the far clipping plane of the camera
		inline float GetFar() { return ZFar; }

		virtual std::string getTypeDes() override
		{
			return "Camera";
		}

	private:

		// --------------------------------------------------------- //
		//// Internal functions to update camera vectors on change ////
		// --------------------------------------------------------- //

		Transform* transform;
		virtual void OnEnable(Object* obj) override;
		virtual void OnDisable(Object* obj) override;
		virtual void OnStart(Object* obj) override;
		virtual void Update(Object* obj) override;
		virtual void OnShutdown(Object* obj) override;
	};

}
#endif