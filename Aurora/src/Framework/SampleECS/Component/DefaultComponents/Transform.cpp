#include "aurora_pch.h"
#include "Transform.h"

#include "glm.hpp"

namespace Aurora
{
	glm::mat4* Transform::ModelMat()
	{
		auto trans = glm::mat4x4(scale.x, 0, 0, 0,
			0, scale.y, 0, 0,
			0, 0, scale.z, 0,
			position.x, position.y, position.z, 1);

		trans = glm::rotate(trans, glm::radians(rotation.y), glm::vec3(0.0f, 1.0f, 0));
		trans = glm::rotate(trans, glm::radians(rotation.x), glm::vec3(1.0f, 0, 0));
		trans = glm::rotate(trans, glm::radians(rotation.z), glm::vec3(0.0f, 0, 1.0f));

		return &trans;
	}

	void Transform::OnEnable(Object* obj)
	{
	}
	void Transform::OnDisable(Object* obj)
	{
	}
	void Transform::OnStart(Object* obj)
	{
	}

	void Transform::Update(Object* obj)
	{
		auto trans = glm::mat4x4(1.0f);
		trans = glm::rotate(trans, glm::radians(rotation.y), glm::vec3(0.0f, 1.0f, 0));
		trans = glm::rotate(trans, glm::radians(rotation.x), glm::vec3(1.0f, 0, 0));
		trans = glm::rotate(trans, glm::radians(rotation.z), glm::vec3(0.0f, 0, 1.0f));

		up = trans * glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
		front = trans * glm::vec4(0.0f, 0.0f, -1.0f, 1.0f);
		right = trans * glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	}

	void Transform::OnShutdown(Object* obj)
	{
	}
}