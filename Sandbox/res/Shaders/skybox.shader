#shader vertex
#version 450 core

layout(location = 0) in vec4 in_position;
layout(location = 1) in vec4 in_normal;
layout(location = 2) in vec4 in_tangent;
layout(location = 3) in vec2 in_texcoords;

out vec3 vertout_normal;
out vec3 vertout_fragment_position;
out vec2 vertout_texture_coords;

//uniform mat4 u_modelviewprojection; // MVP
uniform mat4 u_model;		// M
uniform mat4 u_view;		// V
uniform mat4 u_projection;	// P


void main()
{
    mat4 skybox_transform = u_view;
    skybox_transform[3] = vec4(0.0, 0.0, 0.0, 1.0);

    gl_Position = u_projection * skybox_transform * u_model * vec4(in_position);
    gl_PointSize = 10.0;

    vertout_fragment_position = vec3(u_model * in_position);
    vertout_texture_coords = in_texcoords;
};




















#shader fragment
#version 450 core

// use only (6) diffuse textures for each plane
layout(binding = 0) uniform sampler2D colortexture;

layout(location = 0) out vec4 out_color;

in vec3 vertout_fragment_position;
in vec2 vertout_texture_coords;

uniform vec3 u_light_position;
uniform vec3 u_camera_position;


void main()
{
    out_color = vec4(texture(colortexture, vertout_texture_coords).rgba);
};
