#pragma once

namespace Aurora
{
	class HierarchyPanel
	{
	public:
		HierarchyPanel() {};
		virtual ~HierarchyPanel() = default;

		void draw(ImGuiContext* imguiContext);

		static unsigned int selectedId;

	};
}

