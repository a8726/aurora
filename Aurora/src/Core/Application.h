/*runtime application*/

#pragma once

#include "Window.h"
#include "GLFW/glfw3.h"
#include "Framework/InputSystem/InputSystem.h"

namespace Aurora
{

	class Application
	{
	public:
		Application();
		virtual ~Application() = default;

	protected:
		Window* m_window;

	};

	Application* CreateApplication();
	static Aurora::Application* app;
}
