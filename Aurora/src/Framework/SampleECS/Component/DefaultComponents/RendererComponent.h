#pragma once

#include "Framework/SampleECS/Component/ComponentBase.h"
#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"
#include "../thirdparty/glm/glm.hpp"
#include "Core/ResourceManager/ResourceTypes/Material.h"
#include "Core/Renderer/IndexBuffer.h"
#include "Core/Renderer/VertexBuffer.h"
#include "Core/Renderer/VertexArray.h"
#include "Framework/SampleECS/System/RenderSystem/RenderQueue.h"
#include "Core/StaticReflection.h"

namespace Aurora
{
	class RendererComponent : public ComponentBase
	{
	public:
		RendererComponent();
		RendererComponent(VertexArray* aVertexArray, IndexBuffer* aIndexBuffer, Material* aMaterial);
		RendererComponent(RenderParameters* renderParameters);

		void SetParameters(RenderParameters* renderParameters);
		void SetParameters(VertexArray* aVertexArray, IndexBuffer* aIndexBuffer, Material* aMaterial);

		virtual std::string getTypeDes() override
		{
			return "RendererComponent";
		}

		DEFINE_PROPERTIES(
			(glm::vec3) light_pos
		)

	private:
		bool isInit = false;
		VertexArray* m_VertexArray;
		IndexBuffer* m_IndexBuffer;
		Material* m_Material;
		Transform* m_transform;

		// Each RenderComponent will have its local UniformStorage ...
		// ... which will be used for unique Uniforms of each Shader:
		UniformStorage m_UniStore;
		RenderParameters m_RenderParams;

		virtual void OnEnable(Object* obj) override;
		virtual void OnDisable(Object* obj) override;
		virtual void OnStart(Object* obj) override;
		virtual void Update(Object* obj) override;
		virtual void OnShutdown(Object* obj) override;
	};
}

