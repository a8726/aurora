#include "aurora_pch.h"
#include "RenderQueue.h"

namespace Aurora
{

	void RenderQueue::push(RenderParameters* renderParas)
	{
		if (std::strcmp(renderParas->material->m_shader->getFilename(), "../Sandbox/res/Shaders/water.shader") == 0)
		{
			paras.emplace_front(renderParas);
		}
		else paras.emplace_back(renderParas);
	}

	void RenderQueue::pop()
	{
		//paras.pop_front();
		paras.pop_back();
	}

	RenderParameters* RenderQueue::front()
	{
		//if (strcmp(paras.front()->material->m_shader.getFilename() == "../Sandbox/res/Shaders/water.shader")
		//if (std::strcmp(paras.front()->material->m_shader->getFilename(), "../Sandbox/res/Shaders/water.shader") == 0)
		//{
		//	if (paras.size() ==
		//}
		return paras.front();
	}

	RenderParameters* RenderQueue::back()
	{
		return paras.back();
	}

	void RenderQueue::clear()
	{
		paras.clear();
	}

	bool RenderQueue::isEmpty()
	{
		return paras.empty();
	}

}
