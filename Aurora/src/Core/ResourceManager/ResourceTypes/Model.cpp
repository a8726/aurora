// PCH
#include <aurora_pch.h>

// Third Party
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

// Custom
#include "Model.h"
#include <Core/Log.h>
#include <Framework/SampleECS/Component/DefaultComponents/Transform.h>
#include "Core/ResourceManager/ResourceTypes/MaterialProperties.h"

#include "Core/Renderer/openglMacros.h"

namespace Aurora
{
	Model::~Model()
	{
		// TODO: GPU Cleanup
		return;
	}

	Model* Model::clone() const
	{
		return new Model(*this);
	}

	void Model::render(Transform* transform)
	{
		for (auto& mesh : m_Meshes)
		{
			mesh.render(transform);
		}
	}

	std::vector<RenderParameters*> Model::getRenderParameters()
	{
		std::vector<RenderParameters*> renderParamaters;
		renderParamaters.reserve(m_Meshes.size());
		for (auto& mesh : m_Meshes)
		{
			renderParamaters.push_back(mesh.getRenderParameters());
		}
		return renderParamaters;
	}

	void Model::processNode(aiNode* node, const aiScene* scene)
	{
		// Process all the m_Meshes
		for (size_t i = 0; i < node->mNumMeshes; i++)
		{
			aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
			this->m_Meshes.push_back(processMesh(mesh, scene));
		}
		// Process children
		for (size_t i = 0; i < node->mNumChildren; i++)
		{
			processNode(node->mChildren[i], scene);
		}
	}

	std::vector<std::string> getTextureNames(aiMaterial* material, aiTextureType type)
	{
		std::vector<std::string> textureNames;

		for (size_t i = 0; i < material->GetTextureCount(type); i++)
		{
			aiString name;
			material->GetTexture(type, i, &name);
			textureNames.push_back(name.C_Str());
		}

		return textureNames;
	}

	Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene)
	{
		std::vector<Vertex> vertices;
		std::vector<unsigned int> indices;

		// Process vertices
		for (size_t i = 0; i < mesh->mNumVertices; i++)
		{
			// Tempoary vertex as ASSIMP has own structures
			Vertex vertex;
			glm::vec3 tempVector;

			// Positions
			tempVector.x = mesh->mVertices[i].x;
			tempVector.y = mesh->mVertices[i].y;
			tempVector.z = mesh->mVertices[i].z;

			vertex.position = tempVector;

			// Normals- these were generated if not present
			tempVector.x = mesh->mNormals[i].x;
			tempVector.y = mesh->mNormals[i].y;
			tempVector.z = mesh->mNormals[i].z;

			vertex.normal = tempVector;

			// Texture co-ords, if they exist
			if (mesh->mTextureCoords[0])
			{
				glm::vec2 texCoords;
				texCoords.x = mesh->mTextureCoords[0][i].x;
				texCoords.y = mesh->mTextureCoords[0][i].y;
				vertex.texCoords = texCoords;
			}

			vertex.color = glm::vec3(1.0f);

			vertices.push_back(vertex);
		}

		// Process indices
		for (size_t i = 0; i < mesh->mNumFaces; i++)
		{
			aiFace face = mesh->mFaces[i];
			for (size_t j = 0; j < face.mNumIndices; j++)
			{
				indices.push_back(face.mIndices[j]);
			}
		}

		MaterialProperties materialProperties;
		materialProperties.matPath = m_Path;

		// Process materials if they exist
		if (mesh->mMaterialIndex >= 0)
		{
			// Loading from ASSIMP Structures
			aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
			aiString name;
			material->Get(AI_MATKEY_NAME, name);
			//			base_colour		ambient_colour		specular_colour		emission_colour
			aiColor3D	diffuseColor,	ambientColor,		specularColor,		emissionColor;
			//		specular_coef	optical_density		opacity
			float	shininess,		refracti,			opacity;
			//	illum
			int	shadingModel;
			// Textures
			std::vector<std::string> diffuseMaps, normalMaps, specularMaps, bumpMaps;

			// Load the values from ASSIMP structures
			material->Get(AI_MATKEY_COLOR_DIFFUSE, diffuseColor);
			material->Get(AI_MATKEY_COLOR_AMBIENT, ambientColor);
			material->Get(AI_MATKEY_COLOR_SPECULAR, specularColor);
			material->Get(AI_MATKEY_COLOR_EMISSIVE, emissionColor);
			material->Get(AI_MATKEY_SHININESS, shininess);
			material->Get(AI_MATKEY_REFRACTI, refracti);
			material->Get(AI_MATKEY_OPACITY, opacity);
			material->Get(AI_MATKEY_SHADING_MODEL, shadingModel);

			diffuseMaps = getTextureNames(material, aiTextureType_DIFFUSE);
			normalMaps = getTextureNames(material, aiTextureType_NORMALS);
			specularMaps = getTextureNames(material, aiTextureType_SPECULAR);
			bumpMaps = getTextureNames(material, aiTextureType_HEIGHT);

			// Texture properties are per texture in assimp. Options:
			// 1) Assume the same texture sizes and modes for all
			// 2) Refactor texture to contain the texture properties currently
			//		contained in MaterialProperties.h and load per texture (preferred.)

			// Convert to MaterialProperties
			materialProperties.name = name.C_Str();
			materialProperties.base_colour = glm::vec4(diffuseColor.r, diffuseColor.g, diffuseColor.b, 1.0f);
			materialProperties.ambient_colour = glm::vec4(ambientColor.r, ambientColor.g, ambientColor.b, 1.0f);
			materialProperties.specular_colour = glm::vec4(specularColor.r, specularColor.g, specularColor.b, 1.0f);
			materialProperties.emission_colour = glm::vec4(emissionColor.r, emissionColor.g,	emissionColor.b, 1.0f);
			materialProperties.specular_coef = shininess;
			materialProperties.optical_density = refracti;
			materialProperties.opacity = opacity;
			materialProperties.illum = shadingModel;

			// [Texture Filter] Let Loaded Objects have Linear Texture Filtering and Mipmapping:
			materialProperties.min_filter = GL_LINEAR_MIPMAP_LINEAR;
			materialProperties.mag_filter = GL_LINEAR;



			std::vector<Aurora::MaterialProperties::TextureSource> textureSources;
			for (auto& diffuseMap : diffuseMaps)
			{
				textureSources.push_back({ MaterialProperties::TEXTURE::DIFFUSE, diffuseMap });
			}
			for (auto& normalMap : normalMaps)
			{
				textureSources.push_back({ MaterialProperties::TEXTURE::NORMAL, normalMap });
			}
			for (auto& specularMap : specularMaps)
			{
				textureSources.push_back({ MaterialProperties::TEXTURE::SPECULAR, specularMap });
			}
			for (auto& bumpMap : bumpMaps)
			{
				// Treating bump maps as normal maps to be compatible with more files
				// Bump maps generally aren't used anymore, but many exporters such as blender refer to normal maps as bump maps
				textureSources.push_back({ MaterialProperties::TEXTURE::NORMAL, bumpMap });
			}

			materialProperties.texture_sources = textureSources;
			// TODO: Set default shader based on illum

		}

		return Mesh(vertices, indices, materialProperties);
	}

	bool Model::load(const std::string path)
	{
		m_Path = path;

		Assimp::Importer importer;
		const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenNormals | aiProcess_CalcTangentSpace);

		// Check for load success
		if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
		{
			std::string error = std::string("Model loading error: ERROR::ASSIMP::") + importer.GetErrorString();
			AURORA_LOG_ERROR(error);
			return false;
		}

		auto mats = scene->mMaterials;

		processNode(scene->mRootNode, scene);

		return true;
	}

	

}