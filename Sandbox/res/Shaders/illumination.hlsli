
float clamp_dot(vec3 vector1, vec3 vector2)
{
    return max(dot(vector1, vector2), 0.f);
}

vec3 diffuse_contribution(float clampdot_nl, vec3 object_diffuse_col, vec3 light_colour)
{
    return clampdot_nl * object_diffuse_col / 3.14159265f * light_colour;
}

vec3 add_local_illumination(
    vec3 object_diffuse_col, vec3 object_ambient_col,
    vec3 object_specular_col, float opacity, float shine,
    vec3 normal, vec3 light_dir, vec3 camera_dir,
    float specular_on, float diffuse_on, float ambient_on)
{
    vec3 ambient = vec3(0.0, 0.0, 0.0);
    vec3 diffuse = vec3(0.0, 0.0, 0.0);
    vec3 specular = vec3(0.0, 0.0, 0.0);

    vec3 light_colour = vec3(1.0, 1.0, 1.0);
    vec3 halfway_vector = (camera_dir + light_dir) / length(camera_dir + light_dir);

    // AMBIENT LIGHT CONTRIBUTION:
    // (constant colour, does not depend on number of lights)
    ambient += object_ambient_col * object_diffuse_col;

    // DIFFUSE LIGHT CONTRIBUTION
    diffuse += diffuse_contribution(clamp_dot(normal, light_dir), object_diffuse_col, light_colour);

    // SPECULAR LIGHT CONTRIBUTION:
    float ndothap = pow(max(dot(normal, halfway_vector), 0.0), shine);
    //specular += (shine + 2.f) / 8.f * ndothap * clamp_dot(normal, light_dir) * object_specular_col * light_colour;

    //float spec = pow(max(dot(camera_dir, direction_to_reflected_ray), 0.0), shine);
    // (multiply by 10.f to make the specular highlights more visible)
    specular = ndothap * object_specular_col * light_colour;

    // SUM ALL CONTRIBUTIONS:
    return ambient_on * ambient + diffuse_on * diffuse + specular_on * specular;
}