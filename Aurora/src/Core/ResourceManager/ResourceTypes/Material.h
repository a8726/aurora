#pragma once
#include<string>
#include<vector>
#include "../thirdparty/glm/glm.hpp"
#include "Core/Renderer/Shader.h"
#include "MaterialProperties.h"
#include "Core/ResourceManager/ResourceTypes/Texture.h"

#include "Core/Renderer/openglMacros.h"

namespace Aurora
{
	struct MaterialData
	{// Separate Struct containing ...
	 // ...values to be passed to shader 
		glm::vec4		m_ambient_col = glm::vec4(0.5f, 0.5f, 0.5f, 1.f);  // glm::vec4(51.f, 51.f, 51.f, 255.f);
		glm::vec4		m_diffuse_col = glm::vec4(0.5f, 0.5f, 0.5f, 1.f);
		glm::vec4		m_specular_col = glm::vec4(0.5f, 0.5f, 0.5f, 0.f);
		glm::vec4		m_emission_col = glm::vec4(0.f, 0.f, 0.f, 1.f);
		float 			m_specular_coef = 500.f;
		float 			m_optical_density = 1.f;
		float 			m_opacity = 1.f;
		float 			m_illum = 1.f;
	};

	class Material : public Resource
	{
	public:
		bool load(const std::string path) override;
		Material* clone() const override;
		
		Material();
		Material(MaterialProperties initproperties);
		~Material() override;

		// Since Material, and NOT the object itself is passed to rendering ...
		// ... we keep track of rendering mode here:
		// default to GL_TRIANGLES, since .obj files use that
		int	render_mode = GL_TRIANGLES;

		// 'None' default material
		Material(std::string g_name)
		{
			m_name = g_name;
			// m_texture = RGBAImage();
		}
		//				 0	 1	 2	 3	 4	 5	 6	 7
		enum COMPONENT { Ns, Ka, Kd, Ks, Ke, Ni, d,  illum };

		std::string		m_name = ""; // unique string ID
		std::string		m_matPath = "";
		// components - if unchanged, remain default as such:
		std::vector<Texture*> m_textures;
		Shader*			m_shader;
		MaterialData	m_data;

		void setShader(std::string path)
		{
			delete m_shader;
			m_shader = new Shader(path);
		}

		void setTextureMinFilter(unsigned filter)
		{
			for (uint32_t i = 0; i < m_textures.size(); i++)
			{
				m_textures[i]->setMinFilter(filter);
			}
		}

		void setTextureMagFilter(unsigned filter)
		{
			for (uint32_t i = 0; i < m_textures.size(); i++)
			{
				m_textures[i]->setMagFilter(filter);
			}
		}

		void update(COMPONENT comp, float value)
		{
			switch (comp)
			{
			case Ns:
			{
				m_data.m_specular_coef = value;
				break;
			}
			case Ni:
			{
				m_data.m_optical_density = value;
				break;
			}
			case d:
			{
				m_data.m_opacity = value;
				break;
			}
			case illum:
			{
				m_data.m_illum = value;
				break;
			}
			default:
				break;
			}
		}

		void update(COMPONENT comp, glm::vec4 value)
		{
			switch (comp)
			{
			case Ka:
			{
				m_data.m_ambient_col = value;
				break;
			}
			case Kd:
			{
				m_data.m_diffuse_col = value;
				break;
			}
			case Ks:
			{
				m_data.m_specular_col = value;
				break;
			}
			case Ke:
			{
				m_data.m_emission_col = value;
				break;
			}
			default:
				break;
			}
		}

	};
}

