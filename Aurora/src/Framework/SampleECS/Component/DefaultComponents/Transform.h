#pragma once

#include "Framework/SampleECS/Component/ComponentBase.h"
#include "../thirdparty/glm/glm.hpp"
#include "Core/StaticReflection.h"

namespace Aurora
{
	class Transform : public ComponentBase
	{
	public:
		Transform(glm::vec3 _position = glm::vec3(0.0f, 0.0f, 0.0f),
			glm::vec3 _rotation = glm::vec3(0.0f, 0.0f, 0.0f),
			glm::vec3 _scale = glm::vec3(1.0f, 1.0f, 1.0f),
			glm::vec3 _front = glm::vec3(0.0f, 0.0f, -1.0f),
			glm::vec3 _up = glm::vec3(0.0f, 1.0f, 0.0f),
			glm::vec3 _right = glm::vec3(1.0f, 0.0f, 0.0f)) :
		position(_position), 
		rotation(_rotation),
		scale(_scale),
		front(_front),
		up(_up),
		right(_right) {};

		inline glm::vec3& Position() { return position; };
		inline glm::vec3& Rotation() { return rotation; };
		inline glm::vec3& Scale() { return scale; };

		inline glm::vec3& Front() { return front; };
		inline glm::vec3& Up() { return up; }
		inline glm::vec3& Right() { return right; };

		glm::mat4* ModelMat();

		virtual std::string getTypeDes() override
		{
			return "Transform";
		}

		DEFINE_PROPERTIES
		(
			(glm::vec3) position,
			(glm::vec3) rotation,
			(glm::vec3) scale,
			(glm::vec3) front,
			(glm::vec3) up,
			(glm::vec3) right
		)

	public:
		virtual void OnEnable(Object* obj) override;
		virtual void OnDisable(Object* obj) override;
		virtual void OnStart(Object* obj) override;
		virtual void Update(Object* obj) override;
		virtual void OnShutdown(Object* obj) override;
	};
}

