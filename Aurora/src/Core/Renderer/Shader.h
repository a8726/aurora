#pragma once
#include <string>
#include <unordered_map>

#include "../thirdparty/glm/glm.hpp"

//#include "../thirdparty/Eigen/Eigen/Dense.h"
namespace Aurora
{
	/*  Reference:
	 *  Title: Renderer
	 *  Author: Studio Cherno
	 *  Date: 2021
	 *  Availability: https://github.com/TheCherno/Hazel/tree/master/Hazel/src/Hazel/Renderer
	 *  Type: Tutorial/Notes
	 *  Renderer System was based on and extended upon the Hazel Game Engine Renderer ...
	 *  ... includes: Renderer, VertexArray, VertexBuffer, VertexBufferLayout, IndexBuffer and Shader
	 */

	struct ShaderProgramSource
	{	
		/*
			COMBINED Shader type requires vertex and fragment 
			Struct for reading in a conventional Shader Program
			This is a simple Vertex-Fragment Shader combination 
			(as one file)
		*/
		std::string VertexSource;
		std::string FragmentSource;
	};

	struct ShaderUniformLayout
	{
		enum TYPE { FLOAT, VEC3, VEC4, MAT4 };
		unsigned	ID;
		//TYPE		type;
		std::string type;
		std::string name;
	};

	class Shader	
	{
	public:

		enum TYPE {COMBINED, VERTEX, FRAGMENT, COMPUTE};
		//enum TYPE {COMBINED, VERTEX, FRAGMENT, COMPUTE};

		Shader() = default;
		Shader(const std::string& filename);
		Shader(const std::string& filename, TYPE shader_type);
		~Shader();

		void bind() const;
		void unbind() const;

		// set uniforms:
		//void setUniform1i(const std::string& name, int v0);
		void setUniform1i(const char* name, int v0);
		//void setUniform1f(const std::string& name, float v0);

		void setUniform1f(const char* name, float v0);
		void setUniform3f(const char* name, float v0, float v1, float v2);
		//void setUniform4f(const std::string& name, float v0, float v1, float v2, float v3);

		//void setUniformMat4f(const std::string& name, const glm::mat4& mat4x4);
		void setUniformMat4f(const char*, const glm::mat4& mat4x4);
		//void setUniformRawMat4f(const std::string& name, const float* raw4x4);
		template <typename T>
		void setUniformBlock(const char* name, T* data);

		void pushTexture(unsigned textureID);
		void popTexture();

		unsigned int m_RendererID;
		// a shader might have a texture-bind queue:
		// (in case the shader has:
		//  diffuse+normal+specular+... maps/textures)
		std::deque<unsigned> m_textureQ;

		std::vector<ShaderUniformLayout> m_uniforms;

		const char* getFilename() { return m_Filepath.c_str(); }

	private:

		std::string m_Filepath;
		//std::unordered_map<std::string, int> m_UniformLocationCache;
		std::unordered_map<std::string_view, int> m_UniformLocationCache;
		TYPE m_type;

		ShaderProgramSource parseShader(const std::string& filepath);
	
		unsigned int compileShader(unsigned int type,
								   const std::string& source);

		unsigned int createShader(const std::string& vertexShader,
								  const std::string& fragmentShader);

		//unsigned int getUniformLocation(const std::string& name);
		//unsigned int getUniformLocation(const std::string_view name);
		unsigned int getUniformLocation(const char* name);

	};

}