#pragma once

#include "Core/Singleton.h"
#include "Framework/SampleECS/Object/Object.h"
#include "Scene.h"

namespace Aurora
{
	class ObjectStore;

	class Global : public Singleton<Global>
	{
	public:

		virtual ~Global()
		{
			for (size_t i = 0; i < scenes.size(); i++)
			{
				delete(scenes.at(i));
			}
		}

		void init();
		void Update();
		void Render();
		
		void ResetScenes(std::string path);

		void RegisterObject(Object* obj);
		void UnregisterObject(Object* obj);
		ObjectStore* GetSceneObjects() { return scenes[activeSceneIndex]->GetObjects(); }
	private:
		unsigned int activeSceneIndex = 0;
		std::vector<Scene*> scenes;
	};
	
}

