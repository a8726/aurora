#pragma once
#include "Framework/SampleECS/Object/Object.h"
#include "PrimitiveObjectProperties.h"
#include "Core/Renderer/Primitives.h"
#include "Core/Renderer/Renderer.h"
#include "Core//Renderer/VertexBufferLayout.h"

#include "PrimitiveObject.h"

namespace Aurora
{
    class Plane : public PrimitiveObject
    {
	public:

		Plane();
		~Plane();
		Plane(PrimitiveObjectProperties properties);
		// Implement Virtual Function getTypeDes() from Parent Class:
		const char* getTypeDes() override { return "Plane"; }

		Surface		m_primitive_surface;
		Surface		generatePlane(PrimitiveObjectProperties properties);
		Surface		generatePlane(const float& plane_height,
			const unsigned& segments1, const unsigned& segments2,
			const float& width, const float& height, const float& tex_width,
			const float& tex_height, const int& flat_index);

	protected:
		// Implement Virtual Function render() from Parent Class:
		void render() override;
		Surface generateSurface(PrimitiveObjectProperties properties) override;

    };
}
