#pragma once
#include "Framework/SampleECS/Object/Object.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/Plane.h"
namespace Aurora
{
	class Cube : public Plane
	{
	public:

		Cube();
		~Cube();
		Cube(PrimitiveObjectProperties properties);
		// Implement Virtual Function getTypeDes() from Parent Class:
		const char* getTypeDes() override { return "Cube"; }

		Surface		m_primitive_surface;

	protected:
		// Implement Virtual Function render() from Parent Class:
		Surface generateSurface(PrimitiveObjectProperties properties) override;

	};
}
