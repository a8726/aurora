#pragma once

#include "Framework/SampleECS/Component/ComponentBase.h"
#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"
#include "Core/StaticReflection.h"

class ObjMoveTest : public Aurora::ComponentBase
{
public:
	virtual std::string getTypeDes() override
	{
		return "ObjMoveTest";
	}

private:
	float moveSpeed = 0.5f;

	Aurora::Transform* transform;

	void keyIKeepCallback();
	void keyJKeepCallback();
	void keyKKeepCallback();
	void keyLKeepCallback();
	void keyUKeepCallback();
	void keyOKeepCallback();

	virtual void OnEnable(Aurora::Object* obj) override;
	virtual void OnDisable(Aurora::Object* obj) override;
	virtual void OnStart(Aurora::Object* obj) override;
	virtual void Update(Aurora::Object* obj) override;
	virtual void OnShutdown(Aurora::Object* obj) override;
};

