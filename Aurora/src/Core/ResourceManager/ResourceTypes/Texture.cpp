#include "aurora_pch.h"
#include "glad/glad.h"
#define STB_IMAGE_IMPLEMENTATION
#include "Texture.h"

// we use glm::vec3 for 3d vectors (3x1)
#include "../thirdparty/glm/glm.hpp"

namespace Aurora
{

    Texture::Texture(): m_data(0), m_image(0)
    {
    }

    const GLubyte* Texture::getData()
    {
        return m_data;
    }

    void Texture::bind()
    {
        //std::cout << "tex " << m_texture_path.c_str() << " on " << m_type << std::endl;
        glActiveTexture(GL_TEXTURE0 + m_type);
        glBindTexture(GL_TEXTURE_2D, m_ID);
    }

    void Texture::generate()
    {
        // unbind any current texture:
        glBindTexture(GL_TEXTURE_2D, 0);
        glGenTextures(1, &this->m_ID);

        glBindTexture(GL_TEXTURE_2D, this->m_ID);

        // target, level, internal format, dimensions, border, format, type, data
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
        glTexImage2D(/*target*/ GL_TEXTURE_2D, /*level*/0, 
                     /*internal format */ GL_RGB8, this->getWidth(), this->getHeight(),
            /*border*/ 0, /*format*/ GL_RGB, /*type*/ GL_UNSIGNED_BYTE, this->getData());
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, this->m_wrapping);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, this->m_wrapping);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, this->m_min_filter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, this->m_mag_filter);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    Texture* Texture::clone() const
    { 
        return new Texture(*this); 
    }

    bool Texture::load(const std::string path)
    {
        if (path != "texdefault" && !path.empty())
        {// only update the texture path if ...
         // ... it not the default one already
            m_texture_path = path;
        }

        FILE* file_ptr;
        fopen_s(&file_ptr, m_texture_path.c_str(), "rb");

        if (file_ptr == nullptr)
        {
            AURORA_LOG_FATAL("ERROR: Could Not Read Texture File");
            //m_data = nullptr;
            return false;
        }

        //m_image = new cimg_library::CImg<GLubyte>(m_texture_path.c_str());
        m_data = stbi_load(m_texture_path.c_str(), &m_width, &m_height, &m_channels, 0);

        unsigned int nm = m_width * m_height;

        // useful for debugging - Cimg allows to just display the loaded image!
        //m_image->display();

        return true;
    }

    void Texture::load()
    {
        this->load(m_texture_path.c_str());
    }


    Texture::~Texture()
    {
        delete m_data;
        delete m_image;
    }
}
