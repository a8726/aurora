#pragma once

namespace Aurora
{
	struct PrimitiveObjectProperties
	{
		// OPTIONAL PARAMETERS FOR THE PLANE:
		float						  plane_offset = -0.5f;
		unsigned					  plane_flat_index = 1;
		unsigned					  segments_x = 10;
		unsigned					  segments_y = 10;
		float						  width = 1.f;
		float						  height = 1.f;
		float						  tex_width = 1.f;
		float						  tex_height = 1.f;
		// OPTIONAL PARAMETERS FOR THE CUBE/SPHERE:
		float						  radius = 0.25f;
		// OPTIONAL PARAMETERS FOR THE SPHERE:
		float						  squish_stretch_factor = 1.f;
		unsigned					  stack_count = 30;
		unsigned					  sector_count = 30;
		// OPTIONAL PARAMETERS FOR TRANSFORMATION:
		float						  scale_x = 1.f;
		float						  scale_y = 1.f;
		float						  scale_z = 1.f;

	};

}