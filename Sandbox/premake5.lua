project "Sandbox"
	language "C++"
	cppdialect "c++17"
	kind "ConsoleApp"
	staticruntime "off"

	targetdir (targetOutputDir .. "/%{prj.name}")
	objdir (objOutputDir .. "/%{prj.name}")

	includedirs
	{
		"%{wks.location}/Aurora/src",
		thirdparty .. "/spdlog/include",
		thirdparty .. "/GLFW/include",
		thirdparty .. "/ImGui",
	}

	defines
	{
		"AURORA_PLATFORM_WINDOWS"
	}

	files
	{
		"src/**.h",
		"src/**.cpp",

		"**.lua",
	}

	links
	{
		"Aurora"
	}

	filter "configurations:Debug"
		defines "TO_DEBUG"
		runtime "Debug"
		links
		{
			thirdparty.. "/fbxsdk/lib/debug/libfbxsdk-md.lib",
			thirdparty.. "/fbxsdk/lib/debug/libxml2-md.lib",
			thirdparty.. "/fbxsdk/lib/debug/zlib-md.lib",
		}
		symbols "on"

	filter "configurations:Release"
		defines "TO_RELEASE"
		runtime "Release"
		links
		{
			thirdparty.. "/fbxsdk/lib/release/libfbxsdk-md.lib",
			thirdparty.. "/fbxsdk/lib/release/libxml2-md.lib",
			thirdparty.. "/fbxsdk/lib/release/zlib-md.lib",
		}
		optimize "off"