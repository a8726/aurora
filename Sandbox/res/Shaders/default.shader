#shader vertex
#version 450 core

layout(location = 0) in vec4 in_position;
layout(location = 1) in vec4 in_normal;

out vec3 vertout_normal;
out vec3 vertout_fragment_position;

uniform mat4 u_model;		// M
uniform mat4 u_view;		// V
uniform mat4 u_projection;	// P


void main()
{
    gl_Position = u_projection * u_view * u_model * vec4(in_position);
    gl_PointSize = 10.0;

    vertout_fragment_position = vec3(u_model * in_position);
    vertout_normal = mat3(transpose(inverse(u_model))) * vec3(in_normal.x, in_normal.y, in_normal.z);
};




















#shader fragment
#version 450 core

layout(binding = 0, std140) uniform u_material
{
    vec4	u_ambient_col;
    vec4	u_diffuse_col;
    vec4	u_specular_col;
    vec4	u_emission_col;
    float 	u_specular_coef;
    float 	u_optical_density;
    float 	u_opacity;
    float 	u_illum;
};

// output color per pixel (fragment):
layout(location = 0) out vec4 out_color;

// take inputs from vertex shader
in vec3 vertout_normal;
in vec3 vertout_fragment_position;

uniform vec3 u_light_position;
uniform vec3 u_camera_position;

#include "../Sandbox/res/Shaders/illumination.hlsli"

void main()
{
    // Normal vector:
    vec3 N = normalize(vertout_normal.rgb);
    // Light direction (from current ray position)
    vec3 L = normalize(u_light_position - vertout_fragment_position);
    // Camera direction (from current ray position)
    vec3 C = normalize(u_camera_position - vertout_fragment_position);
    // add illumination to the colour:
    vec3 illuminated_color = add_local_illumination(
        /* diffuse color */ u_diffuse_col.rgb, 
        /* ambient color */ u_ambient_col.rgb,
        /* specular color*/ u_specular_col.rgb,
        /* opacity       */ u_opacity,
        /* shine         */ u_specular_coef,
        /* normal, light direction, camera direction */ N, L, C,
        /* turn individual lights on                 */ u_specular_col.a, u_diffuse_col.a, u_ambient_col.a);

    // final fragment color:
    out_color = vec4(illuminated_color, 1.f);
};
