#include "aurora_pch.h"
#include "MemoryTracker.h"

bool Aurora::MemoryTracker::isInit = false;

void Aurora::MemoryTracker::init()
{
	isInit = true;
}

void Aurora::MemoryTracker::Allocate(size_t size)
{
	if (isInit)
	{
		memoryAllocated += size;
	}
}

void Aurora::MemoryTracker::Free(size_t size)
{
	if (isInit)
	{
		memoryFreed += size;
	}
}

size_t Aurora::MemoryTracker::getCurMemoryCost()
{
	return memoryAllocated - memoryFreed;
}

size_t Aurora::MemoryTracker::getMemoryAllocated()
{
	return memoryAllocated;
}

size_t Aurora::MemoryTracker::getMemoryFreed()
{
	return memoryFreed;
}
