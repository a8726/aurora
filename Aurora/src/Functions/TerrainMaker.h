// Created by John Barbone
// all work is my own

#pragma once

#ifndef _TERRAIN_MAKER_
#define _TERRAIN_MAKER_

#define PATH_TO_HEIGHT_SHADER "../Aurora/src/Functions/noisemap.comp"
#define PATH_TO_NORMAL_SHADER "../Aurora/src/Functions/normalsColors.comp"


#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <string>

#include "glad/glad.h"

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGl/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif
/*
class TerrainMaker {
public:
	int texHeight, texWidth, texelStride;
	float meshXdim, meshYdim, meshMaxHeight;
	int zones;
	float seed;


};
*/
const char * GetGLErrorStr(GLenum err) {
	switch (err) {
	case GL_NO_ERROR:          return "No error";
	case GL_INVALID_ENUM:      return "Invalid enum";
	case GL_INVALID_VALUE:     return "Invalid value";
	case GL_INVALID_OPERATION: return "Invalid operation";
	case GL_STACK_OVERFLOW:    return "Stack overflow";
	case GL_STACK_UNDERFLOW:   return "Stack underflow";
	case GL_OUT_OF_MEMORY:     return "Out of memory";
	default:                   return "Unknown error";
	}
}

// assumes both textures have same width and height
// texelStride is how many texels we go between vertices
// so with texelStride = 1, we turn every texel into a vertex
// with texelStride = 10, a 60x80 texture becomes a mesh of a 6x8 grid of vertices
void makeMeshFromTexture(int heightTextureUnit, int normalTextureUnit, int colorTextureUnit, int texWidth, int texHeight, int texelStride, float meshXsize, float meshYsize, float maxHeight, const char * resultsFolder = "../Sandbox/res/Terrain", const char * filename = "terrain") {
	
	std::string objPath = std::string(resultsFolder) + "/" + std::string(filename) + ".obj";
	std::string mtlPath = std::string(resultsFolder) + "/" + std::string(filename) + ".mtl";
	std::string ppmPath = std::string(resultsFolder) + "/" + std::string(filename) + ".ppm";
	std::string ppmHPath = std::string(resultsFolder) + "/" + std::string(filename) + "_heightmap.ppm";
	std::string ppmNPath = std::string(resultsFolder) + "/" + std::string(filename) + "_normalmap.ppm";


	// open file first so that if any errors here we just skip the rest
	// create the output filepath
	std::ofstream outputFile(objPath);
	if (!outputFile.good()) { // if bad, alert the user
		std::cerr << "Cannot use save file " << objPath << std::endl;
		return;
	}

	// init vectors
	std::vector<float> verts    = std::vector<float>();
	std::vector<float> uvs      = std::vector<float>();
	std::vector<float> norms    = std::vector<float>();

	std::vector<int> faces      = std::vector<int>();

	
	

	// 1 for GL_RED, 4 for GL_RGBA, etc. 
	// height should be single value, so read it as red
	int formatFactor = 1; 

	std::vector<GLfloat> heightData;
	heightData.resize(texHeight * texWidth * formatFactor);

	while (glGetError() != GL_NO_ERROR);
	// clear the error queue just in case

	// get image data from texture
	glActiveTexture(GL_TEXTURE0 + heightTextureUnit);
	//glBindTexture(GL_TEXTURE_2D, heightTexture);
	// don't need to rebind since its the last texture bound to that unit
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT, heightData.data());
	// FOR USE WITH OPENGL 4.5:
	
	auto err = glGetError();
	if (err != GL_NO_ERROR) {
		throw std::runtime_error { std::string { "glGetTexImage failed: " }
					+ std::string{ GetGLErrorStr(err)} };
	}

	// end reading heightmap texture



	// begin reading normal map texture
 
	std::vector<GLfloat> normalData;
	normalData.resize(texHeight * texWidth * 4);

	while (glGetError() != GL_NO_ERROR);
	// clear the error queue just in case

	// get image data from texture
	glActiveTexture(GL_TEXTURE0 + normalTextureUnit);

	//glBindTexture(GL_TEXTURE_2D, normalTexture);
	// don't need to rebind since its the last texture bound to that unit
	//glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, normalData.data());


	err = glGetError();
	if (err != GL_NO_ERROR) {
		throw std::runtime_error { std::string { "glGetTexImage failed: " }
					+ std::string{ GetGLErrorStr(err)} };
	}

	// end reading normal map texture

	/// READING COLOR TEXTURE
	
	//std::vector<GLfloat> colorData;
	std::vector<GLubyte> colorData;
	colorData.resize(texHeight * texWidth * 3);

	while (glGetError() != GL_NO_ERROR);
	// clear the error queue just in case

	// get image data from texture
	glActiveTexture(GL_TEXTURE0 + colorTextureUnit);

	// don't need to rebind bc it was the last texture bound to this texture unit
	glPixelStorei(GL_PACK_ALIGNMENT, 1);

	//glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_FLOAT, colorData.data());
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, colorData.data());
	// rgb instead of rgba bc we don't need a value
	// format and type does not need to match TexImage internal format here
	

	err = glGetError();
	if (err != GL_NO_ERROR) {
		throw std::runtime_error{ std::string { "glGetTexImage failed: " }
					+ std::string{ GetGLErrorStr(err)} };
	}

	//// MAKING MTL FILE
	std::ofstream MTLfile(mtlPath, std::ofstream::trunc);
	MTLfile << "# Blender MTL File: 'None' \n"
		<< "# Material Count : 1\n"
		<< "\n"
		<< "newmtl terrain_mtl\n"
		<< "Ns 1.000000\n"
		<< "Ka 0.100000 0.100000 0.100000\n"
		<< "Kd 0.800000 0.800000 0.800000\n"
		<< "Ks 0.000000 0.000000 0.000000\n"
		<< "Ke 1.0 1.0 1.0\n"
		<< "Ni 1.450000\n"
		<< "d 1.000000\n"
		<< "illum 1\n"
		<< "map_Kd " << filename << "_normalmap.ppm" << "\n";


	//// MAKING PPM FILE
	unsigned int r, g, b;

	std::ofstream PPMfile(ppmPath, std::ofstream::trunc);
	PPMfile << "P6\n" << texWidth << " " << texHeight << "\n255\n#comment";
	// unsure why but need 3 \n to fix alignment
	//PPMfile << (unsigned char)'\n';
	PPMfile << (unsigned char)'\n';
	PPMfile << (unsigned char)'\n';
	for (GLubyte b : colorData)
		PPMfile << (unsigned char)b;
	//PPMfile << "P3\n" << texWidth << " " << texHeight << "\n255\n";
	//PPMfile.precision(0);
	//PPMfile.setf(std::ios::fixed, std::ios::floatfield);
	//for (int i = 0; i < texWidth; i++) {
	//	for (int j = 0; j < texHeight; j++) {
	//		
	//		r = colorData[(i * texWidth + j) * 3 + 0] * 255.0;
	//		g = colorData[(i * texWidth + j) * 3 + 1] * 255.0;
	//		b = colorData[(i * texWidth + j) * 3 + 2] * 255.0;
	//		PPMfile << r << " " << g << " " << b << "  ";
	//		if ((j + 1) % 4 == 0) PPMfile << "\n";
	//	}
	//}

	// flip these in the output
	// heightmap
	std::ofstream PPMHfile(ppmHPath, std::ofstream::trunc);
	PPMHfile << "P6\n" << texWidth << " " << texHeight << "\n255\n#comment";
	// unsure why but need 3 \n to fix alignment
	//PPMfile << (unsigned char)'\n';
	//PPMfile << (unsigned char)'\n';
	//PPMfile << (unsigned char)'\n';
	//PPMHfile.precision(0);
	//PPMHfile.setf(std::ios::fixed, std::ios::floatfield);
	for (int i = 0; i < texWidth; i++) {
		for (int j = 0; j < texHeight; j++) {

			r = (unsigned int)(heightData[(i * texWidth + j) * formatFactor + 0] * 255.0);
			// rgb vals are identical
			// and only have red channel from output
			// so r r r output
			PPMHfile << (unsigned char)r;
			PPMHfile << (unsigned char)r;
			PPMHfile << (unsigned char)r;
			//PPMHfile << r << " " << r << " " << r << "  ";
			//if ((j + 1) % 4 == 0) PPMHfile << "\n";
		}
	}

	// normalmap
	std::ofstream PPMNfile(ppmNPath, std::ofstream::trunc);
	PPMNfile << "P6\n" << texWidth << '\n' << texHeight << "\n255\n#comment\n";
	// unsure why but need 3 \n to fix alignment
	//PPMfile << (unsigned char)'\n';
	//PPMfile << (unsigned char)'\n';
	//PPMfile << (unsigned char)'\n';
	//PPMNfile.precision(0);
	//PPMNfile.setf(std::ios::fixed, std::ios::floatfield);
	for (int i = 0; i < texWidth; i++) {
		for (int j = 0; j < texHeight; j++) {
		//for (int j = texHeight - 1; j > 0; j--) { // use if vertical flip needed

			r = (unsigned int)(normalData[(i * texWidth + j) * 4 + 0] * 255.0);
			g = (unsigned int)(normalData[(i * texWidth + j) * 4 + 1] * 255.0);
			b = (unsigned int)(normalData[(i * texWidth + j) * 4 + 2] * 255.0);

			PPMNfile << (unsigned char)r;
			PPMNfile << (unsigned char)g;
			PPMNfile << (unsigned char)b;
			//PPMNfile << r << " " << g << " " << b << "  ";
			//if ((j + 1) % 4 == 0) PPMNfile << "\n";
		}
	}
	
	 


	//// MAKING OBJ FILE


	// start to make the mesh
	int numXsamples = texWidth  / texelStride;
	int numYsamples = texHeight / texelStride;

	float uvX, uvY;
	int lookupX, lookupY, lookupIndex;

	/// construct vectors

	// init row, col here so that they retain value of their respective end conditions after loops finish
	int row, col;
	for (row = 0; row < numYsamples - 1; row ++) {
		// scale lookup
		uvY = (((float)row)/((float)numYsamples - 1));
		lookupY = uvY * texHeight;
		if (lookupY >= texHeight) lookupY = texHeight - 1;
		
		for (col = 0; col < numXsamples - 1; col ++) {

			uvX = (((float)col)/((float)numXsamples - 1));
			lookupX = uvX * texWidth;
			if (lookupX >= texWidth) lookupX = texWidth - 1;
			lookupIndex = (lookupY * texWidth + lookupX);

			// add x, y, z to verts
			// y is up, get val from texture data
			verts.push_back((uvX - 0.5) * meshXsize);
			verts.push_back(maxHeight * heightData[lookupIndex] - maxHeight * 0.5); // offset y by half maxheight
			//verts.push_back(maxHeight * normalData[4 * lookupIndex + 3]);
			verts.push_back((uvY - 0.5) * meshYsize);

			// add UV coords to uvs
			uvs.push_back(uvX);
			uvs.push_back(uvY);

			// add normals 
			// look up as 4 * lookupIndex + 0,1,2 for x,y,z 
			// then remap 0-1 as -1 to 1 for each
			
			float normX = (normalData[4 * lookupIndex + 0] * 2.f) - 1.f;
			float normY = (normalData[4 * lookupIndex + 1] * 2.f) - 1.f;
			float normZ = (normalData[4 * lookupIndex + 2] * 2.f) - 1.f;
			// calculate vector norm
			float norm = sqrt(normX * normX + normY * normY + normZ * normZ);
			// re-normalize by dividing by norm before pushing
			// (may not be normalized after remapping process)
			norms.push_back(normX / norm);
			norms.push_back(normY / norm);
			norms.push_back(normZ / norm);


			/// MAKING FACES
			// this col loop does not iterate through vertices in the last row or col of vertices
			// so each vertex made by this loop adds the faces between it and the vertex at row + 1 and col + 1
			// visual: vertex (a) will add the faces (aeb) and (ade), vertex (b) will add the faces (bec) and (cef) 
			// a - - - b - - - c
			// | \     |     / |
			// |   \   |   /   |
			// |     \ | /     |
			// d - - - e - - - f
			// if this was the whole mesh, (c), (d), (e), (f) would not be made in this col loop 
			// thus no extraneous faces would be made

			// determine relevent vertex IDs
			int vID     = (row + 0) * numXsamples + col;
			int vcID    = (row + 0) * numXsamples + col + 1;
			int vrID    = (row + 1) * numXsamples + col;
			int vrcID   = (row + 1) * numXsamples + col + 1;
			
			// determine creating a or b style faces (see figure above for distinction)
			// (a) style for when row and col are simultaneously even or odd
			// (b) style for when row or col is even and the other is odd
			if (row % 2 == col % 2) { // (a) style
				// face 1: (aeb)
				faces.push_back(vID);
				faces.push_back(vrcID);
				faces.push_back(vcID);
				// face 2: (ade)
				faces.push_back(vID);
				faces.push_back(vrID);
				faces.push_back(vrcID);
			} else { // (b) style
				// face 1: (bec)
				faces.push_back(vID);
				faces.push_back(vrID);
				faces.push_back(vcID);
				// face 2: (cef)
				faces.push_back(vcID);
				faces.push_back(vrID);
				faces.push_back(vrcID);
			}

		} // end col loop
		// do last column (col should have value numXsamples - 1 here)
		// same as others but no faces made

		// knowing col value means we can simplify/skip some math
		uvX = 1.f;
		lookupX = texWidth - 1;
		lookupIndex = (lookupY * texWidth + lookupX);
		
		// add x, y, z to verts
		// y is up, get val from texture data
		verts.push_back((uvX - 0.5) * meshXsize);
		verts.push_back(maxHeight * heightData[lookupIndex] - maxHeight * 0.5);
		//verts.push_back(maxHeight * normalData[4 * lookupIndex + 3]);
		verts.push_back((uvY - 0.5) * meshYsize);

		// add UV coords to uvs
		uvs.push_back(uvX);
		uvs.push_back(uvY);

		// add normals 
		// look up as 4 * lookupIndex + 0,1,2 for x,y,z 
		// then remap 0-1 as -1 to 1 for each
		norms.push_back((normalData[4 * lookupIndex + 0] * 2.f) - 1.f);
		norms.push_back((normalData[4 * lookupIndex + 1] * 2.f) - 1.f);
		norms.push_back((normalData[4 * lookupIndex + 2] * 2.f) - 1.f);

	} // end row loop
	// row should have value numYsamples - 1 here
	// knowing row value means we can simplify/skip some math
	uvY = 1.f;
	lookupY = texHeight - 1;

	// same as nested col loop, except no faces added
	for (col = 0; col < numXsamples; col ++) {

		uvX = (((float)col)/((float)numXsamples - 1));
		lookupX = uvX * texWidth;
		if (lookupX >= texWidth) lookupX = texWidth - 1;
		lookupIndex = (lookupY * texWidth + lookupX);

		// add x, y, z to verts
		// y is up, get val from texture data
		verts.push_back((uvX - 0.5) * meshXsize);
		verts.push_back(maxHeight * heightData[lookupIndex] - maxHeight * 0.5);
		//verts.push_back(maxHeight * normalData[4 * lookupIndex + 3]);
		verts.push_back((uvY - 0.5) * meshYsize);

		// add UV coords to uvs
		uvs.push_back(uvX);
		uvs.push_back(uvY);

		// add normals 
		// look up as 4 * lookupIndex + 0,1,2 for x,y,z 
		// then remap 0-1 as -1 to 1 for each
		norms.push_back((normalData[4 * lookupIndex + 0] * 2.f) - 1.f);
		norms.push_back((normalData[4 * lookupIndex + 1] * 2.f) - 1.f);
		norms.push_back((normalData[4 * lookupIndex + 2] * 2.f) - 1.f);

	} // end last col loop

	// end vector construction

	/// WRITING TO FILE
	// use comments to write mini header
	outputFile << "# generated terrain object\n";
	outputFile << "# " << numXsamples << " by " << numYsamples << " vertices\n";
	outputFile << "# " << verts.size() / 3 << " vertices\n";
	outputFile << "# " << uvs.size() / 2 << " texture coords\n";
	outputFile << "# " << norms.size() / 3 << " normals from texture\n";
	outputFile << "# " << faces.size() / 3 << " faces\n\n";

	// link mtl file
	outputFile << "mtllib " << mtlPath << "\n";

	// write vertices
	// prefix is 'v'
	outputFile << "\n\n# vertices \n";
	for (int i = 0; i < verts.size(); i += 3 )
		outputFile << "v " << verts[i] << " " << verts[i + 1] << " " << verts[i + 2] << std::endl;
		
	
	// write texture coords
	// prefix is 'vt'
	outputFile << "\n\n# texture coords \n";
	for (int i = 0; i < uvs.size(); i += 2 )
		outputFile << "vt " << uvs[i] << " " << uvs[i + 1] << std::endl;

	// write "normals" NB: this version does not get normals from texture
	// prefix is 'vn'
	outputFile << "\n\n# normals \n";
	if (norms.size() != verts.size()) for (int i = 0; i < verts.size(); i += 3) outputFile << "vn 0.0 1.0 0.0\n"; 
	else for (int i = 0; i < norms.size(); i += 3) 
		outputFile << "vn " << norms[i] << " " << norms[i + 1] << " " << norms[i + 2] << std::endl;

	outputFile << "\n\n# material to use \nusemtl terrain_mtl\ns 1\n";

	// write faces
	// prefix is 'f'
	// for each vertex give id, tex coord id, normal id 
	// as v/vt/vn
	outputFile << "\n\n# faces \n";
	for (int i = 0; i < faces.size(); i += 3) {
		// + 1 to all bc vertices are 1-indexed in .obj files
		outputFile << "f "  << faces[i] + 1 << "/" << faces[i] + 1 << "/" << faces[i] + 1 << " " 
							<< faces[i + 1] + 1 << "/" << faces[i + 1] + 1 << "/" << faces[i + 1] + 1 << " "
							<< faces[i + 2] + 1 << "/" << faces[i + 2] + 1 << "/" << faces[i + 2] + 1 << std::endl;  
	}


	std::cout << "done! \n";
}


/// SHADER LOADING
// taken from my 3rd year final project
GLuint loadShader(const char * path, bool safe = true) {
	GLuint shader = glCreateShader(GL_COMPUTE_SHADER);

	std::ifstream shader_file (path, std::ifstream::in);

	if (!shader_file.is_open()) {
		std::cerr << "File " << path << " failed to open \n";
		return 0;
	}
	// seek to end of file
	shader_file.seekg(0,shader_file.end);
	// report int position of end of file
	int fileLength = shader_file.tellg();
	// seek back to start
	shader_file.seekg(0, shader_file.beg);

	//read file to char array
	char * shaderSource = new char[fileLength + 1];
	//std::cout << shader_file.fill('\0') << std::endl;
	shader_file.read(shaderSource, fileLength);
	shader_file.close();

	

	// ensure last char is null terminator
	shaderSource[fileLength] = '\0';

	//// NB: if getting errors with shader compilation 
	// (c0000 unexpected undefined at unkown) kinda thing 
	// change comp format to LF from CRLF

	//std::cout << std::endl;
	//for (int i = 20; i > 0; i -= 1)
	//    std::cout << shaderSource[fileLength - i];
	//std::cout << std::endl;
	//printf("\n%sx\n", shaderSource);
	

	// debug things
	GLint success = GL_FALSE;
	int logLength;

	// compiling shader
	glShaderSource(shader, 1, &shaderSource, NULL);
	glCompileShader(shader);
	
	//check shader
	if (safe) {
		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		if (!success) {
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
			//char shaderErrorLog[logLength];
			//std::vector<char> shaderErrorLog;
			//shaderErrorLog.resize(logLength);
			//shaderErrorLog.reserve(logLength);
			char* shaderErrorLog = (char*)alloca(logLength * sizeof(char));
			glGetShaderInfoLog(shader, logLength, NULL, &shaderErrorLog[0]);
			printf("Error compiling %s \n%s \n", path, shaderErrorLog);
			exit(EXIT_FAILURE);
		}
	}

	// linking program
	GLuint program = glCreateProgram();
	glAttachShader(program, shader);
	glLinkProgram(program);

	// check program linking
	if (safe) {
		glGetProgramiv(program, GL_LINK_STATUS, &success);
		if (!success) {
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
			//char programErrorLog[logLength];
			//std::vector<char> programErrorLog;
			//programErrorLog.reserve(logLength);
			//programErrorLog.resize(logLength);
			char* programErrorLog = (char*)alloca(logLength * sizeof(char));
			glGetProgramInfoLog(program, logLength, NULL, &programErrorLog[0]);
			printf("Error in program \n%s\n", programErrorLog);
			exit(EXIT_FAILURE);
		}
	}

	// shader exists as part of program, do not need to store it elsewhere
	glDeleteShader(shader);

	return program;
}

GLuint makeTerrain(int texWidth, int texHeight, int texelStride, float meshXsize, float meshYsize, float maxHeight, int zones = 4, float seed = 1234.5678, const char* resultsFolder = "../Sandbox/res/Terrain/", const char* filename = "terrain") {
	
	//std::cout << "makeTerrain() ~ " << texWidth << " " << texHeight << std::endl;
	
	// start by setting up comp shader programs
	GLuint heightCompProgram = loadShader(PATH_TO_HEIGHT_SHADER);
	GLuint normalCompProgram = loadShader(PATH_TO_NORMAL_SHADER);

	
	
	// compute shader execution based on:
	// https://antongerdelan.net/opengl/compute.html
	// very loosly based


	GLuint * terrainTextures = new GLuint[3];
	glGenTextures(3, terrainTextures);
	// 3 textures:
	// 0: heightmap
	// 1: normal map
	// 2: color texture

	

	// prepare heightmap texture
	glActiveTexture(GL_TEXTURE0 + 2); 
	// using unit 2 (binding 2) to store height texture
	// this is so we can use unit 0 for colors
	glBindTexture(GL_TEXTURE_2D, terrainTextures[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, texHeight, texWidth, 0, GL_RED, GL_FLOAT, NULL);
	// it is CRUCIAL that all formats in the adjacent lines agree with each other AND with the format given in the shader
	glBindImageTexture(0, terrainTextures[0], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32F);
	//				   ^ this first number is the IMAGE UNIT the image is bound to; binding = this for Image2D in shader

	// execute heightmap shader
	glUseProgram(heightCompProgram);

	// set uniforms
	GLint varLocation = glGetUniformLocation(heightCompProgram, "seed");
	if (varLocation != -1) glUniform1f(varLocation, seed);
	varLocation = glGetUniformLocation(heightCompProgram, "zones");
	if (varLocation != -1) glUniform1f(varLocation, (float)zones);

	glDispatchCompute((GLuint)texWidth, (GLuint)texHeight, 1);
  
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	// wait until shader access is over

	// prepare normal map texture
	glActiveTexture(GL_TEXTURE0 + 1); // using TEXTURE unit 1 (sampler binding 1) to store normalmap texture
	glBindTexture(GL_TEXTURE_2D, terrainTextures[1]);

	//glPixelStorei(GL_PACK_ALIGNMENT, 1);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, texHeight, texWidth, 0, GL_RGBA, GL_FLOAT, NULL);
	// it is CRUCIAL that all formats in the adjacent lines agree with each other AND with the format given in the shader
	glBindImageTexture(1, terrainTextures[1], 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
	//				   ^ this first number is the IMAGE UNIT the image is bound to; binding = this for Image2D in shader


	// prepare colors texture
	glActiveTexture(GL_TEXTURE0); // using TEXTURE unit 0 (sampler binding 0) to store colors texture
	glBindTexture(GL_TEXTURE_2D, terrainTextures[2]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, texHeight, texWidth, 0, GL_RGBA, GL_FLOAT, NULL);
	// it is CRUCIAL that all formats in the adjacent lines agree with each other AND with the format given in the shader
	glBindImageTexture(2, terrainTextures[2], 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
	//				   ^ this first number is the IMAGE UNIT the image is bound to; binding = this for Image2D in shader



	// prep second comp shader
	glUseProgram(normalCompProgram);
	
	// set uniforms
	varLocation = glGetUniformLocation(normalCompProgram, "pixelStride");
	if (varLocation != -1) glUniform1f(varLocation, (float)texelStride);
	varLocation = glGetUniformLocation(normalCompProgram, "maxHeight");
	if (varLocation != -1) glUniform1f(varLocation, maxHeight);

	float ms[2] = { meshXsize, meshYsize };
	varLocation = glGetUniformLocation(normalCompProgram, "meshSize");
	if (varLocation != -1) glUniform2fv(varLocation, 1, ms);

	// run shader
	glDispatchCompute((GLuint)texWidth, (GLuint)texHeight, 1);

	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	// wait until done

	std::cout << "shaders done\n saving mesh and textures to disk...\n";

	// make obj and ppm files
	makeMeshFromTexture(2, 1, 0, texWidth, texHeight, texelStride, meshXsize, meshYsize, maxHeight, resultsFolder, filename);

	// return colors texture
	glActiveTexture(GL_TEXTURE0);
	return terrainTextures[0];

}



#endif