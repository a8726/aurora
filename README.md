## Aurora

Getting Started

execute following command to generate .sln for visual studio (lower than vs2019 is ok)

```
premake5 vs2019
Launch the generated .sln file with Visual Studio
choose the Startup Project as Sandbox (should be chosen by default)
Run
```

Table of Contents:
1. Renderer Readme (Dom) ............................... Aurora/src/Core/Renderer/A_Renderer_README.txt



## Rules

main loop of game: now in the file WindowsWindow.cpp and function init()

basic code: inside Core and Framework and Platform, this three folders

name: Camel-Case

coding: inside your own folder

log pattern: <font color="#C0392B">every output must use this !!!!!!!!</font>

```c++
// logging for engine developer
AURORA_LOG_TRACE(...)
AURORA_LOG_DEBUG(...)
AURORA_LOG_INFO(...)
AURORA_LOG_WARN(...)
AURORA_LOG_ERROR(...)
AURORA_LOG_FATAL(...)

// logging for game developer
DEBUG_LOG_TRACE(...)
DEBUG_LOG_DEBUG(...)
DEBUG_LOG_INFO(...)
DEBUG_LOG_WARN(...)
DEBUG_LOG_ERROR(...)
DEBUG_LOG_FATAL(...)
```

UI: <font color="#C0392B">for every words showed on UI elements, must cover like this</font>

```
LOCALIZATION("Click");
```



## Third Party Libraries

Glad: the OpenGL wrapper, loader and generator.
GLFW: a library for creating window, handling events.
GLM: for all the basic mathematical data structures and algorithms.
ImGUI: a graphical user interface library.
spdlog: for C++ logging.
assimp: to solve the requirements of reading and writing different types of files.
FBX-SDK: for reading and handling FBX files.
Eigen: the expansion of GLM, used for getting inverse matrices 
OZZ: C++ 3D skeletal animation library and tool-set.
stb: for loading images.
Bullet: for physics simulation.


Regarding licences of libraries and programming interfaces that make up the project, OpenGL is under the Free Software License; Premake is under BSD 3-Clause "New" or "Revised" License; Glad is under The MIT License (MIT); GLFW is licensed under the zlib/libpng license, a BSD-like license that allows static linking with closed source software; GLM - (OpenGL mathematical library) is licensed under The Happy Bunny License; ImGUI for the GUI is under the MIT Licence; STB used for texture loading, under MIT License; spdlog is under The MIT License (MIT); ASSIMP is under BSD license; fbx-sdk and we do not distribute, rent, loan, lease, sell, sublicense, or otherwise transfer all or any portion of the Autodesk Materials; Eigen is under Apache License; OZZ is distributed under the MIT License (MIT); Files in Bullet library repository are licences under the zlib license.



External Material

Dynamic Sky

The Dynamic Sky shader (Ferreira 2019), particularly the blue sky and the sun, was adapted. Note, that Volumetric Clouds were developed by ourselves, this section only refers to the sky itself. The sky shader was used a base code for the dynamic sky, that is, sky that has the moving sun. Modifications to this code that were made include extracting hard-coded values, and passing them as parameters.

The code is available in the author's shff Repository, at the URL address:
https://github.com/shff/opengl_sky
The Source code was modified and adapted. The opengl_sky Repository is under the MIT License, which states that, using the code is permitted in any commercial or non-comercial program and allows modifications, and larger works may be distributed under different terms and without source code.

Renderer Starter Code
The Core Renderer System was based on and extended upon the Hazel Game Engine Renderer. Code was used for educational purposes to develop the system and extended it to fit our engine framework. This helped to form a good understanding of industry-standard Graphics architecture. 
The code is available in the official Hazel Repository, at the URL address:
https://github.com/TheCherno/Hazel/tree/master/Hazel/src/Hazel/Renderer

The Source code was modified and adapted. The Hazel Repository is under the Apache License 2.0, which states that, using the code is permitted in any commercial or non-comercial program and allows reproducing and distributing copies of the Work or Derivative Works thereof in any medium, with or without modifications.


