#include "aurora_pch.h"
#include "RenderSystem.h"
#include "glad/glad.h"

namespace Aurora
{
	void RenderSystem::init()
	{
		renderer = new Renderer();
		renderer->initFramebuffer();
	}

	//void RenderSystem::Render(RenderParameters* renderParas)
	//{
	//	renderQueue.push(renderParas);
	//}

	void RenderSystem::Render(RenderParameters& renderParas)
	{
		renderQueue.push(&renderParas);
	}

	void RenderSystem::GetCameraMats(glm::mat4* viewMat, glm::mat4* projMat)
	{
		*viewMat = camera->GetViewMatrix();
		*projMat = camera->GetProjectionMatrix();
	}

	void RenderSystem::GetFrameBufferSize(uint32_t* width, uint32_t* height)
	{
		*width = renderer->m_framebuffer_width;
		*height = renderer->m_framebuffer_height;
	}

	void RenderSystem::render()
	{
		if (camera == nullptr)
		{
			AURORA_LOG_ERROR("Please set main camera first");

			return;
		}

		glBindFramebuffer(GL_FRAMEBUFFER, renderer->m_RendererID);
		// use sky blue #87ceeb colour:
		glClearColor(0.5294, 0.8078, 0.9216, 1);

		renderer->clear();
		// instead of assuming same render mode for everything ...
		//renderer->setDrawingMode(GL_TRIANGLES);

		while (!renderQueue.isEmpty())
		{
			//RenderParameters* paras = renderQueue.front();
			RenderParameters* paras = renderQueue.back();
			//  ... we set render mode from parameters
			renderer->setDrawingMode(paras->material->render_mode);
			//RenderParameters paras = renderQueue.front();
			//renderer->draw(*paras->va, *paras->ib, *paras->shader);
			//renderer->draw(*paras->va, *paras->ib, *paras->material);
			renderer->draw(*paras->va, *paras->ib, *paras->material, paras->us);
			//renderer->draw(*paras.va, *paras.ib, *paras.material, paras.us);
			//delete paras;
			renderQueue.pop();
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}