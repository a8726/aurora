#pragma once

#include "Framework/SampleECS/Component/ComponentBase.h"
#include "Core/StaticReflection.h"

#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"

namespace Aurora
{
	class SpinningComp : public ComponentBase
	{
		DEFINE_PROPERTIES(
			(float) moveSpeed
		)

	public:
		bool rotateX;
		bool rotateY;
		bool rotateZ;

		long double curTime;
		glm::vec3 originPos;

		Transform* tsm;
		virtual void OnEnable(Object* obj) override;
		virtual void OnDisable(Object* obj) override;
		virtual void OnStart(Object* obj) override;
		virtual void Update(Object* obj) override;
		virtual void OnShutdown(Object* obj) override;
		virtual std::string getTypeDes() override;
	};

	class BobbingComp : public ComponentBase
	{
		DEFINE_PROPERTIES(
			(float)moveSpeed
		)

			virtual void OnEnable(Object* obj) override;
		virtual void OnDisable(Object* obj) override;
		virtual void OnStart(Object* obj) override;
		virtual void Update(Object* obj) override;
		virtual void OnShutdown(Object* obj) override;
		virtual std::string getTypeDes() override;
	};




	class PanningComp : public ComponentBase
	{
		DEFINE_PROPERTIES(
			(float)moveSpeed
		)

			virtual void OnEnable(Object* obj) override;
		virtual void OnDisable(Object* obj) override;
		virtual void OnStart(Object* obj) override;
		virtual void Update(Object* obj) override;
		virtual void OnShutdown(Object* obj) override;
		virtual std::string getTypeDes() override;
	};
}