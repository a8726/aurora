#shader vertex
#version 450 core

layout(location = 0) in vec4 in_position;
layout(location = 1) in vec4 in_normal;
layout(location = 2) in vec2 in_texcoords;

out vec3 vertout_normal;
out vec3 vertout_fragment_position;
out vec2 vertout_texture_coords;


//uniform mat4 u_modelviewprojection; // MVP
uniform mat4 u_model;		// M
uniform mat4 u_view;		// V
uniform mat4 u_projection;	// P


void main()
{

    //gl_Position = vec4(in_position.xy, 0., 1.);
    gl_Position = vec4(in_position.xy, 0.999999, 1.); // 1.0 doesn't work for some reason lmao
                                                      // too many 9's rounds up, so be careful

    gl_PointSize = 10.0;

    //vertout_fragment_position = vec3(u_model * in_position);
    vec4 roof_pos = vec4(in_position.x, 1.0, in_position.y, 1.0); // flip Y and Z so that the plane defines the "roof" plane
    // roof pos is in clip space so multiply by inverse view and projection matricies to get back to world space


    vertout_fragment_position = vec3(inverse(u_view) * inverse(u_projection) * vec4(in_position.xy, 0.5, 1.)) / 50.f;
    
    vertout_normal = mat3(transpose(inverse(u_model))) * vec3(in_normal.x, in_normal.y, in_normal.z);
    //vertout_normal = vec3(in_normal.x, in_normal.y, in_normal.z);

    vertout_texture_coords = in_texcoords;

};




















#shader fragment
#version 450 core

//layout(location = 0) in vec2 texcoord;
//layout(location = 1) in vec3 color;
layout(binding = 2) uniform sampler2D colortexture;
layout(binding = 4) uniform sampler3D noiseTexture;

layout(location = 0) out vec4 out_color;
//out float gl_FragDepth;

in vec3 vertout_normal;
in vec3 vertout_fragment_position;
in vec2 vertout_texture_coords;

uniform mat4 u_model;
uniform mat4 u_view; // view matrix is same as lookat matrix
uniform mat4 u_projection;

uniform vec3 u_light_position;
uniform vec3 u_camera_position; // can use this as origin
                                // don't need target if we have lookat matrix already

uniform vec3 iResolution;

uniform vec4 u_color;
uniform float u_time;

// remapping as described by Schneider's chapter in GPU Pro 7
float remap(float val, float old_min, float old_max, float new_min, float new_max) {
    return new_min + (((val - old_min) / (old_max - old_min)) * (new_max - new_min));
}


// returns 6t^5 - 15t^4 + 10t^3
float quintic_fade(float t) {
    return t * t * t * (t * (t * 6.0 - 15.0) + 10.0);
}

// main lookup for density values
float scene(vec3 p) {
    float clearsky = 0.32;

    vec3 timeMod = vec3(u_time * .15, 0., 0.);
    
    // g is low oct noise
    // swizzle lookup to be consistant across y plane
    //float g = texture(noiseTexture, p.xzz * 0.03 + timeMod * 0.1).x;
    float g = texture(colortexture, (p.xz + 0.5) * 0.077 + timeMod.xy * 0.25).x + 0.1
            * texture(colortexture, p.xz * 0.097 + timeMod.xy * 0.25).x + 0.1;
    float f = texture(noiseTexture, p * .575 + timeMod).x;
    //float f = 1. - clamp(remap(texture(noiseTexture, p * 0.27 + timeMod).x, clearsky, 1.0, 0.0, 1.0), 0., 1.);
    f = 2. * f - 1.;
    
    f = mix(f * 0.1 - 2.1, f, g * g) + 0.1;

    //return 1.5 * f - .5 - ((p.y - (2.777 + 4.)) * 1.3);

    if (p.y > 1.5 + 4.)
        //return 0.;
        return 1.5 * f - .5 - ((p.y - (2.777 + 4.)) * 1.3); // noise bounding based on y value
    else
        //return (1.5 * f - .5) - (-(p.y - .9777) * 3.7);
        return 1.5 * f - (-4.64 * (p.y - 5.25));
        //return (1.5 * f - .5) - ( (-p.y + ( (1.5 + 4.) - ( (2.777 + 4.) - (1.5 + 4.) ) ) ) * 1.3);
}

// lookup to get cone sample noise influence
vec3 noiseVec(vec3 p) {
    float a = texture(noiseTexture, p.xyz).x;
    float b = texture(noiseTexture, p.yzz).x;
    float c = texture(noiseTexture, p.zxy).x;
    vec3 v = vec3(a, b, c);
    return v * 2. - 1.;
}

// using Henyey-Greenstein phase function to calculate probabilty of forward scattering
//                   1           1 - g^2
// pHG(theta, g) = ---- ----------------------------
//                 4 PI    1 + g^2 - 2g cos(theta)^1.5
// where theta is the angle between the viewing direction and the sun direction
// and g is an eccentricity
float HG_probability(vec3 view, vec3 sun_dir, float eccentricity) {
    float cos_theta = dot(sun_dir, view);
    return (4. * 3.1416 * (1. - eccentricity * eccentricity)) /
        (1. + eccentricity * eccentricity - (2. * eccentricity * pow(cos_theta, 1.5)));
}

// powdered sugar effect, described by Schneider, is what gives clouds some dark edges even when directly facing light source
float powder_effect(float lightDensity) {
    return 1. - exp(-2. * lightDensity);
}




vec4 volCloud(vec3 uvw, vec3 sun_dir, vec3 cloudCol, vec3 lightCol, vec4 bgCol) {
    float camAngle = 0.;
    float dist = 3.;
    int ray_it = 100;
    int light_it = 8;
    float opacity = 48.;
    cloudCol = vec3(0.25, 0.25, 0.3) + 0.5 * cloudCol;
    float light_opacity = 8.;
    //lightCol = vec3(0.9, 0.7, 0.1);
    lightCol *= 0.25;

    // becomes fragment depth
    // 1.0 gets clipped
    float cloudDepth = 0.999;

    /// raycasting 
    // rays start at origin
    
    // u_camera_position is origin but not in clip space
    // the vector from the camera is glm's front vector is target - origin
    // but the right hand mode flips this before returning it
    // who knows
    
    vec3 origin = u_camera_position;
    float cloud_pseudoheight = 5.f;
    origin.y -= cloud_pseudoheight;

    // GLM lookat is transposed by default
    // not an issue elsewhere bc its used transposed
    //mat4 cam = inverse(u_projection) * transpose(u_view) ;
    //
    //vec3 ot = normalize(cam[2].xyz) * -1.;
    //vec3 left = normalize(cam[0].xyz);
    //vec3 up = normalize(cam[1].xyz);

    vec3 ot = normalize(transpose(u_view)[2].xyz) * -1.;
    vec3 left = normalize(transpose(u_view)[0].xyz);
    vec3 up = normalize(transpose(u_view)[1].xyz);


    /// normalized directional vector from origin to target
    //vec3 ot = normalize(target - origin);
    //vec3 ot = normalize(u_view[2].xyz);
    /// normalized left directional vector
    // supply what we consider the world up vector (y in this case)
    //vec3 left = normalize(cross(vec3(0., 1., 0.), ot));
    //vec3 left = normalize(u_view[0].xyz);
    /// normalized up directional vector
    //vec3 up = normalize(cross(ot, left));
    //vec3 up = normalize(u_view[1].xyz);
    // normalized directional vector from origin to pixel
    //vec3 dir = normalize(uvw.x * left + uvw.y * up + 1. * ot);
    vec3 dir = normalize(mat3(left, up, ot) * normalize(vec3(uvw.xy, 2.5)));
    // ^ these two lines are the same



    // density ray loop things


    // following bounding based on iq's clouds shader:
    float volBasePlane = 1. +4.;
    float volTopPlane = 3.1 +4.;

    float stepToBase = (volBasePlane - origin.y) / dir.y; // stepToBase * dir + origin = P where p.y = volBase
    float stepToTop = (volTopPlane - origin.y) / dir.y;  // stepToTop * dir + origin = P where p.y = volTop

    float stepMin, stepMax;
    if (origin.y < volBasePlane) {
        // if origin is below base plane
        // view is from under clouds
        // closest plane is Base plane
        //if (stepToBase < 0.0) return vec4(bgCol.rrr, 0.999);
        if (stepToBase < 0.0) return vec4(bgCol.rgb, 0.999);
        // if negative, then direction to plane is -dir (bad)

        // min set to dist to nearest plane
        // max set to dist to farthest plane
        stepMin = stepToBase;
        stepMax = stepToTop;
        // idk why this works for y values over 
        //stepMin = stepToTop;
        //stepMax = stepToBase;

    }
    else if (origin.y > volTopPlane) {
        // if origin is above top plane
        // view from above clouds
        // closest plane is top plane

        if (stepToTop < 0.0) return vec4(bgCol.rgb, 0.999);
        // if negative, then direction to plane is -dir (bad)
        
        // uncomment to see red above clouds for debug
        //bgCol = vec4(0.5, .2, .2, 1.);
        
        // min set to dist to nearest plane
        // max set to dist to farthest plane
        stepMin = stepToTop;
        stepMax = stepToBase;

        // if above top plane, clouds are closer than everything else
        cloudDepth = -0.1;
    }
    else { // view from inside clouds

        //bgCol = vec4(0.2, .7, .2, 0.999);
        stepMin = 0.0;
        stepMax = 80.0;
        // set max to the distance to the closest bound
        if (stepToTop > 0.0) stepMax = min(stepMax, stepToTop);
        if (stepToBase > 0.0) stepMax = min(stepMax, stepToBase);

        // if within clouds, clouds are closer than everything else
        cloudDepth = -0.1;
    }


    // end bounding based on iq's shader

    float zTravel = stepMax - stepMin;
    float zStep = zTravel / float(ray_it);
    vec3 rayPoint = origin + stepMin * dir;
    float ditherOffset = texture(noiseTexture, rayPoint.zyx * 0.1).x;
    rayPoint += dir * (ditherOffset * zStep);
    vec3 color = vec3(0.);
    float density = 0.;
    //Transmittence
    float T = 1.;
    float travelDist = stepMin + (ditherOffset * zStep);

    // light loop things
    float lzTravel = zTravel * 0.125;
    float lStep = lzTravel / float(light_it);
    //float lStep = lzTravel * 0.125;
    vec3 light_step = sun_dir * lStep;
    float cone_spread_multiplier = length(light_step);
    float light_density = 0.;
    float lightSample = 0.;

    // ray loop
    for (int i = 0; i < ray_it; i++) {
        density = scene(rayPoint);

        if (density > 0.) {
            density = density / float(ray_it);

            T *= 1. - (density * float(ray_it) * 0.5);
            // if ray_it == 100 we can simplify the above commented lines to:
            //density = density / 2.;
            //T *= 1. - density;
            // idk if that's right actually


            // allows transmittence to be "used up" as it goes through the cloud
            if (T <= .01) break;
            //if (T <= .01) {
            //    clamp(color, 0.1, 1.);
            //    if (length(color) < 0.125) color += max(color * 0.25, vec3(0.045));
            //    break;
            //}

            /// light sampling
            // copy point for use in sampling loop
            vec3 samplePoint = rayPoint + ditherOffset * light_step;
            // sampling loop
            for (int j = 1; j < light_it; j++) {
                samplePoint += light_step;
                +(cone_spread_multiplier * float(i) * noiseVec(samplePoint));

                lightSample = scene(samplePoint);
                if (lightSample > 0.) {
                    light_density += lightSample * 1.0;
                }

            }

            // Beer's law states that
            // E = e^-t
            // where 
            // E is light engergy
            // t is thickness
            float lightEnergy = 0.05 * exp(-1. * light_density) * powder_effect(light_density);
            lightEnergy += 3. * lightEnergy * clamp(HG_probability(dir, sun_dir, 0.2), 0., 1.);
            //float lightEnergy = exp(-1. * light_density);
            vec3 localCol = (cloudCol * opacity + lightCol * lightEnergy * light_opacity) * density * T;

            float farness = length(rayPoint - origin);
            if (farness > 30.) { 
                localCol *= (30. / farness) * (30. / farness);
                // for debug: (highlights area)
                //localCol += vec3(0.5, .2, .2);
            }

            color += localCol;
        }


        rayPoint += dir * zStep;


    }

    if (length(color) < 0.2) cloudDepth = 0.999; // if no color, always max depth

    //add background color
    color += bgCol.rgb;

    // Output
    // store depth as A
    return vec4(color, cloudDepth);

    // here goes nothing

}

/*  Reference:
 *  Title: OpenGL Sky
 *  Author: Silvio Henrique Ferreira
 *  Date: 2019 
 *  Availability: https://github.com/shff/opengl_sky
 *  Type: Tutorial/Notes
 *  Code adapted to render the dynamic blue sky and sun
 */

void main()
{
    out_color = vec4(1.0); // u_color;

    vec2 uv = (2. * gl_FragCoord.xy - iResolution.xy) / iResolution.y;
    //uv = (gl_FragCoord.xy) / iResolution.xy;
    vec3 uvw = vec3(uv, abs(sin(80.6)));
    
    //out_color = vec4(uv, 0., 1.);
    //out_color = vec4(texture(colortexture, uv).rgb, 1.);
    //gl_FragDepth = -0.1;
    //out_color = vec4(texture(noiseTexture, vec3(uv, fract(u_time * 0.15))).rrr, 1.);
    //return;

    //vec3 pos = (vec4(u_camera_position, 1.)).xyz;
    //pos = pos / 50.;
    vec3 pos = vec3(vertout_fragment_position.xy, 0.5);// / 50.f;
    //vec3 pos = uvw;
    // NB: vertout_fragment_position IS NOT gl_FragCoord
    //vec3 fsun = vec3(0.0, 0.5f, 0.f);
    //vec3 fsun = vec3(cos(u_time * 10.0 * 0.01), sin(u_time*10.0*0.01), cos(u_time * 10.0 *0.01));
    vec3 fsun = vec3(cos(10 * 10.0 * 0.01), sin(10 * 10.0 * 0.01), cos(10 * 10.0 * 0.01));
    
    const float Br = 0.0001;
    const float Bm = 0.0005;
    //const float g = 1.00;
    const float g = 0.99;

    const vec3 nitrogen = vec3(0.650, 0.570, 0.475);
    const vec3 Kr = Br / pow(nitrogen, vec3(4.0));
    const vec3 Km = Bm / pow(nitrogen, vec3(0.84));


    //out_color = vec4(texture(colortexture, vertout_texture_coords).rgba);

    float mu = dot(normalize(pos), normalize(fsun));
    //float mu = dot(pos, fsun);

    float rayleigh = 6.0 / (8.0 * 3.14) * (1.0 + mu * mu);
    //float rayleigh = 0.1 / (10.0 * 3.14) * (100.f + mu * mu * mu * mu * mu * mu * mu * mu * mu * mu * mu * mu);

    float sun_disk_size = 0.90f;

    //vec3 mie = (Kr + Km * (1.0 - g * g) / (2.0 + g * g) / pow(1.0 + g * g - 2.0 * g * mu, 1.5)) / (Br + Bm);
    vec3 mie = (Kr + Km * (0.99 - g * g) / (0.98 + g * g) / pow(1.0 + g * g - 2.0 * g * mu, sun_disk_size)) / (Br + Bm);

    float sunrise_duration = 3000.f;
    float sunrise_persistance = 2.f;
    float sunrise_vibrance = 100.f;
    float twilight_duration = 10.f;
    float horizon_cutoff_height = 0.1f;
    float atmosphere_scaling = 0.001f;
    float haziness = 100.f;
    float sunlight_strength = 2.f;
    float sundisk_scaling = 0.4f;
    float night_ambiance = 0.1f;
    float sundisk_pureness = 0.7f;

    float sunset_smoothness_offset = 0.5f;

    //vec3 day_extinction = exp(-exp(-((pos.y + fsun.y * 4.0) * (exp(-pos.y * 16.0) + 0.1) / 80.0) / Br) * (exp(-pos.y * 16.0) + 0.1) * Kr / Br)* exp(-pos.y * exp(-pos.y * 8.0) * 4.0)* exp(-pos.y * 2.0) * 4.0;
    vec3 day_extinction =  exp(-exp(-((pos.y + fsun.y * sunrise_persistance) 
                        * (exp(-pos.y * (100.0/sunrise_vibrance)) + sunset_smoothness_offset) / sunrise_duration) / Br)
                        * (exp(-pos.y * twilight_duration) + sunset_smoothness_offset) * Kr / Br)
                        *  exp(-pos.y * exp(-pos.y * horizon_cutoff_height) * atmosphere_scaling)
                        *  exp(-pos.y * (100.0 / haziness)) * sunlight_strength;

    vec3 night_extinction = vec3(1.f - exp(fsun.y)) * night_ambiance;

    vec3 middleColor = vec3(0.992, 0.369, 0.325) * max((0.5f - abs(fsun.y)), 0.f);// pow(abs(fsun.y), 4.f / 5.f));
    //vec3 middleColor = vec3(0.992, 0.369, 0.325) * (1.f - fsun.y);// pow(abs(fsun.y), 4.f / 5.f));

    vec3 extinction = mix(day_extinction, 
                          night_extinction+middleColor, 
                          -fsun.y * sundisk_scaling + sundisk_pureness);

    vec3 orange_tint = vec3(0.9686, 0.949, 0.8196);
    out_color.rgb = rayleigh * mie * extinction * orange_tint * orange_tint;
    //out_color.rgb = vec3(rayleigh); // *extinction;

    // my clouds :)
    //fsun.y *= -1.;
    float volTopPlane = 3.1;
    if (u_camera_position.y > volTopPlane) {
        // recalculate extinctions with pos.y flipped for passing to clouds
        day_extinction = exp(-exp(-((-pos.y + fsun.y * sunrise_persistance)
            * (exp(pos.y * (100.0 / sunrise_vibrance)) + sunset_smoothness_offset) / sunrise_duration) / Br)
            * (exp(pos.y * twilight_duration) + sunset_smoothness_offset) * Kr / Br)
            * exp(pos.y * exp(pos.y * horizon_cutoff_height) * atmosphere_scaling)
            * exp(pos.y * (100.0 / haziness)) * sunlight_strength;
        vec3 extinction = mix(day_extinction,
            night_extinction + middleColor,
            -fsun.y * sundisk_scaling + sundisk_pureness);
    }
    //vec4 cloud_color = volCloud(uvw, normalize(fsun), extinction * 4. * max(pos.y, 0.0), extinction, out_color);
    vec4 cloud_color = volCloud(uvw, normalize(fsun), extinction * 2. * max(pos.y, 0.0), extinction, out_color);
    
    out_color = vec4(cloud_color.rgb, 1.);
    gl_FragDepth = cloud_color.a; // 1.0 gets clipped
};
