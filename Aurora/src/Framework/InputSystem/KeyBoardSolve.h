#pragma once

#define KEYBOARD_SOLVE(keycode) \
case GLFW_KEY_ ## keycode: \
{ \
	if (action == GLFW_PRESS) \
	{ \
		EventSystem::GetInstance().BroadcastEvent(Key_ ## keycode ## _Press); \
		InputSystem::GetInstance().SetKeyKeep(Key_ ## keycode ## _Keep, true); \
	} \
	else if (action == GLFW_RELEASE) \
	{ \
		EventSystem::GetInstance().BroadcastEvent(Key_ ## keycode ## _Release); \
		InputSystem::GetInstance().SetKeyKeep(Key_ ## keycode ## _Keep, false); \
	} \
	break; \
} \

KEYBOARD_SOLVE(Q)
KEYBOARD_SOLVE(W)
KEYBOARD_SOLVE(E)
KEYBOARD_SOLVE(R)
KEYBOARD_SOLVE(T)
KEYBOARD_SOLVE(Y)
KEYBOARD_SOLVE(U)
KEYBOARD_SOLVE(I)
KEYBOARD_SOLVE(O)
KEYBOARD_SOLVE(P)
KEYBOARD_SOLVE(A)
KEYBOARD_SOLVE(S)
KEYBOARD_SOLVE(D)
KEYBOARD_SOLVE(F)
KEYBOARD_SOLVE(G)
KEYBOARD_SOLVE(H)
KEYBOARD_SOLVE(J)
KEYBOARD_SOLVE(K)
KEYBOARD_SOLVE(L)
KEYBOARD_SOLVE(Z)
KEYBOARD_SOLVE(X)
KEYBOARD_SOLVE(C)
KEYBOARD_SOLVE(V)
KEYBOARD_SOLVE(B)
KEYBOARD_SOLVE(N)
KEYBOARD_SOLVE(M)

#define KEYBOARD_SOLVE