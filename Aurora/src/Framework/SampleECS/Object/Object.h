#pragma once

#include "Framework/SampleECS/Component/ComponentBase.h"
//#include "../properties/src/Properties.h"
//#include "../../../../thirdparty/properties/src/Properties.h"
#include "Core/Renderer/Renderer.h"
#include "Core/Renderer/VertexBufferLayout.h"
#include "Framework/EngineGuiSystem/InspectorPanel.h"

#include "Core/Renderer/openglMacros.h"

namespace Aurora
{

	class Object
	{
	public:
		Object() = default;
		virtual ~Object()
		{
			for (size_t i = 0; i < comps.size(); i++)
			{
				delete(getComp(i));
			}
		}

		/// <summary>
		/// add component
		/// </summary>
		void Attach(ComponentBase* comp);

		/// <summary>
		/// add component
		/// </summary>
		void Detach(ComponentBase* comp);

		/// <summary>
		/// return typeId
		/// </summary>
		virtual const char* getTypeDes() = 0
		{
			return "Object";
		}

		template <typename T>
		T* GetComponent(unsigned int compIndex)
		{
			return static_cast<T*>(getComp(compIndex));
		}

		std::string name = "Object";
		// default to GL_TRIANGLE, since .obj files use that
		int	 render_mode = GL_TRIANGLES;
		std::list<Object*> children = std::list<Object*>();
	protected:
		ComponentBase* getComp(unsigned int compIndex);
		std::list<ComponentBase*> comps;

		/// <summary>
		/// push this object to render queue and render it
		/// </summary>
		virtual void render() = 0;

		/// <summary>
		/// update all components
		/// </summary>
		void update();

		friend class ObjectStore;
		friend class InspectorPanel;

	};

	class ObjectStore
	{
	public:
		//ObjectStore() = default;
		virtual ~ObjectStore()
		{
			for (size_t i = 0; i < objs.size(); i++)
			{
				delete(Get(i));
			}
		}

		void RegisterObject(Object* obj)
		{
			objs.push_back(obj);
		}

		void UnregisterObject(Object* obj)
		{
			objs.remove(obj);
		}

		void Update()
		{
			for (auto it = objs.begin(); it != objs.end(); it++)
			{
				(*it)->update();
			}
		}

		void Render()
		{
			for (auto it = objs.begin(); it != objs.end(); it++)
			{
				(*it)->render();
			}
		}

		size_t size()
		{
			return objs.size();
		}

		Object* operator[](unsigned int index)
		{
			if (index >= objs.size())
			{
				AURORA_LOG_ERROR("Index over size!");
			}

			unsigned int i = 0;
			auto it = objs.begin();
			for (; it != objs.end(), i <= index; it++, i++)
			{
				if (i == index)
				{
					return *it;
				}
			}
		}

		Object* Get(unsigned int index)
		{
			if (index >= objs.size())
			{
				AURORA_LOG_ERROR("Index over size!");
			}

			unsigned int i = 0;
			auto it = objs.begin();
			for (; it != objs.end(), i <= index; it++, i++)
			{
				if (i == index)
				{
					return *it;
				}
			}
		}

	protected:
		std::list<Object*> objs;
	};
}