#include "aurora_pch.h"
#include "SpinningComp.h"

#include "Framework/SampleECS/Object/Object.h"

#include <math.h>       /* sin */

void Aurora::SpinningComp::OnEnable(Object* obj)
{
	obj->GetComponent<Transform>(0)->Rotation().x = 10.f;
	obj->GetComponent<Transform>(0)->Rotation().y = 10.f;
	obj->GetComponent<Transform>(0)->Rotation().z = 10.f;

}

void Aurora::SpinningComp::OnDisable(Object* obj)
{
}

void Aurora::SpinningComp::OnStart(Object* obj)
{
	tsm = obj->GetComponent<Transform>(0);
	originPos = tsm->Position();

	moveSpeed = 1.0f;
	rotateX = true;
	rotateY = true;
	rotateZ = true;
}

void Aurora::SpinningComp::Update(Object* obj)
{
	if (rotateX)
	{
		tsm->Rotation().x += moveSpeed;
	}

	if (rotateY)
	{
		tsm->Rotation().y += moveSpeed;
	}

	if (rotateZ)
	{
		tsm->Rotation().z += moveSpeed;
	}

	//float dt = 1.f / ImGui::GetIO().Framerate;
	//curTime += dt;
	//tsm->Position().y = originPos.y + std::sin(curTime) * 0.03f;

}

void Aurora::SpinningComp::OnShutdown(Object* obj)
{
}

std::string Aurora::SpinningComp::getTypeDes()
{
	return "SpinningComp";
}




// ------------------------------------------------------------------ //



void Aurora::BobbingComp::OnEnable(Object* obj)
{
	obj->GetComponent<Transform>(0)->Rotation().x = 10.f;
	obj->GetComponent<Transform>(0)->Rotation().y = 10.f;
	obj->GetComponent<Transform>(0)->Rotation().z = 10.f;

}

void Aurora::BobbingComp::OnDisable(Object* obj)
{
}

void Aurora::BobbingComp::OnStart(Object* obj)
{
	moveSpeed = 0.7f;
}

void Aurora::BobbingComp::Update(Object* obj)
{
	//moveSpeed += 0.01f;
	//obj->GetComponent<Transform>(0)->Rotation().x += moveSpeed;
	//obj->GetComponent<Transform>(0)->Position().y = 0.13f + 0.005f * sin(ImGui::GetTime()*4);
	obj->GetComponent<Transform>(0)->Position().y = 0.13f + 0.005f * sin(ImGui::GetTime()*2);
	//obj->GetComponent<Transform>(0)->Rotation().z += moveSpeed;
}

void Aurora::BobbingComp::OnShutdown(Object* obj)
{
}

std::string Aurora::BobbingComp::getTypeDes()
{
	return "BobbingComp";
}






// ------------------------------------------------------------------- //
void Aurora::PanningComp::OnEnable(Object* obj)
{
	obj->GetComponent<Transform>(0)->Rotation().x = 10.f;
	obj->GetComponent<Transform>(0)->Rotation().y = 10.f;
	obj->GetComponent<Transform>(0)->Rotation().z = 10.f;

}

void Aurora::PanningComp::OnDisable(Object* obj)
{
}

void Aurora::PanningComp::OnStart(Object* obj)
{
	moveSpeed = 0.02f;
}

void Aurora::PanningComp::Update(Object* obj)
{
	//moveSpeed += 0.01f;
	obj->GetComponent<Transform>(0)->Position().x += moveSpeed;
	//obj->GetComponent<Transform>(0)->Position().y = 0.13f + 0.005f * sin(ImGui::GetTime()*4);
	//obj->GetComponent<Transform>(0)->Position().y = 0.13f + 0.005f * sin(ImGui::GetTime() * 2);
	//obj->GetComponent<Transform>(0)->Rotation().z += moveSpeed;
}

void Aurora::PanningComp::OnShutdown(Object* obj)
{
}

std::string Aurora::PanningComp::getTypeDes()
{
	return "PanningComp";
}
