#include "aurora_pch.h"
#include "Time.h"

namespace Aurora
{
	time_t Time::GetSystemTime()
	{
		return time(NULL);
	}

	time_t Time::GetRuntimeTime()
	{
		return startFrom - time(NULL);
	}

	time_t Time::GetDeltaTime()
	{
		return lastFrameDeltaTime;
	}

	time_t Time::GetFixedDeltaTime()
	{
		return fixedDeltaTime;
	}

	void Time::init()
	{
		startFrom = time(NULL);
		fixedDeltaTime = 0.15f;
	}

	void Time::update()
	{
	}
}
