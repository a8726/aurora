#pragma once

#include "Core/Singleton.h"
#include "RenderQueue.h"
#include "../Global.h"
#include "Framework/SampleECS/Component/DefaultComponents/Camera.h"

namespace Aurora
{
	class RenderSystem : public Singleton<RenderSystem>
	{
	public:
		void init();
		//void Render(RenderParameters* renderParas);
		void Render(RenderParameters& renderParas);

		void SetMainCamera(Camera* _camera) { camera = _camera; };
		Camera* GetMainCamera() { return camera; };

		void GetCameraMats(glm::mat4* viewMat, glm::mat4* projMat);
		inline glm::vec3 GetCameraPos() { return camera->GetPosition(); };

		void GetFrameBufferSize(uint32_t* width, uint32_t* height);

		Renderer* GetRenderer() { return renderer; };
	private:
		void render();
		RenderQueue renderQueue;
		Renderer* renderer;
		Camera* camera;

		friend class Global;
	};
}
