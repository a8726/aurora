#pragma once

#include "Core/Singleton.h"
#include "Core/Log.h"
#include "EventCallBack.h"

namespace Aurora
{

	class Event
	{

	public:
		Event(Closure* _closure) : closure(_closure)
		{
			id = eventCount++;
		}

		~Event()
		{
			eventCount--;
		}

		inline uint64 getId() { return id; }
		static uint64 eventCount;

		inline void invoke() { closure->Invoke(); };
		inline void invoke(EventResponseTypeBase* response) { closure->Invoke(response); };

	protected:
		uint64 id;
		Closure* closure;
	};

	class EventManager
	{
		
	public:
		EventManager(uint64 _eventTypeDes):eventTypeDes(_eventTypeDes)
		{
			eventTypeManagerCount++;
		}

		~EventManager()
		{
			eventTypeManagerCount--;
		}

		std::string getTypeDes() { return std::to_string(eventTypeDes); }

		uint64 RegisterEvent(Event* event);
		void UnregisterEvent(uint64 id);

		void TriggerEvent(uint64 id);
		void TriggerEvent(uint64 id, EventResponseTypeBase* response);

		void BroadcastEvent();
		void BroadcastEvent(EventResponseTypeBase* response);

		static uint64 getCount() { return eventTypeManagerCount; }

	private:
		uint64 eventTypeDes;
		static uint64 eventTypeManagerCount;

		std::map<uint64, Event*> events;

	};

	class EventSystem : public Singleton<EventSystem>
	{
	public:

		void init();

		/// <summary>
		/// register event to event manager
		/// </summary>
		/// <param name="eventType"> event manager id </param>
		/// <param name="event"> event </param>
		/// <returns> event id </returns>
		uint64 RegisterEvent(uint64 eventType, Event* event);

		/// <summary>
		/// unregister event from event manager
		/// </summary>
		/// <param name="eventType"> event manager id </param>
		/// <param name="id"> event id </param>
		void UnregisterEvent(uint64 eventType, uint64 id);

		/// <summary>
		/// trigger an event
		/// </summary>
		/// <param name="eventType"> event manager id </param>
		/// <param name="id"> event id </param>
		void TriggerEvent(uint64 eventType, uint64 id);

		/// <summary>
		/// trigger an event with response
		/// </summary>
		/// <param name="eventType"> event manager id </param>
		/// <param name="id"> event id </param>
		/// <param name="response"> callback's parameter </param>
		void TriggerEvent(uint64 eventType, uint64 id, EventResponseTypeBase* response);

		/// <summary>
		/// broadcast an event manager
		/// </summary>
		/// <param name="eventType"> event manager id </param>
		void BroadcastEvent(uint64 eventType);

		/// <summary>
		/// broadcast an event manager with response
		/// </summary>
		/// <param name="eventType"> event manager id </param>
		/// <param name="response"> callback's parameter </param>
		void BroadcastEvent(uint64 eventType, EventResponseTypeBase* response);

		/// <summary>
		/// get the description of an event manager
		/// </summary>
		/// <param name="eventType"> event manager id </param>
		/// <returns> description of this event manager </returns>
		std::string getTypeDes(uint64 eventType);

		/// <summary>
		/// create a new event manager
		/// </summary>
		/// <returns> event manager id </returns>
		static uint64 CreateNewEventType()
		{
			return EventManager::getCount() + 1;
		}

	private:
		std::map<uint64, EventManager*> eventTypes;

		EventManager* getEventManager(uint64 eventType);
	};
}