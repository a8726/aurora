#pragma once

#ifndef EVENT_CALLBACK
#define EVENT_CALLBACK

#include "Core/Closure.h"

namespace Aurora
{
	class EventResponseTypeBase : public Response
	{
	public:
		virtual void init() override {};
	};

	class EventCallBackDefault : public Closure
	{
	public:
		typedef void (*Callback)();

		EventCallBackDefault(Callback _callback) : callback(_callback) { }
		~EventCallBackDefault() = default;

		virtual void Invoke()
		{
			callback();
		}

		virtual void Invoke(Response* response)
		{

		}

	private:
		Callback callback;

	};

	class EventCallBackDefaultWithResponse : public Closure
	{
	public:
		typedef void (*Callback)(Response* response);

		EventCallBackDefaultWithResponse(Callback _callback) : callback(_callback) { }
		~EventCallBackDefaultWithResponse() = default;

		virtual void Invoke()
		{

		}

		virtual void Invoke(Response* response)
		{
			callback(response);
		}

	private:
		Callback callback;

	};

	template <typename T>
	class EventCallBack : public Closure
	{
	public:
		typedef void (T::* Callback)();

		EventCallBack(T* _obj, Callback _callback) : obj(_obj), callback(_callback) { }
		~EventCallBack() = default;

		virtual void Invoke()
		{
			(obj->*callback)();
		}

		virtual void Invoke(Response* response)
		{

		}

	private:
		T* obj;
		Callback callback;

	};

	template <typename T>
	class EventCallBackWithResponse : public Closure
	{
	public:
		typedef void (T::* Callback)(Response* response);

		EventCallBackWithResponse(T* _obj, Callback _callback) : obj(_obj), callback(_callback) { }
		~EventCallBackWithResponse() = default;

		virtual void Invoke()
		{

		}

		virtual void Invoke(Response* response)
		{
			(obj->*callback)(response);
		}

	private:
		T* obj;
		Callback callback;
	};

	Closure* NewEventCallback(void (*callback)());

	Closure* NewEventCallback(void (*callback)(Response* response));

	template <typename T>
	Closure* NewEventCallback(T* obj, void (T::* callback)())
	{
		return new EventCallBack<T>(obj, callback);
	}

	template <typename T>
	Closure* NewEventCallback(T* obj, void (T::* callback)(Response* response))
	{
		return new EventCallBackWithResponse<T>(obj, callback);
	}
}

#endif // !EVENT_CALLBACK