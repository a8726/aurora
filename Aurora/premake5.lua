project "Aurora"
	language "C++"
	cppdialect "c++17"
	architecture "x86_64"
	kind "StaticLib"
	staticruntime "off"
	
	location "%{wks.location}/build"

	targetdir (targetOutputDir .. "/%{prj.name}")
	objdir (objOutputDir .. "/%{prj.name}")

	pchheader "aurora_pch.h"
	pchsource "src/aurora_pch.cpp"

	files
	{
		"src/**.h",
		"src/**.cpp",

		"**.lua",
	}

	includedirs
	{
		"src",
		"src/Functions",
		thirdparty .. "/GLFW/include",
		thirdparty .. "/ImGui",
		thirdparty .. "/spdlog/include",

		thirdparty .. "/Glad/include",

		thirdparty .. "/Eigen/Eigen",

		thirdparty.. "/glm",

		thirdparty.. "/assimp/include",
		
		thirdparty.. "/FBXSDK/include",
		thirdparty.. "/ozz/include",
	}

	links
	{
		"GLFW",
		"ImGui",
		"spdlog",
		"Glad",
		"Eigen",
		"assimp",
		"ozz",
		
--		thirdparty.. "/fbxsdk/lib/debug/libfbxsdk-md.lib",
--		thirdparty.. "/fbxsdk/lib/debug/libxml2-md.lib",
--		thirdparty.. "/fbxsdk/lib/debug/zlib-md.lib",
	}

	filter "configurations:Debug"
		defines "AURORA_GENERATE_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "AURORA_GENERATE_RELEASE"
		runtime "Release"
		optimize "off"