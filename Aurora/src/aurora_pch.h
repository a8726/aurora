/*precompile header set*/
#pragma once

namespace Aurora
{
	typedef unsigned long long uint64;
}

#include <iostream>
#include <memory>
#include <utility>
#include <algorithm>
#include <functional>
#include <stdlib.h>

#include <string>
#include <queue>
#include <stack>
#include <sstream>
#include <array>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>

#include <math.h>

#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cstring>
#include <cmath>
#include <cfloat>
#include <climits>
#include <exception>

#include <ctime>
#include <filesystem>

#include "Framework/I18N/I18N.h"
#include "Core/Log.h"

#ifdef _WIN32

/// windows system
#include <Windows.h>
#include <Commdlg.h>
///

#endif // DEBUG
