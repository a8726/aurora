#pragma once

#ifndef CLOSURE
#define CLOSURE

namespace Aurora
{
	class Response
	{
	public:
		virtual void init() = 0;
	};

	class Closure
	{
	public:
		Closure() = default;
		virtual ~Closure() = default;

		virtual void Invoke() = 0;
		virtual void Invoke(Response* response) = 0;
	};
}

#endif

