#pragma once

#include "MemoryTracker.h"

#ifndef OVERRIDE_ALLOCATION_FUNCTION
#define OVERRIDE_ALLOCATION_FUNCTION

void* operator new(size_t size)
{
	if (Aurora::MemoryTracker::IsInit())
	{
		Aurora::MemoryTracker::GetInstance().Allocate(size);
	}

	return malloc(size);
}

void* operator new[](size_t size)
{
	if (Aurora::MemoryTracker::IsInit())
	{
		Aurora::MemoryTracker::GetInstance().Allocate(size);
	}

	return malloc(size);
}

void operator delete(void* p, size_t size)
{
	if (Aurora::MemoryTracker::IsInit())
	{
		Aurora::MemoryTracker::GetInstance().Free(size);
	}

	free(p);
}

void operator delete[](void* p, size_t size)
{
	if (Aurora::MemoryTracker::IsInit())
	{
		Aurora::MemoryTracker::GetInstance().Free(size);
	}

	free(p);
}

#endif
