#include "aurora_pch.h"
#include "ObjMoveTest.h"
#include "Framework/SampleECS/Object/Object.h"
#include "Framework/InputSystem/InputSystem.h"
#include "Framework/EventSystem/EventSystem.h"

void ObjMoveTest::OnEnable(Aurora::Object* obj)
{
}

void ObjMoveTest::OnDisable(Aurora::Object* obj)
{
}

void ObjMoveTest::OnStart(Aurora::Object* obj)
{
	transform = obj->GetComponent<Aurora::Transform>(0);

	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_I_Press, Aurora::NewEventCallback<ObjMoveTest>(this, &ObjMoveTest::keyIKeepCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_I_Keep, Aurora::NewEventCallback<ObjMoveTest>(this, &ObjMoveTest::keyIKeepCallback));

	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_J_Press, Aurora::NewEventCallback<ObjMoveTest>(this, &ObjMoveTest::keyJKeepCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_J_Keep, Aurora::NewEventCallback<ObjMoveTest>(this, &ObjMoveTest::keyJKeepCallback));

	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_K_Press, Aurora::NewEventCallback<ObjMoveTest>(this, &ObjMoveTest::keyKKeepCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_K_Keep, Aurora::NewEventCallback<ObjMoveTest>(this, &ObjMoveTest::keyKKeepCallback));

	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_L_Press, Aurora::NewEventCallback<ObjMoveTest>(this, &ObjMoveTest::keyLKeepCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_L_Keep, Aurora::NewEventCallback<ObjMoveTest>(this, &ObjMoveTest::keyLKeepCallback));

	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_U_Press, Aurora::NewEventCallback<ObjMoveTest>(this, &ObjMoveTest::keyUKeepCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_U_Keep, Aurora::NewEventCallback<ObjMoveTest>(this, &ObjMoveTest::keyUKeepCallback));

	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_O_Press, Aurora::NewEventCallback<ObjMoveTest>(this, &ObjMoveTest::keyOKeepCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_O_Keep, Aurora::NewEventCallback<ObjMoveTest>(this, &ObjMoveTest::keyOKeepCallback));
}

void ObjMoveTest::Update(Aurora::Object* obj)
{
}

void ObjMoveTest::OnShutdown(Aurora::Object* obj)
{
}

void ObjMoveTest::keyIKeepCallback()
{
	transform->Position() += transform->Front() * moveSpeed * 0.15f;
}

void ObjMoveTest::keyJKeepCallback()
{
	transform->Position() += transform->Right() * -moveSpeed * 0.15f;
}

void ObjMoveTest::keyKKeepCallback()
{
	transform->Position() += transform->Front() * -moveSpeed * 0.15f;
}

void ObjMoveTest::keyLKeepCallback()
{
	transform->Position() += transform->Right() * moveSpeed * 0.15f;
}

void ObjMoveTest::keyUKeepCallback()
{
	transform->Position() += transform->Up() * moveSpeed * 0.15f;
}

void ObjMoveTest::keyOKeepCallback()
{
	transform->Position() += transform->Up() * -moveSpeed * 0.15f;
}