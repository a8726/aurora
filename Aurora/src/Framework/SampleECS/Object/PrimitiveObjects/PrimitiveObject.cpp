#include "aurora_pch.h"
#include "PrimitiveObject.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
//#include "Core/Renderer/Texture.h"
#include "../Core/ResourceManager/ResourceTypes/Texture.h"

#include "Framework/SampleECS/System/RenderSystem/RenderSystem.h"
#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"

#include "Core/ResourceManager/ResourceManager.h"

namespace Aurora
{
	VertexBufferLayout Aurora::PrimitiveObject::setVertexBufferLayout()
	{
		VertexBufferLayout layout;
		// since the vertices have attributes, ...
		// ... we let OpenGL know what our layout right now is:
		// position atribute size(x, y, z) = vec3:
		layout.push<float>(3);
		// normal attribute size(x, y, z) = vec3:
		layout.push<float>(3);
		// tangent attribute size(x, y, z) = vec3:
		layout.push<float>(3);
		// texture coordinate attribute size(u, v) = vec2
		layout.push<float>(2);

		return layout;
	}

	VertexArray* PrimitiveObject::getVAO()
	{
		return m_vertex_array;
	}

	IndexBuffer* PrimitiveObject::getEBO()
	{
		return m_index_buffer;
	}

	void PrimitiveObject::addAttributeData(const glm::vec3& point3d)
	{
		surface_data.point_vector.push_back(point3d.x);
		surface_data.point_vector.push_back(point3d.y);
		surface_data.point_vector.push_back(point3d.z);
		this->addNumElements(3);
	}
	void PrimitiveObject::addAttributeData(const float& x, const float& y, const float& z)
	{
		surface_data.point_vector.push_back(x);
		surface_data.point_vector.push_back(y);
		surface_data.point_vector.push_back(z);
		this->addNumElements(3);
	}
	void PrimitiveObject::addAttributeData(const float& x, const float& y)
	{
		surface_data.point_vector.push_back(x);
		surface_data.point_vector.push_back(y);
		this->addNumElements(2);
	}

	void PrimitiveObject::endTriangle()
	{
		// in GL_TRIANGLE_STRIP - break up the triangle drawing
		surface_data.index_vector.push_back(65535);
	}

	void PrimitiveObject::addTriangle(const glm::uvec3& triangle_indices)
	{
		surface_data.index_vector.push_back(triangle_indices.x);
		surface_data.index_vector.push_back(triangle_indices.y);
		surface_data.index_vector.push_back(triangle_indices.z);
		this->addNumIndices(1);
	}

	void PrimitiveObject::addTriangle(const float& x, const float& y, const float& z)
	{
		surface_data.index_vector.push_back(x);
		surface_data.index_vector.push_back(y);
		surface_data.index_vector.push_back(z);
		this->addNumIndices(1);
	}

	void PrimitiveObject::addSquare(const unsigned& x, const unsigned& y, const unsigned& z, const unsigned& w)
	{
		surface_data.index_vector.push_back(x);
		surface_data.index_vector.push_back(y);
		surface_data.index_vector.push_back(z);
		surface_data.index_vector.push_back(w);
		surface_data.index_vector.push_back(65535);
		this->addNumIndices(1);
	}

	void PrimitiveObject::addLine(const float& x, const float& y)
	{
		surface_data.index_vector.push_back(x);
		surface_data.index_vector.push_back(y);
		this->addNumIndices(1);
	}

	void PrimitiveObject::addNumPoints(unsigned to_add)
	{
		surface_data.num_points += to_add;
	}

	void PrimitiveObject::setNumPoints(unsigned to_set)
	{
		surface_data.num_points = to_set;
	}

	void PrimitiveObject::addNumElements(unsigned to_add)
	{
		surface_data.num_elements += to_add;
	}
	void PrimitiveObject::setNumElements(unsigned to_set)
	{
		surface_data.num_elements = to_set;
	}
	void PrimitiveObject::addNumIndices(unsigned to_add)
	{
		surface_data.num_triangles += to_add;
	}
	void PrimitiveObject::setNumIndices(unsigned to_set)
	{
		surface_data.num_triangles = to_set;
	}
}
