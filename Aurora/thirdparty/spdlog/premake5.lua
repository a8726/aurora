project "spdlog"
    kind "StaticLib"
    language "C++"
	staticruntime "off"

	location "%{wks.location}/build"

    targetdir (targetOutputDir .. "/%{prj.name}")
	objdir (objOutputDir .. "/%{prj.name}")

    files
	{
		thirdparty .. "/spdlog/include/spdlog/**.cpp",
		thirdparty .. "/spdlog/include/spdlog/**.h",

		thirdparty .. "/spdlog/src/**.cpp",
		thirdparty .. "/spdlog/src/**.h",

		"**.lua",
	}

    includedirs
    {
        thirdparty .. "/spdlog/include",
		thirdparty .. "/spdlog/src"
    }

	defines
	{
		"SPDLOG_COMPILED_LIB",
	}

    filter "system:windows"
		systemversion "latest"
		cppdialect "C++17"
		staticruntime "On"

    filter "configurations:Debug"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		runtime "Release"
		optimize "off"
