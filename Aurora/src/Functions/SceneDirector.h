#pragma once

#include "Framework/SampleECS/Component/ComponentBase.h"
#include "../thirdparty/glm/glm.hpp"
#include "Core/StaticReflection.h"

#include "Framework/SampleECS/Object/PrimitiveObjects/Plane.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/Cube.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/Sphere.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/PrimitiveObjectProperties.h"
#include "Framework/SampleECS/Object/DefaultObjects/ModelObject.h"
#include "exampleObj.h"
#include "Framework/SampleECS/Component/DefaultComponents/Camera.h"
#include "Framework/SampleECS/Component/DefaultComponents/RendererComponent.h"
#include "Framework/MotionSystem/MotionSystem.h"

namespace Aurora
{
	class SceneDirector : public ComponentBase
	{

		// Inherited via ComponentBase
		virtual void OnEnable(Object* obj) override;
		virtual void OnDisable(Object* obj) override;
		virtual void OnStart(Object* obj) override;
		virtual void Update(Object* obj) override;
		virtual void OnShutdown(Object* obj) override;
		virtual std::string getTypeDes() override;
		
		void RegisterBasicScene(double aTime, void (Aurora::SceneDirector::*aCallback)())
		{
			m_basic_queue.emplace_back(std::make_tuple(aTime, aCallback));
		}
		void RegisterTransformScene(double aTime, void (Aurora::SceneDirector::*aCallback)(float, float, float), float x, float y, float z)
		{
			m_transform_queue.emplace_back(std::make_tuple(aTime, aCallback, x, y, z));
		}
		void RegisterTransformScaleScene(double aTime, void (Aurora::SceneDirector::* aCallback)(glm::vec3, glm::vec3), glm::vec3 aTransform, glm::vec3 aScale)
		{
			m_transform_scale_queue.emplace_back(std::make_tuple(aTime, aCallback, aTransform, aScale));
		}

		std::deque<std::tuple<double, void (SceneDirector::*)()>> m_basic_queue;
		std::deque<std::tuple<double, void (SceneDirector::*)(float, float, float), float, float, float> > m_transform_queue;
		std::deque<std::tuple<double, void (SceneDirector::*)(glm::vec3, glm::vec3), glm::vec3, glm::vec3> > m_transform_scale_queue;
		//std::vector<std::tuple<double, void (SceneDirector::*)(glm::vec3), glm::vec3> > m_transform_queue;

		// all with spinning
		Sphere* sphere_normal;
		ModelObject* teapot_normal;
		Cube* cube_normal;
		Cube* cube_normal_texture1;
		Cube* cube_normal_texture2;
		Cube* cube_normal_texture3;
		ModelObject* hand;

		Plane* floor;
		Plane* water;
		ModelObject* city;
		ModelObject* terrain;

		exampleObj* camera;
		exampleObj* anim;
		bool startZoom = false;

		private:
			void makeCamera();
			void makeSphere();
			void makeCube();
			void makeTeapot();
			void makeHand();
			void makeAnim();

			void makeFloor();
			void makeWater();
			void makeCity();
			void makeTerrain();

			void init();

			void setupWater(float x, float y, float z);
			void setupCity(float x, float y, float z);
			void setupTerrain(float x, float y, float z);

			void setupSphereOnHand(float x, float y, float z);
			void setupTeapotOnHand(float x, float y, float z);
			void setupLightOnHand(float x, float y, float z);
			void setupCubeNormalOnHand(float x, float y, float z);
			void setupCubeNormalTex1OnHand(float x, float y, float z);
			void setupCubeNormalTex2OnHand(float x, float y, float z);
			void setupCubeNormalTex3OnHand(float x, float y, float z);

			void setupAnimiInFrontHand(float x, float y, float z);
			void setupCamBackAnim(float x, float y, float z);

			void camZoom();

			void setupFirstScene();
			void setupSecondScene(float x, float y, float z);
			void setupThirdScene(glm::vec3 aTransform, glm::vec3 aScale);
			void setupFourthScene(float x, float y, float z);
			//std::unordered_map<double, void (SceneDirector::*)()> m_scenes;
			//std::tuple<double, void (SceneDirector::*)()> m_basic_scenes;
			//std::tuple<double, void (SceneDirector::*)(glm::vec3), glm::vec3> m_transform_scenes;

			DEFINE_PROPERTIES(
				(double) curTime
			)
	};
}

