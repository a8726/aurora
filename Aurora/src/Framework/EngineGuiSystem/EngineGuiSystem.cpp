#include "aurora_pch.h"

// Stdlib
#include <codecvt>

// Third party
#include "glad/glad.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

// Custom
#include "Core/Log.h"
#include "EngineGuiSystem.h"
#include "Framework/InputSystem/InputSystem.h"
#include "Framework/EventSystem/EventSystem.h"

#include "Core/Renderer/VertexBufferLayout.h"
#include "Core/ResourceManager/ResourceManager.h"
#include "Core/ResourceManager/ResourceTypes/Model.h"
#include "Framework/SampleECS/Object/DefaultObjects/ModelObject.h"
#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"
#include "Framework/SampleECS/Component/DefaultComponents/RendererComponent.h"

#include <codecvt>


#include "GuiTypeSupport.h"
#include "Framework/SampleECS/Object/Skybox/Skybox.h"

#include "Functions/TerrainMaker.h"

namespace Aurora
{

    // helper function
    void RecursiveDirectoryTree(const std::string& path, std::string& load_string)
    {
        ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow
            | ImGuiTreeNodeFlags_OpenOnDoubleClick
            | ImGuiTreeNodeFlags_SpanAvailWidth;

        for (const auto& entry : std::filesystem::directory_iterator(path))
        {
            std::string entry_path = entry.path().string();
            std::string entry_name = entry_path.substr(entry_path.find_last_of('\\') + 1);

            // leaf node
            if (!std::filesystem::is_directory(entry_path))
            {
                bool selection = false;
                ImGui::Selectable(entry_name.c_str(), &selection, ImGuiSelectableFlags_AllowDoubleClick);
                if (ImGui::IsMouseDoubleClicked(0) && selection)
                {
                    load_string = entry_path;
                }
            }
            else // directory node
            {
                if (ImGui::TreeNodeEx(entry_name.c_str(), flags))
                {
                    RecursiveDirectoryTree(entry_path, load_string);
                    ImGui::TreePop();
                }
            }
        }
    }


    EngineGuiSystem::EngineGuiSystem()
    {
        hierarchyPanel = new HierarchyPanel();
        inspectorPanel = new InspectorPanel();
    }

    void EngineGuiSystem::RenderDockspace(Renderer* renderer)
        //, Aurora::WindowsWindow* window)
    {
        static bool dockspace_open = true;
        static bool opt_fullscreen = true;
        static bool opt_padding = false;
        static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;

        // We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
        // because it would be confusing to have two docking targets within each others.
        ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
        if (opt_fullscreen)
        {
            const ImGuiViewport* viewport = ImGui::GetMainViewport();
            ImGui::SetNextWindowPos(viewport->WorkPos);
            ImGui::SetNextWindowSize(viewport->WorkSize);
            ImGui::SetNextWindowViewport(viewport->ID);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
            window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize |
                ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
        }
        else
        {
            dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode;
        }

        // When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background
        // and handle the pass-thru hole, so we ask Begin() to not render a background.
        if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
            window_flags |= ImGuiWindowFlags_NoBackground;

        // Important: note that we proceed even if Begin() returns false (aka window is collapsed).
        // This is because we want to keep our DockSpace() active. If a DockSpace() is inactive,
        // all active windows docked into it will lose their parent and become undocked.
        // We cannot preserve the docking relationship between an active window and an inactive docking, otherwise
        // any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
        if (!opt_padding)
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));


        ImGui::Begin("DockSpace Demo", &dockspace_open, window_flags);
        {
            if (!opt_padding)
                ImGui::PopStyleVar();

            if (opt_fullscreen)
                ImGui::PopStyleVar(2);

            // Submit the DockSpace
            ImGuiIO& io = ImGui::GetIO();
            if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
            {
                ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
                ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
            }

            if (ImGui::BeginMenuBar())
            {
                if (ImGui::BeginMenu(LOCALIZATION("Options")))
                {
                    // Disabling fullscreen would allow the window to be moved to the front of other windows,
                    // which we can't undo at the moment without finer window depth/z control.
                    ImGui::MenuItem(LOCALIZATION("Fullscreen"), NULL, &opt_fullscreen);
                    ImGui::MenuItem(LOCALIZATION("Padding"), NULL, &opt_padding);
                    ImGui::Separator();

                    if (ImGui::MenuItem("Flag: NoSplit", "", (dockspace_flags & ImGuiDockNodeFlags_NoSplit) != 0))
                    {
                        dockspace_flags ^= ImGuiDockNodeFlags_NoSplit;
                    }
                    if (ImGui::MenuItem("Flag: NoResize", "", (dockspace_flags & ImGuiDockNodeFlags_NoResize) != 0))
                    {
                        dockspace_flags ^= ImGuiDockNodeFlags_NoResize;
                    }
                    if (ImGui::MenuItem("Flag: NoDockingInCentralNode", "",
                        (dockspace_flags & ImGuiDockNodeFlags_NoDockingInCentralNode) != 0))
                    {
                        dockspace_flags ^= ImGuiDockNodeFlags_NoDockingInCentralNode;
                    }
                    if (ImGui::MenuItem("Flag: AutoHideTabBar", "",
                        (dockspace_flags & ImGuiDockNodeFlags_AutoHideTabBar) != 0))
                    {
                        dockspace_flags ^= ImGuiDockNodeFlags_AutoHideTabBar;
                    }
                    if (ImGui::MenuItem("Flag: PassthruCentralNode", "",
                        (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode) != 0, opt_fullscreen))
                    {
                        dockspace_flags ^= ImGuiDockNodeFlags_PassthruCentralNode;
                    }
                    ImGui::Separator();

                    if (ImGui::MenuItem(LOCALIZATION("Close"), NULL, false, dockspace_open != NULL))
                        dockspace_open = false;
                    ImGui::EndMenu();
                }

                ImGui::EndMenuBar();
            }

            ImGui::Begin(LOCALIZATION("Main Menu"), NULL, ImGuiWindowFlags_MenuBar);
            {
                // Track if the mouse is captured - if it is captured, do not update 3D view rotation
                // (if this is not set, the UI menu is click-through)
                ImGuiIO& io = ImGui::GetIO();
                if (ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem))
                {
                    m_mouse_captures = true;
                }
                else
                {
                    m_mouse_captures = false;
                }

                if (ImGui::BeginMenuBar())
                {
                    if (ImGui::BeginMenu(LOCALIZATION("File")))
                    {
                        if (ImGui::MenuItem(LOCALIZATION("Open"), "Ctrl+O"))
                        {
                            const int file_name_len = 256;
                            char file_name[file_name_len] = "";
                            OPENFILENAME open_file;
                            memset(&open_file, 0, sizeof(OPENFILENAME));
                            open_file.lStructSize = sizeof(OPENFILENAME);
                            open_file.hwndOwner = NULL;
                            open_file.lpstrFilter = L"OBJ Object File (*.obj)\0*.obj\0All (*.*)\0*.*\0";
                            open_file.nFilterIndex = 1;
                            open_file.lpstrFile = (LPWSTR)file_name;
                            open_file.nMaxFile = file_name_len;
                            open_file.lpstrTitle = L"Select an OBJ file";
                            open_file.lpstrDefExt = L"obj";
                            open_file.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_NOCHANGEDIR;

                            // Add Comdlg32.lib to your project settings->linker->object/library modules edit:
                            BOOL ret = GetOpenFileName(&open_file);

                            // Get the path and convert to UTF-8
                            std::wstring path = std::wstring(open_file.lpstrFile);
                            using convert_type = std::codecvt_utf8<wchar_t>;
                            std::wstring_convert<convert_type, wchar_t> converter;
                            std::string convertedPath = converter.to_bytes(path);

						    // Load
						    // Aliases
						    auto& resourceManager = Aurora::ResourceManager::GetInstance();
						    auto& global = Aurora::Global::GetInstance();

						    if (resourceManager.loadResource<Aurora::Model>(convertedPath))
						    {
							    // Put on the object queue
							    auto model = std::static_pointer_cast<Aurora::Model>(resourceManager.getResource(convertedPath));
							    Aurora::ModelObject* obj = new Aurora::ModelObject(model);

							    obj->Attach(new Aurora::Transform(glm::vec3(0, 0, 0)));
                                for (auto renderPararameters : obj->getRenderParameters())
                                {
                                    obj->Attach(new Aurora::RendererComponent(renderPararameters));
                                }

							    global.RegisterObject(obj);
							    AURORA_LOG_INFO("Object registered to global");
						    }

					    }

                        // ~TODO~
                        // Save the engine project (scene objects/assets)
                        if (ImGui::MenuItem(LOCALIZATION("Save As"), "Ctrl+Shift+S"))
                        {
                        }

                        ImGui::EndMenu();
                    }
                    ImGui::EndMenuBar();
                }

                if (ImGui::Button(LOCALIZATION("Reset Game")))
                {
                    AURORA_LOG_INFO(LOCALIZATION("Reset Game"));
                }
                ImGui::SameLine();
                if (ImGui::Button(LOCALIZATION("Pause Game")))
                {
                    AURORA_LOG_INFO(LOCALIZATION("Pause Game"));
                }
                ImGui::SameLine();
                if (ImGui::Button(LOCALIZATION("Play Game")))
                {
                    AURORA_LOG_INFO(LOCALIZATION("Play Game"));
                }

                // Using the generic BeginListBox() API, you have full control over how to display the combo contents.
                // (your selection data could be an index, a pointer to the object, an id for the object, a flag intrusively
                // stored in the object itself, etc.)


                std::string path = ".";

                static int item_current_idx = 0; // Here we store our selection data as an index.

                std::string item_to_load = "";

                ImGui::SetNextItemOpen(true, ImGuiCond_Once);
                if (ImGui::TreeNode(LOCALIZATION("Content")))
                {
                    RecursiveDirectoryTree(path, item_to_load);
                    ImGui::TreePop();
                }

                // load resource
                if (item_to_load != "")
                {
                    std::string suffix = item_to_load.substr(item_to_load.find_last_of('.'));

                    // load model
                    if (suffix == ".obj" || suffix == ".fbx")
                    {
                        AURORA_LOG_INFO("Load Model: {}", item_to_load);
                        Aurora::ResourceManager::GetInstance().loadResource<Aurora::Model>(item_to_load);
                    }
                    else if (suffix == ".bmp")
                    {
                        AURORA_LOG_INFO("Load Texture: {}", item_to_load);
                        // TODO load texture
                        // Aurora::ResourceManager::GetInstance().loadResource<Aurora::Texture>(item_to_load);
                    }
                }

                ImGui::Text("Time Running %.3f seconds", ImGui::GetTime());
                ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate,
                    ImGui::GetIO().Framerate);

                //ImGui::Text("Memory Allocated %d", Aurora::MemoryTracker::GetInstance().getMemoryAllocated());
                //ImGui::Text("Memory Freed %d", Aurora::MemoryTracker::GetInstance().getMemoryFreed());
                ImGui::Text("Memory Use %d", Aurora::MemoryTracker::GetInstance().getMemoryAllocated() - Aurora::MemoryTracker::GetInstance().getMemoryFreed());
                //ImGui::Text("Memory Percentage %.3f", (float)Aurora::MemoryTracker::GetInstance().getMemoryFreed() / (float)Aurora::MemoryTracker::GetInstance().getMemoryAllocated() * 100.f);
                

				ImGui::End();
			}

			// Terrain window starts here
			ImGui::SetNextWindowSize(ImVec2(300, 350));

			ImGui::Begin(LOCALIZATION("Terrain Generation"), NULL);
			{
				// Track if the mouse is captured - if it is captured, do not update 3D view rotation
				// (if this is not set, the UI menu is click-through)
				ImGuiIO& io = ImGui::GetIO();
				if (ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem))
				{
					m_mouse_captures = true;
				}
				else
				{
					m_mouse_captures = false;
				}

				
				ImGui::Text("Generated Mesh settings");
				ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.325f);
				ImGui::InputFloat("mesh X dim", &meshX);	//ImGui::SameLine();
				ImGui::InputFloat("mesh Z dim", &meshY);
				ImGui::InputFloat("max mesh height", &maxMeshHeight);

				int strideMax = (texX < texY) ? texX : texY;
				ImGui::SliderInt("pixels between vertices", &texelStride, 1, strideMax);

				ImGui::Text("Generated Texture settings");
				ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.325f);
				ImGui::InputInt("texture width", &texX);	//ImGui::SameLine();
				ImGui::InputInt("texture height", &texY);

				ImGui::Text("Generation settings");
				ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.8f);
				ImGui::SliderFloat("seed", &seed, 0.f, 2000.f);
				ImGui::SliderInt("floors", &zones, 0, 10);

				ImGui::Text("");

				if (ImGui::Button(LOCALIZATION("Generate Terrain"), ImVec2(ImGui::GetWindowWidth() * 0.95f, 30)))
				{
					AURORA_LOG_INFO(LOCALIZATION("Clicked"));
					// optional args "resultDirectory" and "filename" are defaulting to putting terrain.obj and terrain.ppm in ../Sandbox/res/Terrain
                    const char* resultsFolder = "../Sandbox/res/Terrain";
                    const char* filename = "terrain_test";
                    
                    makeTerrain(texX, texY, texelStride, meshX, meshY, maxMeshHeight, zones, seed, resultsFolder, filename);

                    std::string objPath = std::string(resultsFolder) + "/" + std::string(filename) + ".obj";
                    std::string ppmPath = std::string(resultsFolder) + "/" + std::string(filename) + ".ppm";

                    //Aurora::ResourceManager::GetInstance().loadResource<Aurora::Model>(objPath);

                    auto& resourceManager = Aurora::ResourceManager::GetInstance();
                    auto& global = Aurora::Global::GetInstance();

                    if (resourceManager.loadResource<Aurora::Model>(objPath))
                    {
                        // Put on the object queue
                        auto model = std::static_pointer_cast<Aurora::Model>(resourceManager.getResource(objPath));
                        Aurora::ModelObject* obj = new Aurora::ModelObject(model);

                        obj->Attach(new Aurora::Transform(glm::vec3(0, 0, 0)));
                        for (auto renderPararameters : obj->getRenderParameters())
                        {
                            obj->Attach(new Aurora::RendererComponent(renderPararameters));
                        }

                        global.RegisterObject(obj);
                        AURORA_LOG_INFO("Object registered to global");
                    }
				}

				ImGui::End();
			}

			// end Terrain window

			ImGui::SetNextWindowSize(ImVec2(1340, 800));

            // This is where the GUI Viewport window is set up
            // We read the framebuffer and show the image ...
            // ... which the renderer outputs to color attachment texture
            ImGui::Begin(LOCALIZATION("Viewport"));
            {
                ImVec2 viewport_size = ImGui::GetContentRegionAvail();

                uint32_t textureID = renderer->m_ColorAttachment;

                // check if viewport size changes, ...
                // ... and if so - recreate the framebuffer:
                if (viewport_size.x != renderer->m_framebuffer_width
                    && viewport_size.y != renderer->m_framebuffer_height)
                {
                    renderer->resizeFramebuffer(viewport_size.x, viewport_size.y);
                    //window->resize(viewport_size.x, viewport_size.y);
                }

                ImGui::Image((void*)textureID,
                    ImVec2{ (float)renderer->m_framebuffer_width, (float)renderer->m_framebuffer_height },
                    ImVec2{ 0, 1 },
                    ImVec2{ 1, 0 });
                ImGui::End();
            }
            
            GuiTypeSupport::GetInstance().init(imGuiContext);
            hierarchyPanel->draw(imGuiContext);
            inspectorPanel->draw(imGuiContext, hierarchyPanel);

            // logger window
            ImGuiTextFilter filter;
            ImGui::SetNextWindowSize(ImVec2(500, 400));
            if (ImGui::Begin(LOCALIZATION("Logger"), NULL))
            {
                // Options menu
                // Main window
                bool clear = ImGui::Button("Clear");
                ImGui::SameLine();
                filter.Draw("Filter", -100.0f);

                ImGui::Separator();
                ImGui::BeginChild("scrolling", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar);

                if (clear)
                    Aurora::Log::ClearLog();

                ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));
                const char* buf = Aurora::Log::GetTextBuffer()->begin();
                const char* buf_end = Aurora::Log::GetTextBuffer()->end();
                if (filter.IsActive())
                {
                    for (int line_no = 0; line_no < Aurora::Log::GetLineOffsets()->Size; line_no++)
                    {
                        const char* line_start = buf + Aurora::Log::GetLineOffsets()->Data[line_no];
                        const char* line_end = (line_no + 1 < Aurora::Log::GetLineOffsets()->Size) ? (buf + Aurora::Log::GetLineOffsets()->Data[line_no + 1] - 1) : buf_end;
                        if (filter.PassFilter(line_start, line_end))
                            ImGui::TextUnformatted(line_start, line_end);
                    }
                }
                else
                {
                    ImGuiListClipper clipper;
                    clipper.Begin(Aurora::Log::GetLineOffsets()->Size);
                    while (clipper.Step())
                    {
                        for (int line_no = clipper.DisplayStart; line_no < clipper.DisplayEnd; line_no++)
                        {
                            const char* line_start = buf + Aurora::Log::GetLineOffsets()->Data[line_no];
                            const char* line_end = (line_no + 1 < Aurora::Log::GetLineOffsets()->Size) ? (buf + Aurora::Log::GetLineOffsets()->Data[line_no + 1] - 1) : buf_end;
                            ImGui::TextUnformatted(line_start, line_end);
                        }
                    }
                    clipper.End();
                }
                ImGui::PopStyleVar();

                if (ImGui::GetScrollY() >= ImGui::GetScrollMaxY())
                    ImGui::SetScrollHereY(1.0f);

                ImGui::EndChild();
                ImGui::End();
            }

            ImGui::End();
        }
    }
}
