#include "aurora_pch.h"
#include "EventCallBack.h"

namespace Aurora
{
	Closure* NewEventCallback(void (*callback)())
	{
		return new EventCallBackDefault(callback);
	}

	Closure* NewEventCallback(void (*callback)(Response* response))
	{
		return new EventCallBackDefaultWithResponse(callback);
	}
}