/*program entry point*/

#pragma once

#include "aurora_pch.h"
#include "Application.h"
#include "Log.h"
#include "Framework/EventSystem/EventSystem.h"

#ifdef AURORA_PLATFORM_WINDOWS
int main(int argc, char** argv)
{
	Aurora::app = Aurora::CreateApplication();
	return 0;
}
#endif