workspace "Aurora"
	startproject "Sandbox"
	
	language "C++"
	architecture "x86_64"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

thirdparty = "%{wks.location}/Aurora/thirdparty"
targetOutputDir = "%{wks.location}/bin/%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"
objOutputDir = "%{wks.location}/build/obj/%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

include "Aurora"

include "Aurora/thirdparty/GLFW"
include "Aurora/thirdparty/Glad"
include "Aurora/thirdparty/ImGui"
include "Aurora/thirdparty/spdlog"

include "Aurora/thirdparty/glm"
include "Aurora/thirdparty/Eigen"
include "Aurora/thirdparty/assimp"

include "Aurora/thirdparty/CImg"
include "Aurora/thirdparty/Properties"

include "Aurora/thirdparty/LuaJit"

include "Aurora/thirdparty/FBXSDK"
include "Aurora/thirdparty/ozz"

include "Sandbox"