#pragma once

#include "Core/Singleton.h"
#include "glm.hpp"

namespace Aurora
{
	class GuiTypeSupport : public Singleton<GuiTypeSupport>
	{
	public:
		void init(ImGuiContext* _imguiContext)
		{
			imguiContext = _imguiContext;
		}

		void drawProps(int& data, const std::string& name);
		void drawProps(float& data, const std::string& name);
		void drawProps(double& data, const std::string& name);
		void drawProps(glm::vec2& vec2, const std::string& name);
		void drawProps(glm::vec3& vec3, const std::string& name);
		void drawProps(glm::vec4& vec4, const std::string& name);
		void drawProps(std::string& str, const std::string& name);
		void drawProps(char* str, const std::string& name);

	private:
		ImGuiContext* imguiContext;
	};

	template<typename T>
	void drawClass(T* obj, const char* name = "") {

		if constexpr (std::is_class_v<std::decay_t<T>>)
		{
			foreach(*obj, [=](auto&& name, auto&& value)
				{
					GuiTypeSupport::GetInstance().drawProps(*value, name);
				});
		}
	}
}
