#pragma once

namespace Aurora
{
	class ObjectStore;
	class Object;

	class Scene
	{
	public:
		Scene();
		~Scene();

		void Render();
		void Update();

		void RegisterObject(Object* obj);
		void UnregisterObject(Object* obj);

		ObjectStore* GetObjects() { return entities; }

	private:
		ObjectStore* entities;
	};
}