project "Eigen"
	language "C"
	kind "StaticLib"
	staticruntime "off"

	location "%{wks.location}/build"

	targetdir (targetOutputDir .. "/%{prj.name}")
	objdir (objOutputDir .. "/%{prj.name}")
	
	includedirs
	{
		"Eigen",
	}

	files
	{
        "**.c",
		"**.h",
		"**.cpp",

		"**.lua",
	}

    filter "configurations:Debug"
		defines "_DEBUG"
		symbols "On"
		runtime "Debug"
	
	filter "configurations:Release"
		defines "NDEBUG"
		flags { "OptimizeSpeed", "NoFramePointer", "ExtraWarnings", "NoEditAndContinue" }