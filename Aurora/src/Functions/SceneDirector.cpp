#include "aurora_pch.h"
#include "SceneDirector.h"
#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/Cube.h"

#include "Core/ResourceManager/ResourceManager.h"
#include "SpinningComp.h"
#include "CameraMoveTest.h"

//double Aurora::SceneDirector::curTime = 0.0;

void Aurora::SceneDirector::init()
{
    makeFloor();
    makeWater();
    makeCity();
    makeTerrain();

    makeCamera();
    makeHand();
    makeCube();
    makeSphere();
    makeTeapot();
    makeAnim();
}


void Aurora::SceneDirector::OnEnable(Object* obj)
{
    // see time as name of the scene director object
    obj->name = "Scene Director";
    curTime = 0.0;

    float posx = -0.3f;
    float posy = 0.18f;
    float posz = 0.f;

    double start_offset = 7.0;
    double slowdown = 2.0;

    // Basic scene means the transformation is fixed
    // If we register basic scene again at 10.0 seconds, ...
    // ... the objects of setupFirstScene will appear on the same spot
    RegisterTransformScene(start_offset + slowdown * 3.0, &SceneDirector::setupSphereOnHand, posx, posy, posz);
    RegisterTransformScene(start_offset + slowdown * 4.1, &SceneDirector::setupTeapotOnHand, posx, posy-0.05f, posz); // -0.330f, 0.130f, 0);
    RegisterTransformScene(start_offset + slowdown * 5.3, &SceneDirector::setupCubeNormalOnHand, posx, posy, posz);
    RegisterTransformScene(start_offset + slowdown * 6.7, &SceneDirector::setupCubeNormalTex2OnHand, posx, posy, posz);
    RegisterTransformScene(start_offset + slowdown * 7.8, &SceneDirector::setupCubeNormalTex1OnHand, posx, posy, posz);
    RegisterTransformScene(start_offset + slowdown * 9.5, &SceneDirector::setupCubeNormalTex3OnHand, posx, posy, posz);
    RegisterTransformScene(start_offset + slowdown * 11.2, &SceneDirector::setupWater, posx, posy - 10.f, posz);
    RegisterTransformScene(start_offset + slowdown * 13.0, &SceneDirector::setupCity, posx, posy - 15.8f, posz - 4.2f);
    RegisterTransformScene(start_offset + slowdown * 15.0, &SceneDirector::setupTerrain, posx, posy-15.8f, posz-4.2f);
    RegisterTransformScene(start_offset + slowdown * 17.0, &SceneDirector::setupAnimiInFrontHand, -0.850f, -0.9f, -0.08f);

    //RegisterTransformScene(14.5f, &SceneDirector::setupCamBackAnim, -0.850f, -0.9f, -0.08f);
    //RegisterTransformScene(8.5f, &SceneDirector::setupCubeNormalTex2OnHand, posx, posy, posz);
    //RegisterTransformScene(16.5f, &SceneDirector::setupCubeNormalTex1OnHand, -0.330f, 0.130f, 0);

    //RegisterTransformScene(0.5f, &SceneDirector::setupSecondScene, -0.330f, 0.130f, 0);


    //RegisterTransformScene(/*seconds*/ 1.0, /*callback*/ &SceneDirector::setupFourthScene, -2.1f, 0.f, 0.f);
    //RegisterTransformScaleScene(/*seconds*/ 4.0, /*callback*/ &SceneDirector::setupThirdScene, glm::vec3(0.f, -0.5, 1.f), glm::vec3(0.05f));
    //RegisterTransformScaleScene(/*seconds*/ 6.0, /*callback*/ &SceneDirector::setupThirdScene, glm::vec3(-5.f, -0.5, 1.f), glm::vec3(0.03f));
    //RegisterBasicScene(/*seconds*/ 5.0, /*callback*/ &SceneDirector::setupFirstScene);
    // By using Transform Scenes, we can register the SAME scene MULTIPLE times ...
    // ... with a different coordinate
    //RegisterTransformScene(/*seconds*/ 7.0, /*callback*/ &SceneDirector::setupSecondScene, 2.f, 0.5f, 0.3f);
    //RegisterTransformScene(/*seconds*/ 10.0, /*callback*/ &SceneDirector::setupSecondScene, -1.1f, 1.8f, 0.f);

}

void Aurora::SceneDirector::OnDisable(Object* obj)
{
}

void Aurora::SceneDirector::OnStart(Object* obj)
{
    init();
}

void Aurora::SceneDirector::Update(Object* obj)
{
    //float dt = 1.f / ImGui::GetIO().Framerate;
    //curTime += dt;
    curTime = ImGui::GetTime();

    //obj->name = std::to_string(curTime);

    while (!m_transform_queue.empty() && std::get<0>(m_transform_queue.front()) < curTime)
    {
        auto element = m_transform_queue.front();
        auto calllback = std::get<1>(m_transform_queue.front());
        (this->*calllback)(std::get<2>(element), std::get<3>(element), std::get<4>(element));
        m_transform_queue.pop_front();
    }
    while (!m_transform_scale_queue.empty() && std::get<0>(m_transform_scale_queue.front()) < curTime)
    {
        auto element = m_transform_scale_queue.front();
        auto calllback = std::get<1>(m_transform_scale_queue.front());
        (this->*calllback)(std::get<2>(element), std::get<3>(element));
        m_transform_scale_queue.pop_front();
    }

    while (!m_basic_queue.empty() && std::get<0>(m_basic_queue.front()) < curTime)
    {
       auto calllback = std::get<1>(m_basic_queue.front());
       (this->*calllback)();
       m_basic_queue.pop_front();
    }

    if (startZoom)
    {
        camZoom();
    }
}

void Aurora::SceneDirector::camZoom()
{
    Transform* camTsm = camera->GetComponent<Transform>(0);
    camTsm->Position() += camTsm->Front() * -0.03f;
}

void Aurora::SceneDirector::OnShutdown(Object* obj)
{
}

std::string Aurora::SceneDirector::getTypeDes()
{
    return "SceneDirector";
}

void Aurora::SceneDirector::makeCamera()
{
    camera = new exampleObj(0);
    camera->name = "camera";
    camera->Attach(new Transform(glm::vec3(0.143, 0.605, -0.013f), glm::vec3(-47.54, 90, 0)));
    camera->Attach(new Camera());
    camera->Attach(new CameraMoveTest());

    Aurora::RenderSystem::GetInstance().SetMainCamera(camera->GetComponent<Camera>(1));

    Global::GetInstance().RegisterObject(camera);
}

void Aurora::SceneDirector::makeAnim()
{
    anim = new exampleObj(2);
    anim->name = "Player";
    anim->Attach(new Transform(glm::vec3(999.0f, 999.0f, 999.0f), glm::vec3(0, 90, 0)));
    anim->Attach(new MotionSystem());

    Global::GetInstance().RegisterObject(anim);
}

void Aurora::SceneDirector::makeSphere()
{
    auto sphere_generate_pri = PrimitiveObjectProperties();
    sphere_generate_pri.radius = 0.075f;

    sphere_normal = new Sphere(sphere_generate_pri);
    sphere_normal->name = "sphere_normal";
    sphere_normal->Attach(new Transform(glm::vec3(999, 999, 999)));

    //Global::Render
    sphere_normal->Attach(new RendererComponent(sphere_normal->getVAO(), sphere_normal->getEBO(), new Material()));
    sphere_normal->Attach(new BobbingComp());

    Global::GetInstance().RegisterObject(sphere_normal);
}

void Aurora::SceneDirector::makeWater()
{
    PrimitiveObjectProperties water_properties;
    water_properties.height = 50.f;
    water_properties.width = 50.f;
    water_properties.tex_width = 10.f;
    water_properties.tex_height = 10.f;
    water = new Plane(water_properties);
    water->name = "Water";
    water->Attach(new Transform(glm::vec3(999)));// 4.f)));


    MaterialProperties water_material_properties;
    //water_material_properties.base_colour = glm::vec4(0.4f, 0.85f, 0.89f, 1.0f);
    water_material_properties.specular_colour = glm::vec4(1.f, 1.f, 1.f, 15.0f);
    //water_material_properties.specular_colour = glm::vec4(1.f, 1.f, 1.f, 1.0f);
    water_material_properties.specular_coef = 10000.f;
    water_material_properties.specular_coef = 10.f;
    water_material_properties.asset_sources = {
        {Aurora::MaterialProperties::ASSET::SHADER, "../Sandbox/res/Shaders/water.shader"},
    };
    water_material_properties.texture_sources = {
        {Aurora::MaterialProperties::TEXTURE::DIFFUSE, "../Sandbox/res/Textures/water-diffuse.jpg"},
        {Aurora::MaterialProperties::TEXTURE::NORMAL, "../Sandbox/res/Textures/water-normal.jpg" },
        {Aurora::MaterialProperties::TEXTURE::SPECULAR, "../Sandbox/res/Textures/terrain-height.jpg" },
    };
    water_material_properties.name = "Water";
    water_material_properties.min_filter = GL_LINEAR_MIPMAP_LINEAR;
    water_material_properties.mag_filter = GL_LINEAR;
    Material* water_material = new Material(water_material_properties);

    water->Attach(new RendererComponent(water->getVAO(), water->getEBO(), water_material));

    Global::GetInstance().RegisterObject(water);
}


void Aurora::SceneDirector::makeFloor()
{
    // FLOOR
    PrimitiveObjectProperties floor_properties;
    floor_properties.height = 100.f;
    floor_properties.width = 100.f;
    floor_properties.tex_width = 100.f;
    floor_properties.tex_height = 100.f;
    floor = new Plane(floor_properties);
    floor->name = "floor";
    floor->Attach(new Transform(glm::vec3(0.f, -10.f, 0.f)));

    Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/black_floor.mtl");
    auto matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/black_floor.mtl");
    Material* floor_material = static_cast<Material*>(matptr.get());
    floor_material->setShader("../Sandbox/res/Shaders/textured.shader");

    floor->Attach(new RendererComponent(floor->getVAO(), floor->getEBO(), floor_material));

    Global::GetInstance().RegisterObject(floor);
}



void Aurora::SceneDirector::makeCube()
{
    auto cube_generate_pri = PrimitiveObjectProperties();
    cube_generate_pri.scale_x = 0.3f;
    cube_generate_pri.scale_y = 0.3f;
    cube_generate_pri.scale_z = 0.3f;

    cube_normal = new Cube(cube_generate_pri);
    cube_normal->name = "cube_normal";
    cube_normal->Attach(new Transform(glm::vec3(999, 999, 999)));

    //Global::Render
    cube_normal->Attach(new RendererComponent(cube_normal->getVAO(), cube_normal->getEBO(), new Material()));
    cube_normal->GetComponent<RendererComponent>(1)->light_pos = glm::vec3(5.f, 5.f, 0.f);
    cube_normal->Attach(new SpinningComp());

    Global::GetInstance().RegisterObject(cube_normal);

    cube_normal_texture1 = new Cube(cube_generate_pri);
    cube_normal_texture1->name = "cube_normal_texture1";
    cube_normal_texture1->Attach(new Transform(glm::vec3(999, 999, 999)));

    Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/metal_box.mtl");
    auto matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/metal_box.mtl");
    Material* cube_material = static_cast<Material*>(matptr.get());
    cube_material->setTextureMinFilter(0x2703/*GL_LINEAR_MIPMAP_LINEAR*/);
    cube_material->setTextureMagFilter(0x2601/*GL_LINEAR*/);

    //Global::Render
    cube_normal_texture1->Attach(new RendererComponent(cube_normal_texture1->getVAO(), cube_normal_texture1->getEBO(), cube_material));
    cube_normal_texture1->GetComponent<RendererComponent>(1)->light_pos = glm::vec3(4.11f, 0.23f, 2.13f);
    cube_normal_texture1->Attach(new SpinningComp());

    Global::GetInstance().RegisterObject(cube_normal_texture1);


    cube_normal_texture2 = new Cube(cube_generate_pri);
    cube_normal_texture2->name = "cube_normal_texture2";
    cube_normal_texture2->Attach(new Transform(glm::vec3(999, 999, 999)));

    Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/metal_box_plain.mtl");
    matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/metal_box_plain.mtl");
    Material* cube_material2 = static_cast<Material*>(matptr.get());
    cube_material2->setShader("./res/Shaders/textured.shader");

    //Global::Render
    cube_normal_texture2->Attach(new RendererComponent(cube_normal_texture2->getVAO(), cube_normal_texture2->getEBO(), cube_material2));
    cube_normal_texture2->GetComponent<RendererComponent>(1)->light_pos = glm::vec3(5.f, 5.f, 0.f);
    //cube_normal_texture2->Attach(new RendererComponent(cube_normal_texture2->getVAO(), cube_normal_texture2->getEBO(), new Material()));
    cube_normal_texture2->Attach(new SpinningComp());

    Global::GetInstance().RegisterObject(cube_normal_texture2);








    cube_normal_texture3 = new Cube(cube_generate_pri);
    cube_normal_texture3->name = "cube_normal_texture3";
    cube_normal_texture3->Attach(new Transform(glm::vec3(999, 999, 999)));

    Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/metal_box2.mtl");
    matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/metal_box2.mtl");
    Material* cube_material3 = static_cast<Material*>(matptr.get());
    //cube_material2->setShader("./res/Shaders/textured.shader");

    //Global::Render
    cube_normal_texture3->Attach(new RendererComponent(cube_normal_texture3->getVAO(), cube_normal_texture3->getEBO(), cube_material3));
    cube_normal_texture3->GetComponent<RendererComponent>(1)->light_pos = glm::vec3(4.11f, 0.23f, 2.13f);
    //cube_normal_texture2->Attach(new RendererComponent(cube_normal_texture2->getVAO(), cube_normal_texture2->getEBO(), new Material()));
    cube_normal_texture3->Attach(new SpinningComp());

    Global::GetInstance().RegisterObject(cube_normal_texture3);






}

void Aurora::SceneDirector::makeTeapot()
{
    auto& resourceManager = Aurora::ResourceManager::GetInstance();
    auto& global = Aurora::Global::GetInstance();

    //if (resourceManager.loadResource<Aurora::Model>("../Sandbox/res/Test Models/teapot/teapot-1K-triangles.obj"))
    if (resourceManager.loadResource<Aurora::Model>("../Sandbox/res/Test Models/teapot/teapot_scene.obj"))
    {
        // Put on the object queue
        //auto model = std::static_pointer_cast<Aurora::Model>(resourceManager.getResource("../Sandbox/res/Test Models/teapot/teapot-1K-triangles.obj"));
        auto model = std::static_pointer_cast<Aurora::Model>(resourceManager.getResource("../Sandbox/res/Test Models/teapot/teapot_scene.obj"));
        teapot_normal = new Aurora::ModelObject(model);
        teapot_normal->name = "teapot";

        teapot_normal->Attach(new Aurora::Transform(glm::vec3(999), glm::vec3(0.f, -82.f, 0.f), glm::vec3(0.015f)));

        for (auto renderPararameters : teapot_normal->getRenderParameters())
        {
            RendererComponent* render = new Aurora::RendererComponent(renderPararameters);
            teapot_normal->Attach(render);
            render->light_pos = glm::vec3(-10.0f, 7.5f, 0);
        }
        teapot_normal->GetComponent<RendererComponent>(1)->light_pos = glm::vec3(-30.f, -13.f, -7.f);
        //teapot_normal->Attach(new SpinningComp());
        teapot_normal->Attach(new BobbingComp());
        //teapot_normal->m

        global.RegisterObject(teapot_normal);
        AURORA_LOG_INFO("Teapot registered to global");
    }
}

void Aurora::SceneDirector::makeHand()
{
    auto& resourceManager = Aurora::ResourceManager::GetInstance();
    auto& global = Aurora::Global::GetInstance();

    //if (resourceManager.loadResource<Aurora::Model>("../Sandbox/res/Test Models/hand/hand-1K-triangles.obj"))
    if (resourceManager.loadResource<Aurora::Model>("../Sandbox/res/Test Models/hand/hand.obj"))
    {
        // Put on the object queue
        //auto model = std::static_pointer_cast<Aurora::Model>(resourceManager.getResource("../Sandbox/res/Test Models/hand/hand-1K-triangles.obj"));
        auto model = std::static_pointer_cast<Aurora::Model>(resourceManager.getResource("../Sandbox/res/Test Models/hand/hand.obj"));
        hand = new Aurora::ModelObject(model);
        hand->name = "hand";

        hand->Attach(new Aurora::Transform(glm::vec3(0)));
        for (auto renderPararameters : hand->getRenderParameters())
        {
            hand->Attach(new Aurora::RendererComponent(renderPararameters));
        }

        global.RegisterObject(hand);
        AURORA_LOG_INFO("Hand registered to global");
    }
}



void Aurora::SceneDirector::setupSphereOnHand(float x, float y, float z)
{
    teapot_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture1->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture2->GetComponent<Transform>(0)->Position() = glm::vec3(999);

    sphere_normal->GetComponent<SpinningComp>(2)->originPos = glm::vec3(x, y, z);
    sphere_normal->GetComponent<Transform>(0)->Position() = glm::vec3(x, y, z);
}

void Aurora::SceneDirector::setupTeapotOnHand(float x, float y, float z)
{
    sphere_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture1->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture2->GetComponent<Transform>(0)->Position() = glm::vec3(999);

    teapot_normal->GetComponent<SpinningComp>(2)->originPos = glm::vec3(x, y, z);
    teapot_normal->GetComponent<Transform>(0)->Position() = glm::vec3(x, y, z);
    //teapot_normal->GetComponent<RendererComponent>(0)->light_pos = glm::vec3(-30.f, -13.f, -7.f);
}

void Aurora::SceneDirector::setupLightOnHand(float x, float y, float z)
{
    sphere_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    teapot_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture1->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture2->GetComponent<Transform>(0)->Position() = glm::vec3(999);

    //teapot_normal->GetComponent<Transform>(0)->Position() = glm::vec3(x, y, z);
    hand->GetComponent<RendererComponent>(1)->light_pos = glm::vec3(4.11f, 0.23f, 2.13f);
}

void Aurora::SceneDirector::setupCubeNormalOnHand(float x, float y, float z)
{
    sphere_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    teapot_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture1->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture2->GetComponent<Transform>(0)->Position() = glm::vec3(999);

    hand->GetComponent<RendererComponent>(1)->light_pos = glm::vec3(0.8f, 0.1f, 0.f);
    cube_normal->GetComponent<Transform>(0)->Position() = glm::vec3(x, y, z);
}

void Aurora::SceneDirector::setupCubeNormalTex1OnHand(float x, float y, float z)
{
    sphere_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    teapot_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture2->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture3->GetComponent<Transform>(0)->Position() = glm::vec3(999);

    cube_normal_texture1->GetComponent<SpinningComp>(2)->originPos = glm::vec3(x, y, z);
    cube_normal_texture1->GetComponent<Transform>(0)->Position() = glm::vec3(x, y, z);
}

void Aurora::SceneDirector::setupCubeNormalTex2OnHand(float x, float y, float z)
{
    sphere_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    teapot_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture1->GetComponent<Transform>(0)->Position() = glm::vec3(999);

    cube_normal_texture2->GetComponent<SpinningComp>(2)->originPos = glm::vec3(x, y, z);
    cube_normal_texture2->GetComponent<Transform>(0)->Position() = glm::vec3(x, y, z);
}

void Aurora::SceneDirector::setupCubeNormalTex3OnHand(float x, float y, float z)
{
    sphere_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    teapot_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture1->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture2->GetComponent<Transform>(0)->Position() = glm::vec3(999);

    cube_normal_texture3->GetComponent<Transform>(0)->Position() = glm::vec3(x, y, z);
}



void Aurora::SceneDirector::setupWater(float x, float y, float z)
{
    floor->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    water->GetComponent<Transform>(0)->Position() = glm::vec3(x, y, z);
}

void Aurora::SceneDirector::setupCity(float x, float y, float z)
{
    water->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    city->GetComponent<Transform>(0)->Position() = glm::vec3(x, y, z);

    city->Attach(new PanningComp());
}

void Aurora::SceneDirector::setupTerrain(float x, float y, float z)
{
    float water_height = 5.f;

    city->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    water->GetComponent<Transform>(0)->Position() = glm::vec3(-45.f, -32.f+water_height, -13.f);
    water->GetComponent<Transform>(0)->Rotation() = glm::vec3(x, y, 15.72f);
    terrain->GetComponent<Transform>(0)->Position() = glm::vec3(-45.f, -32.f, -13.f);
    terrain->GetComponent<Transform>(0)->Rotation() = glm::vec3(x, y, 15.72f);
    
    terrain->Attach(new PanningComp());
    water->Attach(new PanningComp());

    //city->GetComponent<RendererComponent>(0)->light_pos = glm::vec3(x, y, z);
}



void Aurora::SceneDirector::makeCity()
{
    auto& resourceManager = Aurora::ResourceManager::GetInstance();
    auto& global = Aurora::Global::GetInstance();

    //if (resourceManager.loadResource<Aurora::Model>("../Sandbox/res/Test Models/city/metro city/city_resized_placeholder.obj"))
    if (resourceManager.loadResource<Aurora::Model>("../Sandbox/res/Test Models/city/metro city/city_resized.obj"))
    {
        // Put on the object queue
        //auto model = std::static_pointer_cast<Aurora::Model>(resourceManager.getResource("../Sandbox/res/Test Models/city/metro city/city_resized_placeholder.obj"));
        auto model = std::static_pointer_cast<Aurora::Model>(resourceManager.getResource("../Sandbox/res/Test Models/city/metro city/city_resized.obj"));
        city = new Aurora::ModelObject(model);
        city->name = "city";

        city->Attach(new Aurora::Transform(glm::vec3(999)));

        for (auto renderPararameters : city->getRenderParameters())
        {
            city->Attach(new Aurora::RendererComponent(renderPararameters));
        }
        city->GetComponent<RendererComponent>(1)->light_pos = glm::vec3(-80.f, 3.f, -8.3f);

        //city->Attach(new PanningComp());

        global.RegisterObject(city);
        AURORA_LOG_INFO("City registered to global");
    }
}

void Aurora::SceneDirector::makeTerrain()
{
    auto& resourceManager = Aurora::ResourceManager::GetInstance();
    auto& global = Aurora::Global::GetInstance();

    if (resourceManager.loadResource<Aurora::Model>("../Sandbox/res/Test Models/textured_realistic_terrain_renormalized.obj"))
    {
        // Put on the object queue
        auto model = std::static_pointer_cast<Aurora::Model>(resourceManager.getResource("../Sandbox/res/Test Models/textured_realistic_terrain_renormalized.obj"));
        terrain = new Aurora::ModelObject(model);
        terrain->name = "terrain";

        terrain->Attach(new Aurora::Transform(glm::vec3(999)));

        for (auto renderPararameters : terrain->getRenderParameters())
        {
            terrain->Attach(new Aurora::RendererComponent(renderPararameters));
        }
        terrain->GetComponent<RendererComponent>(1)->light_pos = glm::vec3(-80.f, 3.f, -8.3f);

        global.RegisterObject(terrain);
        AURORA_LOG_INFO("Terrain registered to global");
    }
}

void Aurora::SceneDirector::setupAnimiInFrontHand(float x, float y, float z)
{
    Transform* animTsm = anim->GetComponent<Transform>(0);
    animTsm->Position() = glm::vec3(x, y, z);
    anim->GetComponent<MotionSystem>(1)->time_ratio = 0.0f;
}

void Aurora::SceneDirector::setupCamBackAnim(float x, float y, float z)
{
    Transform* camTsm = camera->GetComponent<Transform>(0);
    camTsm->Rotation() = glm::vec3(-19.5f, 270.1f, 0);
    camTsm->Position() = glm::vec3(-2.858f, 1.004f, -0.052f);

    anim->GetComponent<MotionSystem>(1)->light_pos = glm::vec3(-5.450, 8.140, 0);

    sphere_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    teapot_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture1->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    cube_normal_texture2->GetComponent<Transform>(0)->Position() = glm::vec3(999);
    hand->GetComponent<Transform>(0)->Position() = glm::vec3(999);

    startZoom = true;
}

void Aurora::SceneDirector::setupFirstScene()
{
    Cube* cube = new Cube(PrimitiveObjectProperties());
    cube->name = "Scene 1 Cube";
    cube->Attach(new Transform(glm::vec3(-0.4f, 0, -1.f)));
    Transform* tsm = cube->GetComponent<Transform>(0);

    Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/metal_box.mtl");
    auto matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/metal_box.mtl");
    Material* cube_material = static_cast<Material*>(matptr.get());
    cube_material->setTextureMinFilter(0x2703/*GL_LINEAR_MIPMAP_LINEAR*/);
    cube_material->setTextureMagFilter(0x2601/*GL_LINEAR*/);

    //Global::Render
    cube->Attach(new RendererComponent(cube->getVAO(), cube->getEBO(), cube_material));
    cube->Attach(new SpinningComp());

    Global::GetInstance().RegisterObject(cube);
}

void Aurora::SceneDirector::setupSecondScene(float x, float y, float z)
{
    Cube* cube = new Cube(PrimitiveObjectProperties());
    cube->name = "Scene 2 Cube";
    cube->Attach(new Transform(glm::vec3(x, y, z)));
    Transform* tsm = cube->GetComponent<Transform>(0);

    Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/metal_box.mtl");
    auto matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/metal_box.mtl");
    Material* cube_material = static_cast<Material*>(matptr.get());
    cube_material->setTextureMinFilter(0x2703/*GL_LINEAR_MIPMAP_LINEAR*/);
    cube_material->setTextureMagFilter(0x2601/*GL_LINEAR*/);

    //Global::Render
    cube->Attach(new RendererComponent(cube->getVAO(), cube->getEBO(), cube_material));

    Global::GetInstance().RegisterObject(cube);
}

void Aurora::SceneDirector::setupThirdScene(glm::vec3 aTransform, glm::vec3 aScale)
{
    auto& resourceManager = Aurora::ResourceManager::GetInstance();
    auto& global = Aurora::Global::GetInstance();

    //if (resourceManager.loadResource<Aurora::Model>("../Sandbox/res/Test Models/Tree5k.obj"))
    if (resourceManager.loadResource<Aurora::Model>("../Sandbox/res/Test Models/tree/tree.obj"))
    {
        // Put on the object queue
        //auto model = std::static_pointer_cast<Aurora::Model>(resourceManager.getResource("../Sandbox/res/Test Models/Tree5k.obj"));
        auto model = std::static_pointer_cast<Aurora::Model>(resourceManager.getResource("../Sandbox/res/Test Models/tree/tree.obj"));
        Aurora::ModelObject* obj = new Aurora::ModelObject(model);

        obj->Attach(new Aurora::Transform(aTransform, glm::vec3(), aScale));
        for (auto renderPararameters : obj->getRenderParameters())
        {
            obj->Attach(new Aurora::RendererComponent(renderPararameters));
        }

        global.RegisterObject(obj);
        AURORA_LOG_INFO("Object registered to global");
    }
}

void Aurora::SceneDirector::setupFourthScene(float x, float y, float z)
{
    Sphere* sphere = new Sphere(PrimitiveObjectProperties());
    sphere->name = "Scene 4 Sphere";
    sphere->Attach(new Transform(glm::vec3(x, y, z)));
    Transform* tsm = sphere->GetComponent<Transform>(0);

    Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/pink.mtl");
    auto matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/pink.mtl");
    Material* cube_material = static_cast<Material*>(matptr.get());
    cube_material->setTextureMinFilter(0x2703/*GL_LINEAR_MIPMAP_LINEAR*/);
    cube_material->setTextureMagFilter(0x2601/*GL_LINEAR*/);

    //Global::Render
    sphere->Attach(new RendererComponent(sphere->getVAO(), sphere->getEBO(), cube_material));

    Global::GetInstance().RegisterObject(sphere);
}