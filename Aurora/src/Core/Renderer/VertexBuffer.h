#pragma once

namespace Aurora
{
	/*  Reference:
	 *  Title: Renderer
	 *  Author: Studio Cherno
	 *  Date: 2021
	 *  Availability: https://github.com/TheCherno/Hazel/tree/master/Hazel/src/Hazel/Renderer
	 *  Type: Tutorial/Notes
	 *  Renderer System was based on and extended upon the Hazel Game Engine Renderer ...
	 *  ... includes: Renderer, VertexArray, VertexBuffer, VertexBufferLayout, IndexBuffer and Shader
	 */

	class VertexBuffer 
	{
	private:
		unsigned m_RendererID;
	public:
		VertexBuffer() = default;
		VertexBuffer(const void* data, unsigned int size);
		~VertexBuffer();

		void update(const void* data, unsigned int size);
		void bind() const;
		void unbind() const;
	};
}
