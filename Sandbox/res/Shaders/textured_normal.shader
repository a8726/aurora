#shader vertex
#version 450 core

// These are the attributes stored in a vertex:
layout(location = 0) in vec4 in_position;
layout(location = 1) in vec4 in_normal;
layout(location = 2) in vec4 in_tangent;
layout(location = 3) in vec2 in_texcoords;
layout(location = 4) in vec4 in_color;

// naming convention:
// 'vert' = vertex shader
// 'out'  = output
out vec3 vertout_normal;
out vec3 vertout_fragment_position;
out vec2 vertout_texture_coords;
out mat3 vertout_tbn;

uniform mat4 u_model;		// M
uniform mat4 u_view;		// V
uniform mat4 u_projection;	// P


void main()
{
    gl_Position = u_projection * u_view * u_model * vec4(in_position);
    // if drawing mode is set to GL_POINTS, ...
    // ... draw points that are big enough to see by default:
    gl_PointSize = 10.0;

    // Fill in variables that we will pass to fragment shader:
    // translate (and scale) the object's vertex positions:
    vertout_fragment_position = vec3(u_model * in_position);
    // inverse transpose normals in case of scaled model matrix:
    vertout_normal = mat3(transpose(inverse(u_model))) * vec3(in_normal.xyz);
    // pass texture UVs to fragment shader (vec2)
    vertout_texture_coords = in_texcoords;

    // compute Bitangent (used as entry in the TBN Matrix ...
    // ... which takes the normal map to tangent space)
    vec3 B = cross(vertout_normal, vec3(in_tangent.xyz));
    // the TBN Matrix consisting of tangent, bitangent and normal ...
    // ... it transforms the normal texture to be aligned ...
    // ... with the actual surface positions:
    vertout_tbn = mat3(vec3(in_tangent.xyz), vec3(B.xyz), vertout_normal);//mat3(transpose(inverse(u_model)));

};




















#shader fragment
#version 450 core

layout(binding = 0, std140) uniform u_material
{
    vec4	u_ambient_col;
    vec4	u_diffuse_col;
    vec4	u_specular_col;
    vec4	u_emission_col;
    float 	u_specular_coef;
    float 	u_optical_density;
    float 	u_opacity;
    float 	u_illum;
};

layout(binding = 0) uniform sampler2D colortexture;
layout(binding = 1) uniform sampler2D normaltexture;

// output color per pixel (fragment):
layout(location = 0) out vec4 out_color;

// naming convention:
// 'vert' = vertex shader
// 'out'  = output
// NOTE: the names below must match ...
// ... the output names from vertex shader
in vec3 vertout_normal;
in vec3 vertout_fragment_position;
in vec2 vertout_texture_coords;
in mat3 vertout_tbn;

uniform vec3 u_light_position;
uniform vec3 u_camera_position;

#include "../Sandbox/res/Shaders/illumination.hlsli"


void main()
{
    // Normal vector:
    // NOTE: get the normal vector from texture (instead of normalize(vertout_normal.rgb))
    vec3 raw_normal = vec3(texture(normaltexture, vertout_texture_coords).rgb);
    // the normal texture range is [0, 1] so that we can see colours ...
    // ... so here we remap to range [-1, 1]
    raw_normal = normalize(raw_normal * 2.0 - 1.0);
    // transform normal to tangent space:
    vec3 N = vertout_tbn * raw_normal;  //vertout_tbn * N;
    // Light direction (from current ray position)
    vec3 L = normalize(u_light_position - vertout_fragment_position);
    // Camera direction (from current ray position)
    vec3 C = normalize(u_camera_position - vertout_fragment_position);
    // add illumination to the colour:
    vec3 illuminated_color = add_local_illumination(/* diffuse color */ u_diffuse_col.rgb,
        /* ambient color */ u_ambient_col.rgb, /* specular color*/ u_specular_col.rgb,
        /* opacity       */ u_opacity, /* shine         */ u_specular_coef,
        /* normal, light direction, camera direction */ N, L, C,
        /* strength of individual components */ u_specular_col.a, u_diffuse_col.a, u_ambient_col.a);

    out_color = vec4(illuminated_color, 1.f);
    // modulate computed illumination color with texture:
    out_color = out_color * vec4(texture(colortexture, vertout_texture_coords).rgba);
};
