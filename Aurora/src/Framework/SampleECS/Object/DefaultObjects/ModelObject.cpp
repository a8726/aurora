#include "aurora_pch.h"
#include "ModelObject.h"

namespace Aurora {

	void ModelObject::render()
	{
	}

	void ModelObject::render(Transform* transform)
	{
		m_Model->render(transform);
	}

	std::vector<RenderParameters*> ModelObject::getRenderParameters()
	{
		return (*m_Model).getRenderParameters();
	}

	ModelObject::ModelObject(const std::shared_ptr<Model> model)
	{
		m_Model = model;
	}

}