#include "aurora_pch.h"
#include "Core/Renderer/Renderer.h"
#include "RendererComponent.h"
#include "Transform.h"
#include "Framework/SampleECS/Object/Object.h"
#include "Framework/SampleECS/System/RenderSystem/RenderSystem.h"

#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

namespace Aurora
{
	RendererComponent::RendererComponent()
	{

	}

	RendererComponent::RendererComponent(VertexArray* a_VertexArray, IndexBuffer* a_IndexBuffer, Material* a_Material)
	//RendererComponent::RendererComponent(const VertexArray& a_VertexArray, const IndexBuffer& a_IndexBuffer, const Material& a_Material)
		: m_VertexArray(a_VertexArray), m_IndexBuffer(a_IndexBuffer), m_Material(a_Material), m_transform() 
	{
		m_UniStore = UniformStorage();
		// add default uniform - the position will affect all objects:
		glm::mat4x4 origin = glm::mat4x4(1, 0, 0, 0,
										 0, 1, 0, 0,
										 0, 0, 1, 0,
		/* final line is for position */ 0, 0, 0, 1);

		m_UniStore.addUniform("u_model", origin);
		//m_UniStore.setUniform("u_model", origin);

		m_RenderParams.va = a_VertexArray;
		m_RenderParams.ib = a_IndexBuffer;
		m_RenderParams.material = a_Material;
		m_RenderParams.us = m_UniStore;

		isInit = true;
	}

	RendererComponent::RendererComponent(RenderParameters* a_RenderParameters)
		: m_VertexArray(a_RenderParameters->va), m_IndexBuffer(a_RenderParameters->ib), m_Material(a_RenderParameters->material), m_transform() 
	{
		m_UniStore = UniformStorage();
		// add default uniform - the position will affect all objects:
		glm::mat4x4 origin = glm::mat4x4(1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			/* final line is for position */ 0, 0, 0, 1);

		m_UniStore.addUniform("u_model", origin);
		//m_UniStore.setUniform("u_model", origin);

		m_RenderParams.va = m_VertexArray;
		m_RenderParams.ib = m_IndexBuffer;
		m_RenderParams.material = m_Material;
		m_RenderParams.us = m_UniStore;

		isInit = true;
	}

	void RendererComponent::SetParameters(RenderParameters* renderParameters)
	{
		m_VertexArray = renderParameters->va;
		m_IndexBuffer = renderParameters->ib;
		m_Material = renderParameters->material;

		m_UniStore = UniformStorage();
		// add default uniform - the position will affect all objects:
		glm::mat4x4 origin = glm::mat4x4(1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			/* final line is for position */ 0, 0, 0, 1);

		m_UniStore.addUniform("u_model", origin);
		//m_UniStore.setUniform("u_model", origin);

		m_RenderParams.va = m_VertexArray;
		m_RenderParams.ib = m_IndexBuffer;
		m_RenderParams.material = m_Material;
		m_RenderParams.us = m_UniStore;

		isInit = true;
	}

	void RendererComponent::SetParameters(VertexArray* aVertexArray, IndexBuffer* aIndexBuffer, Material* aMaterial)
	{
		m_VertexArray = aVertexArray;
		m_IndexBuffer = aIndexBuffer;
		m_Material = aMaterial;

		m_UniStore = UniformStorage();
		// add default uniform - the position will affect all objects:
		glm::mat4x4 origin = glm::mat4x4(1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			/* final line is for position */ 0, 0, 0, 1);

		m_UniStore.addUniform("u_model", origin);
		//m_UniStore.setUniform("u_model", origin);

		m_RenderParams.va = aVertexArray;
		m_RenderParams.ib = aIndexBuffer;
		m_RenderParams.material = aMaterial;
		m_RenderParams.us = m_UniStore;

		isInit = true;
	}
	
	void RendererComponent::OnEnable(Object* obj)
	{
		light_pos = glm::vec3(0.8f, 0.1f, 0.f);
		m_transform = obj->GetComponent<Transform>(0);
	}
	void RendererComponent::OnDisable(Object* obj)
	{
	}
	void RendererComponent::OnStart(Object* obj)
	{
	}
	void RendererComponent::Update(Object* obj)
	{
		if (!isInit)
		{
			return;
		}

		float red_channel = 0.0f;
		double increment = 0.05;
		red_channel += sin(1.f);

		m_Material->m_shader->bind();

		float u_time = (float)ImGui::GetTime();
		m_Material->m_shader->setUniform1f("u_time", (float)ImGui::GetTime()); //(float)0.5f);


		Transform* transform = self->GetComponent<Transform>(0);
		//m_Material->m_shader->setUniformMat4f("u_model", glm::mat4x4(1, 0, 0, 0,
		//	0, 1, 0, 0,
		//	0, 0, 1, 0,
		//	transform->Position().x,
		//	transform->Position().y,
		//	transform->Position().z, 1));

		glm::mat4x4 translate = *transform->ModelMat();
		/*glm::mat4x4(1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			transform->Position().x,
			transform->Position().y,
			transform->Position().z, 1);*/

		//UniformStorage uniform;// = UniformStorage(translate);
		//uniform.addUniform("u_model", translate);
		m_UniStore.m_uniforms[0].m_data = translate;


		glm::mat4 viewMat, projMat;
		RenderSystem::GetInstance().GetCameraMats(&viewMat, &projMat);
		m_Material->m_shader->setUniformMat4f("u_view", viewMat);
		m_Material->m_shader->setUniformMat4f("u_projection", projMat);

		//std::cout << std::endl;
		//std::cout << viewMat[0][0] << " " << viewMat[1][0] << " " << viewMat[2][0] << " side" << std::endl;
		//std::cout << viewMat[0][1] << " " << viewMat[1][1] << " " << viewMat[2][1] << " up" << std::endl;
		//std::cout << viewMat[0][2] << " " << viewMat[1][2] << " " << viewMat[2][2] << " -front" << std::endl;
		
		//std::cout << viewMat[0][0] << " " << viewMat[1][0] << " " << viewMat[2][0] << " " << viewMat[3][0] << " side" << std::endl;
		//std::cout << viewMat[0][1] << " " << viewMat[1][1] << " " << viewMat[2][1] << " " << viewMat[3][1] << " up" << std::endl;
		//std::cout << viewMat[0][2] << " " << viewMat[1][2] << " " << viewMat[2][2] << " " << viewMat[3][2] << " -front" << std::endl;
		//std::cout << viewMat[0][3] << " " << viewMat[1][3] << " " << viewMat[2][3] << " " << viewMat[3][3] << std::endl;

		// input camera pos, light pos
		glm::vec3 cameraPos = RenderSystem::GetInstance().GetCameraPos();
		//m_Material->m_shader->setUniform3f("u_light_position", 0.8f, 0.1f, 0.f);
		//m_Material->m_shader->setUniform3f("u_light_position", 150.f*cos(u_time * 10.0 * 0.01), 150.f * sin(u_time * 10.0 * 0.01), 150.f * cos(u_time * 10.0 * 0.01));
		//m_Material->m_shader->setUniform3f("u_light_position", 150.f*cos(u_time * 10.0 * 0.01), 150.f * sin(u_time * 10.0 * 0.01), 150.f * cos(u_time * 10.0 * 0.01));
		m_Material->m_shader->setUniform3f("u_light_position", light_pos.x, light_pos.y, light_pos.z);


		//m_shader->setUniform3f("u_light_position", cameraPos.x, cameraPos.y, cameraPos.z);
		m_Material->m_shader->setUniform3f("u_camera_position", cameraPos.x, cameraPos.y, cameraPos.z);


		// Push the textures to the shader's texture queue ...
		// ... textures are specified in this order:
		// location 0 - diffuse texture (glActiveTexture(GL_TEXTURE0)
		// location 1 - normal texture  (glActiveTexture(GL_TEXTURE1)
		// ... more to be added
		//for (unsigned i = 0; i < m_material.m_textures.size(); i++)
		//{
		//	m_material.m_shader->pushTexture(m_material.m_textures[i]->m_ID);
		//}

		//RenderSystem::GetInstance().Render(new RenderParameters(m_VertexArray, 
		//	m_IndexBuffer, m_Material, uniform));

		//RenderSystem::GetInstance().Render(RenderParameters(m_VertexArray,
		//	m_IndexBuffer, m_Material, m_UniStore));
		m_RenderParams.material->render_mode = obj->render_mode;
		m_RenderParams.us = m_UniStore;
		RenderSystem::GetInstance().Render(m_RenderParams);

		m_Material->m_shader->unbind();
	}
	void RendererComponent::OnShutdown(Object* obj)
	{
	}
}