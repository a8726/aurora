#pragma once

// renderer is needed since the viewport is a part of our GUI
#include "../Core/Renderer/Renderer.h"
#include "Framework/SampleECS/Object/Object.h"
#include "HierarchyPanel.h"
#include "InspectorPanel.h"
//#include "../Core/Window.h"
//#include "../Platform/Windows/WindowsWindow.h"
//#include "WindowsWindow.h"
//#include <srcPlatform\Windows\WindowsWindow.h>

namespace Aurora
{
	class EngineGuiSystem
	{
	public:
		EngineGuiSystem();
		virtual ~EngineGuiSystem() = default;
		//void RenderDockspace();
		void RenderDockspace(Renderer* renderer); // , Aurora::WindowsWindow* window);
		bool m_mouse_captures = false;

		ImGuiContext* imGuiContext;

	private:
		HierarchyPanel* hierarchyPanel;
		InspectorPanel* inspectorPanel;

		// vars for interacting with terrain generation
		// is this the best place for them? probably not
		// is this the fastest place for me to put them so i can move on? yes
		// sorry -JCB
		int texX = 1000, texY = 1000;
		int texelStride = 10;

		float meshX = 100.f, meshY = 100.f;
		float maxMeshHeight = 80.f;

		int zones = 4;
		float seed = 1234.56f;
	};
}