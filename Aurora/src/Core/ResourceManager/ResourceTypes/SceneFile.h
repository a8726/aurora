#pragma once

#include <Core/ResourceManager/Resource.h>

namespace Aurora
{
	namespace FileType
	{
		class SceneFormat
		{
		public:
			SceneFormat() {}
			virtual ~SceneFormat() {}
		};
	}

	class ScenesStorage : public Resource
	{
		bool load(const std::string path) override;
	};
}
