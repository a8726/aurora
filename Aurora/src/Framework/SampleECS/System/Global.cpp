#include "aurora_pch.h"
#include "Global.h"
#include "RenderSystem/RenderSystem.h"
#include "Core/ResourceManager/ResourceManager.h"

namespace Aurora
{

	void Global::init()
	{
		scenes.push_back(new Scene());
	}

	void Global::Update()
	{
		scenes[activeSceneIndex]->Update();
	}

	void Global::Render()
	{
		scenes[activeSceneIndex]->Render();

		// will be changed in the future
		RenderSystem::GetInstance().render();
	}

	void Global::ResetScenes(std::string path)
	{

	}

	void Global::RegisterObject(Object* obj)
	{
		scenes[activeSceneIndex]->RegisterObject(obj);
	}

	void Global::UnregisterObject(Object* obj)
	{
		scenes[activeSceneIndex]->UnregisterObject(obj);
	}

}
