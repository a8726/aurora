#pragma once

#include "Core/Renderer/Renderer.h"
#include "Core/ResourceManager/ResourceTypes/Material.h"

namespace Aurora
{
	class RenderParameters
	{
	public:
		//RenderParameters(VertexArray* _va, IndexBuffer* _ib, Shader* _shader)
		//	: va(_va), ib(_ib), shader(_shader) {}
		~RenderParameters() 
		{
			//delete va;
			//delete ib;
			//delete us;
			//delete material;
		}

		RenderParameters()
			: va(nullptr), ib(nullptr), material(nullptr) {}

		RenderParameters(VertexArray* _va, IndexBuffer* _ib, Material* _material)
			: va(_va), ib(_ib), material(_material) {}

		RenderParameters(VertexArray* _va, IndexBuffer* _ib,
			Material* _material, UniformStorage _us)
			: va(_va), ib(_ib), material(_material), us(_us) {}

		VertexArray* va;
		IndexBuffer* ib;
		UniformStorage us;
		//Shader* shader;
		Material* material;
	};

	class RenderQueue
	{
	public:
		void push(RenderParameters* renderParas);
		//std::deque<RenderParameters> getParameterQueue() { return paras; }
		//void push(const RenderParameters& renderParas);
		void pop();
		RenderParameters* front();
		RenderParameters* back();
		//RenderParameters front();
		void clear();
		bool isEmpty();
		size_t m_queue_size = 0;
	private:
		std::deque<RenderParameters*> paras;
	};
}

