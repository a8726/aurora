#include "aurora_pch.h"
#include "Scene.h"
#include "Framework/SampleECS/Object/Object.h"

namespace Aurora
{
	Scene::Scene()
	{
		entities = new ObjectStore();
	}

	Scene::~Scene()
	{
		delete(entities);
	}

	void Scene::Render()
	{
		entities->Render();
	}

	void Scene::Update()
	{
		entities->Update();
	}

	void Scene::RegisterObject(Object* obj)
	{
		entities->RegisterObject(obj);
	}

	void Scene::UnregisterObject(Object* obj)
	{
		entities->UnregisterObject(obj);
	}
}