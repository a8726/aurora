#pragma once

namespace Aurora
{
	class Object;

	class ComponentBase
	{
	public:
		ComponentBase() = default;
		virtual ~ComponentBase() = default;

		virtual void OnAttach(Object* obj) { self = obj; isAttached = true; };

		virtual void OnEnable(Object* obj) = 0;
		virtual void OnDisable(Object* obj) = 0;

		virtual void OnStart(Object* obj) = 0;
		virtual void Update(Object* obj) = 0;
		virtual void OnShutdown(Object* obj) = 0;

		virtual std::string getTypeDes() = 0;

		bool getValid() { return isValid; }
		void setValid(bool _status)
		{
			if (!isAttached)
			{
				AURORA_LOG_DEBUG("Please attach first");
				return;
			}

			if (_status == true && isValid == false)
			{
				OnEnable(this->self);
			}
			else if (_status == false && isValid == true)
			{
				OnDisable(this->self);
			}

			isValid = _status;
		}

	protected:
		Object* self;
		bool isValid = true;
		bool isAttached = false;
		
	};

	
}

