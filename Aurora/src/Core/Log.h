/*log system core file*/

#pragma once

#include "spdlog/spdlog.h"
#include <spdlog/fmt/ostr.h>
#include <spdlog/fmt/fmt.h>

#include "imgui.h"

namespace Aurora
{
    class Log
    {
    public:
        static void init();
        static void init_imgui_log();

        static std::shared_ptr<spdlog::logger> GetAuroraLogger() { return auroraLogger; }
        static std::shared_ptr<spdlog::logger> GetDebugLogger() { return debugLogger; }

        static void AddLog(std::string log_text);

        static void ClearLog();
        
        static std::shared_ptr<ImGuiTextBuffer> GetTextBuffer() { return imgui_text_buffer; }
        static std::shared_ptr<ImVector<int>> GetLineOffsets() { return imgui_text_lineoffsets; }

    private:
        static std::shared_ptr<spdlog::logger> auroraLogger;
        static std::shared_ptr<spdlog::logger> debugLogger;
        
        static std::shared_ptr<ImGuiTextBuffer> imgui_text_buffer;
        static std::shared_ptr<ImVector<int>> imgui_text_lineoffsets;
        inline static bool imgui_log_init = false;
    };
}

#define AURORA_LOG_TRACE(...) Aurora::Log::GetAuroraLogger()->trace(__VA_ARGS__); \
                                    Aurora::Log::AddLog(fmt::format(__VA_ARGS__));
#define AURORA_LOG_DEBUG(...) Aurora::Log::GetAuroraLogger()->debug(__VA_ARGS__); \
                                    Aurora::Log::AddLog(fmt::format(__VA_ARGS__));
#define AURORA_LOG_INFO(...) Aurora::Log::GetAuroraLogger()->info(__VA_ARGS__); \
                                    Aurora::Log::AddLog(fmt::format(__VA_ARGS__));
#define AURORA_LOG_WARN(...) Aurora::Log::GetAuroraLogger()->warn(__VA_ARGS__); \
                                    Aurora::Log::AddLog(fmt::format(__VA_ARGS__));
#define AURORA_LOG_ERROR(...) Aurora::Log::GetAuroraLogger()->error(__VA_ARGS__); \
                                    Aurora::Log::AddLog(fmt::format(__VA_ARGS__));
#define AURORA_LOG_FATAL(...) Aurora::Log::GetAuroraLogger()->critical(__VA_ARGS__); \
                                    Aurora::Log::AddLog(fmt::format(__VA_ARGS__));

#define DEBUG_LOG_TRACE(...) Aurora::Log::GetDebugLogger()->trace(__VA_ARGS__); \
                                    Aurora::Log::AddLog(fmt::format(__VA_ARGS__));
#define DEBUG_LOG_DEBUG(...) Aurora::Log::GetDebugLogger()->debug(__VA_ARGS__); \
                                    Aurora::Log::AddLog(fmt::format(__VA_ARGS__));
#define DEBUG_LOG_INFO(...) Aurora::Log::GetDebugLogger()->info(__VA_ARGS__); \
                                    Aurora::Log::AddLog(fmt::format(__VA_ARGS__));
#define DEBUG_LOG_WARN(...) Aurora::Log::GetDebugLogger()->warn(__VA_ARGS__); \
                                    Aurora::Log::AddLog(fmt::format(__VA_ARGS__));
#define DEBUG_LOG_ERROR(...) Aurora::Log::GetDebugLogger()->error(__VA_ARGS__); \
                                    Aurora::Log::AddLog(fmt::format(__VA_ARGS__));
#define DEBUG_LOG_FATAL(...) Aurora::Log::GetDebugLogger()->critical(__VA_ARGS__); \
                                    Aurora::Log::AddLog(fmt::format(__VA_ARGS__));

#ifdef AURORA_GENERATE_RELEASE

#define AURORA_LOG_TRACE(...)
#define AURORA_LOG_DEBUG(...)
#define AURORA_LOG_INFO(...)
#define AURORA_LOG_WARN(...)
#define AURORA_LOG_ERROR(...)
#define AURORA_LOG_FATAL(...)

#define DEBUG_LOG_TRACE(...)
#define DEBUG_LOG_DEBUG(...)
#define DEBUG_LOG_INFO(...)
#define DEBUG_LOG_WARN(...)
#define DEBUG_LOG_ERROR(...)
#define DEBUG_LOG_FATAL(...)

#endif // AURORA_GENERATE_RELEASE
