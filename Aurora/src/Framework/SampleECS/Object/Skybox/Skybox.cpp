#include "aurora_pch.h"
#include "Skybox.h"
#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"
#include "Framework/SampleECS/Component/DefaultComponents/RendererComponent.h"
#include "Framework/SampleECS/System/Global.h"
#include "Core/ResourceManager/ResourceTypes/Material.h"

#include "glad/glad.h"


namespace Aurora
{
	Skybox::Skybox()
	{
		float wall_offsets[6] = {  -m_skybox_size, m_skybox_size,
								   -m_skybox_size, m_skybox_size,
								   -m_skybox_size, m_skybox_size };

		int wall_flat_indices[6] = {
			1, 1, 2, 2, 0, 0
		};

		Surface surface;

		std::vector<float>    point_vector;
		std::vector<unsigned> index_vector;

		int num_points = 0;
		int element_count = 0;
		int index_counter = 0;

		// Doing tests with a sky dome too:
		//PrimitiveObjectProperties skybox_wall_properties;
		//skybox_wall_properties.generation_type = PrimitiveObject::SPHERE;
		//skybox_wall_properties.asset_sources = {
		//	{Aurora::PrimitiveObject::ASSET::SHADER, "./res/Shaders/dynamic_skybox.shader"},
		//	{Aurora::PrimitiveObject::ASSET::DIFFUSE_MAP, m_texsource + std::to_string(1) + ".jpg"}
		//};
		//skybox_wall_properties.base_colour = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
		//skybox_wall_properties.is_dynamic_colour = false;
		//skybox_wall_properties.texture_filter = GL_NEAREST;
		////skybox_wall_properties.texture_filter = GL_LINEAR;
		//skybox_wall_properties.texture_wrapping = GL_CLAMP_TO_EDGE;

		//skybox_wall_properties.radius = 50.f;

		//skybox_wall_properties.height = m_skybox_size;
		//skybox_wall_properties.width  = m_skybox_size;
		//PrimitiveObject* wall = new PrimitiveObject(skybox_wall_properties);

		//m_walls.push_back(wall);


		for (uint32_t i = 0; i < 1; i++)
		{
			//PrimitiveObjectProperties skybox_wall_properties;
			MaterialProperties skybox_material_props;
			skybox_material_props.asset_sources = {
				{Aurora::MaterialProperties::ASSET::SHADER, "../Sandbox/res/Shaders/dynamic_skybox.shader"},
			};
			skybox_material_props.texture_sources = {
				//{Aurora::MaterialProperties::TEXTURE::OTHER, m_texsource}
				{Aurora::MaterialProperties::TEXTURE::DIFFUSE, m_texsource + "2.bmp"},
				{Aurora::MaterialProperties::TEXTURE::NORMAL, m_texsource + "1.bmp"},
				{Aurora::MaterialProperties::TEXTURE::SPECULAR, m_texsource + "lowoct5.jpg"},
				{Aurora::MaterialProperties::TEXTURE::BUMP, m_texsource + "3.bmp"}
			};

			skybox_material_props.base_colour = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
			skybox_material_props.min_filter = GL_LINEAR;
			skybox_material_props.mag_filter = GL_LINEAR;
			skybox_material_props.texture_wrapping = GL_REPEAT;

			// GENERATE MATERIAL HERE
			Material * skybox_material = new Material(skybox_material_props);
			// material initialized in this way will populate its texture vector based on the material properties
			// for skybox walls, we want to add an additional 3D texture to this list that stores noise 
			Texture3D* cloudVolumeDataTexture = new Texture3D();
			cloudVolumeDataTexture->setAllDim(64);
			//cloudVolumeDataTexture->setAllDim(512);
			cloudVolumeDataTexture->generate();
			skybox_material->m_textures.push_back(cloudVolumeDataTexture);

			// it'll be bound to sampler 1 since only one other texture is bound before it

			PrimitiveObjectProperties skybox_wall_properties;
			//skybox_wall_properties.plane_flat_index = wall_flat_indices[i];
			skybox_wall_properties.plane_flat_index = 2;
			skybox_wall_properties.plane_offset = 0.;
			skybox_wall_properties.height = 1.;
			skybox_wall_properties.width  = 1.;
			skybox_wall_properties.tex_width   = 1.f;
			skybox_wall_properties.tex_height  = 1.f;
			//PrimitiveObject* wall = new PrimitiveObject(skybox_wall_properties);
			Plane* wall = new Plane(skybox_wall_properties);
			wall->name = "skybox_wall " + std::to_string(i);

			m_walls.push_back(wall);

			this->m_walls[i]->Attach(new Transform(glm::vec3(0, 0, 0)));
			this->m_walls[i]->Attach(new RendererComponent(this->m_walls[i]->getVAO(), this->m_walls[i]->getEBO(), skybox_material));
			//Global::GetInstance().RegisterObject(this->m_walls[i]);

		}

	}
	void Skybox::render()
	{
		// this never runs
		
	}
}