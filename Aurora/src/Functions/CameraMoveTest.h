#pragma once

#include "Framework/SampleECS/Component/ComponentBase.h"
#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"

#include "Framework/InputSystem/InputSystem.h"

class CameraMoveTest : public Aurora::ComponentBase
{
public:

	virtual std::string getTypeDes() override
	{
		return "CameraMoveTest";
	}

	DEFINE_PROPERTIES(
		(float) moveSpeed
	)

private:
	
	// previous mouse position ...
	// ... used for getting mouse position difference
	int  m_mouseX = 0;
	int  m_mouseY = 0;
	
	Aurora::Transform* transform;

	void keyWKeepCallback();
	void keyAKeepCallback();
	void keySKeepCallback();
	void keyDKeepCallback();
	// Move Camera Up
	void keyQKeepCallback();
	// Moive Camera Down
	void keyEKeepCallback();

	void mouseRightKeepCallback(Aurora::Response* mouse);
	void mouseRightClickCallback(Aurora::Response* mouse);

	virtual void OnEnable(Aurora::Object* obj) override;
	virtual void OnDisable(Aurora::Object* obj) override;
	virtual void OnStart(Aurora::Object* obj) override;
	virtual void Update(Aurora::Object* obj) override;
	virtual void OnShutdown(Aurora::Object* obj) override;
};

