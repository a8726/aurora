#pragma once
#include "Core/Singleton.h"

namespace Aurora
{

	class I18N : public Singleton<I18N>
	{
	public:
		I18N() = default;
		~I18N() = default;

		void init();
		std::string localization(std::string str);
		const char* localization(const char* str);
	};

}

#define LOCALIZATION(...) Aurora::I18N::GetInstance().localization(__VA_ARGS__)

