#pragma once

namespace Aurora
{
	template<typename T>
	class Singleton
	{
	public:
		static T& GetInstance()
		{
			static T instance;
			return instance;
		}

		Singleton(const T&) = delete;
		Singleton(T&&) = delete;
		void operator = (const T&) = delete;

	protected:
		Singleton() = default;
		virtual ~Singleton() = default;
	};
}
