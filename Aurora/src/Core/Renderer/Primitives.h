#pragma once

/*
    HEADER for graphics primitives

    Includes:
    
        Surface Object definition
        + functions to draw graphics primitives

        Currently supporting:
        - spheres [generateSphereSurface]
        

*/

struct Surface
{
    std::vector<float>    point_vector;
    std::vector<unsigned> index_vector;

    float* point_array;
    unsigned int* index_array;
    int    num_points = 0;
    int    num_elements = 0;
    int    num_triangles = 0;

};

namespace primitives 
{
    static Surface generatePlane(const float& plane_height,
        const int& segments1,
        const int& segments2,
        const float& width = 1.f,
        const float& height = 1.f,
        const int& flat_index = 1,
        const float& tex_width = 1.f,
        const float& tex_height = 1.f);
    
    static Surface generateCube(const float&    radius,
                                const uint32_t& segments1, 
                                const uint32_t& segments2)
    {
        float wall_offsets[6] = { -radius, radius,
                                  -radius, radius,
                                  -radius, radius };

        int wall_flat_indices[6] = {
            1, 1, 2, 2, 0, 0
        };

        Surface surface;

        std::vector<float>    point_vector;
        std::vector<unsigned> index_vector;

        int num_points = 0;
        int element_count = 0;
        int index_counter = 0;

        for (uint32_t i = 0; i < 6; i++)
        {
            Surface face = generatePlane(wall_offsets[i], segments1, segments2,
                                              radius, radius, wall_flat_indices[i]);


            // concatenate vectors
            for (unsigned j = 0; j < face.point_vector.size(); j++)
            {
                point_vector.push_back(face.point_vector[j]);
            }
            for (unsigned j = 0; j < face.index_vector.size(); j++)
            {
                index_vector.push_back(num_points + face.index_vector[j]);
            }

            num_points += face.num_points;
            element_count += face.num_elements;
            index_counter += face.num_triangles;
            //point_vector.insert(point_vector.end(), face.point_vector.begin(), face.point_vector.end());
            //index_vector.insert(index_vector.end(), face.index_vector.begin(), face.index_vector.end());


        }


        surface.point_vector = point_vector;
        surface.index_vector = index_vector;
        surface.num_points = num_points; // point_count;
        surface.num_elements = element_count;
        surface.num_triangles = index_counter;

        //surface = generateFloorPlane(wall_offsets[2], segments1, segments2,
        //    wall_flat_indices[2]);

        return surface;
    }
    /// <summary>
    /// Generate a plane - can be flat in X, Y or Z direction
    /// </summary>
    /// <param name="1. plane offset from midpoint"></param>
    /// <param name="2. segments1"></param>
    /// <param name="3. segments2"></param>
    /// <param name="4. width"></param>
    /// <param name="5. height"></param>
    /// <param name="6. plane direction"></param>
    /// <returns>
    /// Surface containing Point Vector and Index Vector
    /// </returns>
    static Surface generatePlane(const float& plane_height, 
                                 const int& segments1, 
                                 const int& segments2,
                                 const float& width,// = 1.f,
                                 const float& height,//= 1.f,
                                 const int& flat_index,// = 1,
                                 const float& tex_width,// = 1.f,
                                 const float& tex_height)// = 1.f)
    {// draw a plane, ...
     // ... which is split to ...
     // ... as many triangle segments ...
     // ... as user specifies

        std::vector<float>    point_vector;
        std::vector<unsigned> index_vector;

        // normal will be constant:
        float const_normal[3] = { 0.f, 0.f, 0.f };
        const_normal[flat_index] = 1.f;
        float start_coord[2] = { -width, -height };
        float end_coord[2] = { width, height };

        float stepX = (end_coord[0] - start_coord[0]) / (float)segments1;
        float stepZ = (end_coord[1] - start_coord[1]) / (float)segments2;

        int num_points = 0;
        int element_count = 0;
        int index_counter = 0;

        float point1[2], point2[2], point3[2], point4[2];

        // 4 points for one face
        float pointsA[4], pointsB[4];

        uint8_t offsetsA[4] = { 0, 1, 1, 0 };
        uint8_t offsetsB[4] = { 0, 0, 1, 1 };

        for (int i = 0; i < segments1; i++)
        {
            for (int j = 0; j < segments2; j++)
            {
                // generate 4 points:
                for (int k = 0; k < 4; k++)
                {
                    pointsA[k] = start_coord[0] + (i + offsetsA[k]) * stepX;
                    pointsB[k] = start_coord[1] + (j + offsetsB[k])* stepZ;
                }
                
                // now connect the generated 4 points with triangles:
                for (int k = 0; k < 4; k++)
                {
                    // layout = 0 (vec 3 = 3D )
                    switch (flat_index)
                    {
                    case 0:
                        point_vector.push_back(plane_height);
                        point_vector.push_back(pointsA[k]);
                        point_vector.push_back(pointsB[k]);
                        break;

                    case 1:
                        point_vector.push_back(pointsA[k]);
                        point_vector.push_back(plane_height);
                        point_vector.push_back(pointsB[k]);
                        break;

                    case 2:
                        point_vector.push_back(pointsA[k]);
                        point_vector.push_back(pointsB[k]);
                        point_vector.push_back(plane_height);
                        break;
                    default:
                        AURORA_LOG_ERROR("Invalid flat index given, "
                                         "needs to be in range [0,2], given:" 
                                         + std::to_string(flat_index));
                        break;
                    }

                    // layout = 1 ( vec3 = 3D )
                    point_vector.push_back(const_normal[0]);
                    point_vector.push_back(const_normal[1]);
                    point_vector.push_back(const_normal[2]);


                    // layout = 2 ( vec2 = 2D )
                    switch (flat_index)
                    {
                    case 0:
                        if (plane_height > 0)
                        {
                            point_vector.push_back(tex_width  *( (float)(j + offsetsB[k]) / segments2));
                            point_vector.push_back(tex_height *( (float)(i + offsetsA[k]) / segments1));
                        }
                        else
                        {
                            point_vector.push_back(tex_width  *( 1.f - (float)(j + offsetsB[k]) / segments2));
                            point_vector.push_back(tex_height *( (float)(i + offsetsA[k]) / segments1));
                        }
                    break;

                    case 1:
                        if (plane_height > 0)
                        {
                            point_vector.push_back(tex_width *( (float)(i + offsetsA[k]) / segments1));
                            point_vector.push_back(tex_height*( (float)(j + offsetsB[k]) / segments2));
                        }
                        else
                        {
                            point_vector.push_back(tex_width *( (float)(i + offsetsA[k]) / segments1));
                            point_vector.push_back(tex_height *( 1.f - (float)(j + offsetsB[k]) / segments2));
                        }
                    break;

                    case 2:
                        if (plane_height > 0)
                        {
                            point_vector.push_back(tex_width  *( 1.f - (float)(i + offsetsA[k]) / segments1));
                            point_vector.push_back(tex_height *( (float)(j + offsetsB[k]) / segments2));
                        }
                        else
                        {
                            point_vector.push_back(tex_width  *( (float)(i + offsetsA[k]) / segments1));
                            point_vector.push_back(tex_height *( (float)(j + offsetsB[k]) / segments2));
                        }

                    break;
                    default:
                        AURORA_LOG_ERROR("Invalid flat index given, "
                            "needs to be in range [0,2], given:"
                            + std::to_string(flat_index));
                        break;
                    }

                }

                index_vector.push_back(num_points + 0);
                index_vector.push_back(num_points + 1);
                index_vector.push_back(num_points + 2);

                index_vector.push_back(num_points + 0);
                index_vector.push_back(num_points + 3);
                index_vector.push_back(num_points + 2);

                num_points += 4;
                element_count += 4 * 8;// 4 * 6;
                index_counter += 2;

            }
        }

        Surface surface;

        surface.point_vector = point_vector;
        surface.index_vector = index_vector;
        surface.num_points = num_points; // point_count;
        surface.num_elements = element_count;
        surface.num_triangles = index_counter;

        return surface;
    }

    /// <summary>
    /// generate a sphere surface
    /// </summary>
    /// <param name="radius"></param>
    /// <param name="squish_stretch_factor"></param>
    /// <param name="stackCount"></param>
    /// <param name="sectorCount"></param>
    /// <returns></returns>
    static Surface generateSphereSurface(
        const float& radius,
        const float& squish_stretch_factor,
        const int& stackCount,
        const int& sectorCount
    )
    {
        const float PI = 3.1459;
        float sectorStep = 2 * PI / sectorCount;
        float stackStep = PI / stackCount;
        float sectorAngle, stackAngle;
        float x, y, z, xy;                              // vertex position
        float nx, ny, nz, lengthInv = 1.0f / radius;    // vertex normal
        int element_count = 0;

        std::vector<float>    point_vector;
        std::vector<unsigned> index_vector;

        int num_points = 0;
        for (int i = 0; i <= stackCount; ++i)
        {
            for (int j = 0; j <= sectorCount; ++j)
            {
                num_points += 4;
            }
        }

        for (int i = 0; i <= stackCount; ++i)
        {
            stackAngle = PI / 2 - i * stackStep;        // starting from pi/2 to -pi/2
            xy = radius * cosf(stackAngle);             // r * cos(u)
            z = radius * sinf(stackAngle);              // r * sin(u)

            // add (sectorCount+1) vertices per stack
            // the first and last vertices have same position and normal, but different tex coords
            for (int j = 0; j <= sectorCount; ++j)
            {
                sectorAngle = j * sectorStep;           // starting from 0 to 2pi

                // vertex position (x, y, z)
                x = xy * cosf(sectorAngle);             // r * cos(u) * cos(v)
                y = xy * sinf(sectorAngle);             // r * cos(u) * sin(v)

                point_vector.push_back(x);
                point_vector.push_back(y);
                point_vector.push_back(z);

                // normalized vertex normal (nx, ny, nz)
                nx = x * lengthInv;
                ny = y * lengthInv;
                nz = z * lengthInv;

                point_vector.push_back(nx);
                point_vector.push_back(ny);
                point_vector.push_back(nz);

                point_vector.push_back((float)(i) / (sectorCount));
                point_vector.push_back((float)(j) / (stackCount));
                //point_vector.push_back(sectorAngle / 2 * PI);
                //point_vector.push_back( (stackAngle - PI/2) / PI);
                element_count += 8;
            }
        }

        int index_counter = 0;
        int k1, k2;
        for (int i = 0; i <= stackCount; ++i)
        {
            k1 = i * (sectorCount + 1);     // beginning of current stack
            k2 = k1 + sectorCount + 1;      // beginning of next stack

            for (int j = 0; j <= sectorCount; ++j, ++k1, ++k2)
            {
                // 2 triangles per sector excluding first and last stacks
                // k1 => k2 => k1+1
                if (i != 0)
                {
                    index_counter += 3;

                    index_vector.push_back(k1);
                    index_vector.push_back(k2);
                    index_vector.push_back(k1 + 1);
                }

                // k1+1 => k2 => k2+1
                if (i != (stackCount - 1))
                {
                    index_counter += 3;

                    index_vector.push_back(k1 + 1);
                    index_vector.push_back(k2);
                    index_vector.push_back(k2 + 1);
                }

                // store indices for lines
                index_vector.push_back(k1);
                index_vector.push_back(k2);

                index_counter += 2; // 12;

                if (i != 0)  // horizontal lines except 1st stack, k1 => k+1
                {
                    index_counter += 2; // 12;

                    index_vector.push_back(k1);
                    index_vector.push_back(k1 + 1);
                }
            }
        }
        Surface surface;

        surface.point_vector = point_vector;
        surface.index_vector = index_vector;
        surface.num_points = num_points; // point_count;
        surface.num_elements = element_count;
        surface.num_triangles = index_counter;

        return surface;

    }
}


