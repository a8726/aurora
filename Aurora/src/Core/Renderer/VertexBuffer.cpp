#include "aurora_pch.h"

#include "VertexBuffer.h"
#include "Renderer.h"

#include "glad/glad.h"

namespace Aurora
{
    VertexBuffer::VertexBuffer(const void* data, unsigned int size)
    { 
        unsigned int buffer_id;
        GLCall(glGenBuffers(/* how many */ 1,
                            /* ID       */ &m_RendererID));
        // bind (select) it:
        GLCall(glBindBuffer(/* type     */ GL_ARRAY_BUFFER,
                            /* ID       */ m_RendererID));
        // put data to the buffer:
        GLCall(glBufferData(/* type          */ GL_ARRAY_BUFFER,
                            /* size in bytes */ size,
                            /* data          */ nullptr,
                            /* usage-pattern */ GL_STREAM_DRAW));
        GLCall(glad_glBufferSubData(GL_ARRAY_BUFFER, 0, size, data));
    }

    VertexBuffer::~VertexBuffer()
    {
        GLCall(glDeleteBuffers(1, &m_RendererID))
    }

    void VertexBuffer::update(const void* data, unsigned int size)
    {
        // bind (select) it:
        GLCall(glBindBuffer(/* type     */ GL_ARRAY_BUFFER,
            /* ID       */ m_RendererID));
        GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, size, data));
    }

    void VertexBuffer::bind() const
    {
        GLCall(glBindBuffer(/* type     */ GL_ARRAY_BUFFER,
                            /* ID       */ m_RendererID));
    }

    void VertexBuffer::unbind() const
    {
        GLCall(glBindBuffer(/* type     */ GL_ARRAY_BUFFER,
                            /* ID       */ 0));
    }
}