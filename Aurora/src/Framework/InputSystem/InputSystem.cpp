#include "aurora_pch.h"
#include "InputSystem.h"

namespace Aurora
{
	void glfwKeyPress(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		switch (key)
		{

#include "KeyBoardSolve.h"

		default:
			break;
		}
	}

	void glfwMousePress(GLFWwindow* window, int key, int action, int mods)
	{
		switch (key)
		{

#include "MouseSolve.h"

		default:
			break;
		}
	}

	/// <summary>
	/// Windows system specific
	/// </summary>
	/// <param name="window"></param>
	void InputSystem::init(GLFWwindow* _window)
	{
		if (_window == nullptr)
		{
			AURORA_LOG_DEBUG("window handle is null");
		}
		window = _window;

		keyKeep.insert({ Key_W_Keep , false });
		keyKeep.insert({ Key_A_Keep , false });
		keyKeep.insert({ Key_S_Keep , false });
		keyKeep.insert({ Key_D_Keep , false });

		mouseKeep.insert({ Mouse_LEFT_Keep , false });
		mouseKeep.insert({ Mouse_RIGHT_Keep , false });
		mouseKeep.insert({ Mouse_MIDDLE_Keep , false });

		glfwSetKeyCallback(window, glfwKeyPress);
		glfwSetMouseButtonCallback(window, glfwMousePress);
	}

	void InputSystem::TriggerKeep()
	{
		for each (auto var in keyKeep)
		{
			if (var.second == true)
			{
				EventSystem::GetInstance().BroadcastEvent(var.first);
			}
		}

		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		
		for each (auto var in mouseKeep)
		{
			if (var.second == true)
			{
				// initially we had a new mouseclick event created EVERY FRAME on the heap...
				//MouseInteractionResponse* mouseclick = new MouseInteractionResponse(xpos, ypos);
				//MouseInteractionResponse mouseclick = MouseInteractionResponse(xpos, ypos);
				MouseInteractionResponse mouseclick(xpos, ypos);
				EventSystem::GetInstance().BroadcastEvent(var.first, &mouseclick);
				//delete mouseclick;
			}
		}
	}
	
	uint64 InputSystem::RegisterInputCode(keycode keyCode, Closure* callback)
	{	
		Event* callback_event = new Event(callback);
		return EventSystem::GetInstance().RegisterEvent(keyCode, callback_event);
	}

	void InputSystem::SetKeyKeep(keycode key, bool keep)
	{
		keyKeep[key] = keep;
	}

	void InputSystem::SetMouseKeep(keycode key, bool keep)
	{
		mouseKeep[key] = keep;
	}
	
}
