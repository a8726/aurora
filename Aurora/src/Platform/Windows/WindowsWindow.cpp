#include "aurora_pch.h"
// IMPORTANT: include glad first
// (before GLFW extension)
#include "glad/glad.h"
#include "WindowsWindow.h" // includes GLFW 

#include <windows.h>

#include "Core/Singleton.h"
#include "Core/Renderer/VertexBufferLayout.h"
#include "Core/ResourceManager/ResourceTypes/MaterialProperties.h"

#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <math.h>       /* tan */

#include "Functions/exampleObj.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/Plane.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/Cube.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/Sphere.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/PrimitiveObjectProperties.h"
#include "Framework/SampleECS/Component/DefaultComponents/RendererComponent.h"
#include "Core/ResourceManager/ResourceManager.h"
#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"
#include "Framework/SampleECS/Component/DefaultComponents/Camera.h"
#include "Framework/MotionSystem/MotionSystem.h"

#include "Functions/ObjMoveTest.h"
#include "Functions/CameraMoveTest.h"
#include "Framework/SampleECS/Object/Skybox/Skybox.h"

#include "Framework/SampleECS/System/RenderSystem/RenderSystem.h"

#include "../thirdparty/Eigen/Eigen/Dense.h"

#include "fbxsdk.h"

#include "Functions/SceneDirector.h"

namespace Aurora
{

	WindowsWindow::WindowsWindow(const WindowInitProps& props)
	{	
		init(props);
	}

	void WindowsWindow::init(const WindowInitProps& props)
	{
		if (!glfwInit())
		{
			AURORA_LOG_FATAL(LOCALIZATION("GLFW init failure!"));
			exit(EXIT_FAILURE);
		}

		m_glfwWindow = glfwCreateWindow(props.windowWidth, props.windowHeight, props.windowTitle.c_str(), NULL, NULL);

		if (!m_glfwWindow)
		{
			AURORA_LOG_FATAL(LOCALIZATION("Window create failure!"));
			exit(EXIT_FAILURE);
		}

		m_width = props.windowWidth;
		m_height = props.windowHeight;

		AURORA_LOG_DEBUG("Initialised window: " + std::to_string(m_width) + "x" + std::to_string(m_height));

		glfwMakeContextCurrent(m_glfwWindow);
		glfwSwapInterval(1);

		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		{
			AURORA_LOG_FATAL(LOCALIZATION("Failed to initialize GLAD"));
		}

		controllerInit();

		Cube* cube;
		Cube* cube2;
		Plane* floor;
		Plane* water;
		Sphere* sphere;
		Sphere* sphere1;

		Skybox* skybox = new Skybox();
		for (unsigned i = 0; i < skybox->m_walls.size(); i++)
		{
			//skybox->m_walls[i]->Attach(new RendererComponent(Material()));
			Global::GetInstance().RegisterObject(skybox->m_walls[i]);
		}



		/* -------------------------------- SCENE OBJECTS ---------------------------------- */

		cube = new Cube(PrimitiveObjectProperties());
		cube->name = "cube";
		cube2 = new Cube(PrimitiveObjectProperties());
		cube2->name = "cube2";

		PrimitiveObjectProperties sphere_prop = PrimitiveObjectProperties();
		sphere_prop.radius = 0.1;
		sphere = new Sphere(sphere_prop);

		cube->Attach(new Transform(glm::vec3(-0.4f, 0, -1.f)));
		cube2->Attach(new Transform(glm::vec3(0.f, 0.55f, 0.f)));
		sphere->Attach(new Transform(glm::vec3(0.f, 0.f, 0.5f)));
		// If the following line is uncommented, ...
		// ... cube moves together with camera:
		//cube->Attach(new CameraMoveTest());
		cube->Attach(new ObjMoveTest());

		// FLOOR
		PrimitiveObjectProperties floor_properties;
		floor_properties.height = 100.f;
		floor_properties.width = 100.f;
		floor_properties.tex_width = 100.f;
		floor_properties.tex_height = 100.f;
		floor = new Plane(floor_properties);
		floor->name = "floor";
		floor->Attach(new Transform(glm::vec3(0.f, 0.f, 0.f)));
		
		PrimitiveObjectProperties water_properties;
		water_properties.height = 50.f;
		water_properties.width = 50.f;
		water_properties.tex_width = 10.f;
		water_properties.tex_height = 10.f;
		water = new Plane(water_properties);
		water->name = "Water";
		water->Attach(new Transform(glm::vec3(0, 5.f, 4.f)));// 4.f)));

		// ----------------------- Load materials from file or set from properties above  ----------------------- // 
		Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/metal_floor.mtl");
		//Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/black_floor.mtl");
		auto matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/metal_floor.mtl");
		//auto matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/black_floor.mtl");
		Material* floor_material = static_cast<Material*>(matptr.get());
		//floor_material->setShader("../Sandbox/res/Shaders/textured.shader");

		MaterialProperties water_material_properties;
		//water_material_properties.base_colour = glm::vec4(0.4f, 0.85f, 0.89f, 1.0f);
		water_material_properties.specular_colour = glm::vec4(1.f, 1.f, 1.f, 15.0f);
		//water_material_properties.specular_colour = glm::vec4(1.f, 1.f, 1.f, 1.0f);
		water_material_properties.specular_coef = 10000.f;
		water_material_properties.specular_coef = 10.f;
		water_material_properties.asset_sources = {
			{Aurora::MaterialProperties::ASSET::SHADER, "../Sandbox/res/Shaders/water.shader"},
		};
		water_material_properties.texture_sources = {
			{Aurora::MaterialProperties::TEXTURE::DIFFUSE, "../Sandbox/res/Textures/water-diffuse.jpg"},
			{Aurora::MaterialProperties::TEXTURE::NORMAL, "../Sandbox/res/Textures/water-normal.jpg" },
			{Aurora::MaterialProperties::TEXTURE::SPECULAR, "../Sandbox/res/Textures/terrain-height.jpg" },
		};
		water_material_properties.name = "Test Water";
		// old way of specifying texture filtering is not compatible now ...
		// ... because textures get generated when loading a material
		//cube_material->setTextureMinFilter(GL_LINEAR_MIPMAP_LINEAR);
		//cube_material->setTextureMagFilter(GL_LINEAR);
		// so instead, set the filters in the material properties ...
		// ... prior to creating the material itself
		water_material_properties.min_filter = GL_LINEAR_MIPMAP_LINEAR;
		water_material_properties.mag_filter = GL_LINEAR;
		Material* water_material = new Material(water_material_properties);

		MaterialProperties cube2_material_properties;
		cube2_material_properties.base_colour = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
		cube2_material_properties.asset_sources = {
			{Aurora::MaterialProperties::ASSET::SHADER, "../Sandbox/res/Shaders/spinning.shader"},
			//{Aurora::MaterialProperties::ASSET::SHADER, "./res/Shaders/textured.shader"},
		};
		cube2_material_properties.name = "Test Box";
		Material* cube2_material = new Material(cube2_material_properties);

		Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/pink.mtl");
		matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/pink.mtl");
		Material* sphere_material = static_cast<Material*>(matptr.get());


		Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/metal_box.mtl");
		matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/metal_box.mtl");
		Material* cube_material = static_cast<Material*>(matptr.get());




		// ------------------------------------------- END Load materials ------------------------------------------- // 

		// Attach a RendererComponent with the material:
		cube->Attach(new RendererComponent(cube->getVAO(), cube->getEBO(), cube_material));
		cube2->Attach(new RendererComponent(cube2->getVAO(), cube2->getEBO(), cube2_material));
		sphere->Attach(new RendererComponent(sphere->getVAO(), sphere->getEBO(), sphere_material));
		floor->Attach(new RendererComponent(floor->getVAO(), floor->getEBO(), floor_material));
		water->Attach(new RendererComponent(water->getVAO(), water->getEBO(), water_material));

		sphere = new Sphere(PrimitiveObjectProperties());
		sphere1 = new Sphere(PrimitiveObjectProperties());
		sphere->name = "sphere_test";
		sphere1->name = "sphere1_test";
		sphere->Attach(new Transform(glm::vec3(0, 0.0, 0)));
		sphere1->Attach(new Transform(glm::vec3(0, 0.0, 0)));
		sphere_material->setShader("../Sandbox/res/Shaders/textured.shader");
		sphere->Attach(new RendererComponent(sphere->getVAO(), sphere->getEBO(), sphere_material));
		sphere1->Attach(new RendererComponent(sphere1->getVAO(), sphere1->getEBO(), sphere_material));

		PrimitiveObjectProperties light_orb;
		light_orb.radius = 0.05f;
		Cube* light_source = new Cube(light_orb);
		//light_source->Attach(new Transform(glm::vec3(0.8f, 0.1f, 0.f)));
		//light_source->Attach(new Transform(glm::vec3(0.8f, 100.1f, 0.f)));
		float u_time = (float)ImGui::GetTime();
		light_source->Attach(new Transform(glm::vec3(cos(u_time * 10.0 * 0.01), sin(u_time * 10.0 * 0.01), cos(u_time * 10.0 * 0.01))));
		//light_source->Attach(new RendererComponent(Material()));
		light_source->Attach(new RendererComponent(light_source->getVAO(), light_source->getEBO(), sphere_material));


		exampleObj* camera = new exampleObj(1);
		camera->name = "camera";
		// Set up Camera:
		camera->Attach(new Transform(glm::vec3(0, 0, 2)));
		camera->Attach(new Camera());
		camera->Attach(new CameraMoveTest());

		//exampleObj* motion_obj = new exampleObj(2);
		//motion_obj->name = "motion Joints";
		//motion_obj-> Attach(new Transform(glm::vec3(0, 0, 0)));
		//motion_obj->Attach(new MotionSystem());

		//exampleObj* scene_director = new exampleObj(3);
		//scene_director->name = "Scene Director";
		//scene_director->Attach(new Transform(glm::vec3(0, 0, 0)));
		//scene_director->Attach(new SceneDirector());

		//MotionSystem* motiontest = new MotionSystem();
		//motiontest->initialize();
		//motiontest->drawJoint(1);
		
		Global::GetInstance().RegisterObject(camera);
		Global::GetInstance().RegisterObject(sphere);
		//Global::GetInstance().RegisterObject(sphere1);
		//Global::GetInstance().RegisterObject(cube);
		//Global::GetInstance().RegisterObject(cube2);
		Global::GetInstance().RegisterObject(floor);
		//Global::GetInstance().RegisterObject(motion_obj);
		//Global::GetInstance().RegisterObject(scene_director);
		//Global::GetInstance().RegisterObject(water);
		
		/* -------------------------------- END SCENE OBJECTS ---------------------------------- */

		while (!glfwWindowShouldClose(m_glfwWindow))
		{
			imguiLogic();
			gameLogic();
			rendererRender();
			imguiRender(io);

			glfwSwapBuffers(m_glfwWindow);

			glfwPollEvents();
		}

		imguiShutdown();

		glfwDestroyWindow(m_glfwWindow);
		glfwTerminate();
	}

	void WindowsWindow::controllerInit()
	{
#ifdef AURORA_GENERATE_DEBUG
		MemoryTracker::GetInstance().init();
#endif // AURORA_GENERATE_DEBUG
		I18N::GetInstance().init();

		// game global controller
		Global::GetInstance().init();

		InputSystem::GetInstance().init(m_glfwWindow);
		EventSystem::GetInstance().init();
		RenderSystem::GetInstance().init();
		io = imguiInit(); // replaced by gui system
	}

	void WindowsWindow::renderAPIHintSet()
	{
		// for OpenGL
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

		//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	}

	ImGuiIO WindowsWindow::imguiInit()
	{
		IMGUI_CHECKVERSION();
		m_engineGUI.imGuiContext = ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO(); (void)io;
		// "- Hold SHIFT to disable docking (if io.ConfigDockingWithShift == false, default)" "\n"
		// "- Hold SHIFT to enable docking (if io.ConfigDockingWithShift == true)" "\n"
		io.ConfigDockingWithShift = false; // false means that we can dock windows simply sby dragging 
		io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
		io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;

		ImGui::StyleColorsDark();

		ImGui_ImplGlfw_InitForOpenGL(m_glfwWindow, true);
		ImGui_ImplOpenGL3_Init("#version 150");

		// init imgui log
		Log::init_imgui_log();

		return io;
	}

	void WindowsWindow::imguiLogic()
	{ // need discussion
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		if (io.WantCaptureMouse)
		{
			std::cout << "MOUSE CAPTURED" << std::endl;
		}
		else
		{
		}

		show_panel_example = false;
		//ImGui::ShowDemoWindow();
		m_engineGUI.RenderDockspace(RenderSystem::GetInstance().GetRenderer());
	}

	void WindowsWindow::gameLogic()
	{ // main logic of the game
		Global::GetInstance().Update();

		InputSystem::GetInstance().TriggerKeep();
	}

	void WindowsWindow::rendererRender()
	{ // need a renderer for OpenGL
	  //for changing render API and muti-thread render
		Global::GetInstance().Render();
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void WindowsWindow::imguiRender(ImGuiIO io)
	{
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
		{
			GLFWwindow* backup_current_context = glfwGetCurrentContext();
			ImGui::UpdatePlatformWindows();
			ImGui::RenderPlatformWindowsDefault();
			glfwMakeContextCurrent(backup_current_context);
		}
	}

	void WindowsWindow::imguiShutdown()
	{
		ImGui_ImplOpenGL3_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();
	}

}