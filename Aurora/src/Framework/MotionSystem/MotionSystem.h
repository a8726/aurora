#pragma once

#include "Framework/SampleECS/Component/ComponentBase.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/PrimitiveObject.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/Sphere.h"
#include "Framework/MotionSystem/ScratchBuffer.h"
#include "ozz/animation/runtime/skeleton.h"
#include "ozz/animation/runtime/animation.h"
#include "ozz/animation/runtime/local_to_model_job.h"
#include "ozz/animation/runtime/sampling_job.h"
#include "ozz/base/maths/simd_math.h"
#include "ozz/base/maths/soa_transform.h"
#include "ozz/base/maths/vec_float.h"
#include "ozz/base/containers/vector.h"
#include "ozz/mesh.h"
#include "ozz/renderer.h"
#include <Framework/MotionSystem/ScratchBuffer.h>

#include "Core/ResourceManager/ResourceTypes/Mesh.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/Cube.h"
#include "Framework/SampleECS/Component/DefaultComponents/RendererComponent.h"

#include "Core/StaticReflection.h"


namespace Aurora
{
	class MotionSystem : public ComponentBase
	{

	public:
		MotionSystem();
		~MotionSystem();
		MotionSystem(const MotionSystem&);

		DEFINE_PROPERTIES(
			(glm::vec3) light_pos,
			(float) time_ratio,
			(int) id,
			(float) blend_ratio
		)

	public:
		void PlayIdleAndWalk(Object* obj, float blender_ratio);
		void PlayIdleAndSit(Object* obj, float blender_ratio);
		void PlayClick(Object* obj);

		void StopAnim();
		void StartAnim();
		void SetTimeRatio(float time_ratio);

		//load animaiton file
		bool loadAnimation(const char* _filename, ozz::animation::Animation* _animation);

		bool loadAnimation(const char* _filename);

		//load skeleton file 
		bool loadSkeleton(const char* filename, ozz::animation::Skeleton* _skeleton);

		//load mesh file
		bool LoadMeshes(const char* _filename,ozz::vector<ozz::sample::Mesh>* _meshes);

		void test();

		bool drawJoint(Object* obj, int animation_id);

		void setTimeRatio(float ratio_);

		void updateRatio(const ozz::animation::Animation& _animation1, const ozz::animation::Animation& _animation2, float blend_ratio, float _dt, float speed);

		void updateJoint();

		bool initialize();

		void UpdateRuntimeParameters(int animation_num_l, int animation_num_r, float blend_ratio);

		bool drawBlendingJob(Object* obj, int amimation_num_l, int animation_num_r, float blend_ratio, bool isLOp = false /* reverse the animation */, bool isROp = false);

		bool drawSkinnedMesh(unsigned int index, Object* obj, const ozz::sample::Mesh& _mesh, const ozz::span<ozz::math::Float4x4> _skinning_matrices,
			const ozz::math::Float4x4& _transform, const ozz::sample::Renderer::Options& _options);

		//start a default blending job
		void initializeBlendingJob();

		virtual std::string getTypeDes() override
		{
			return "MotionSystem";
		}

		bool isStoped = false;

	private:

		std::vector<glm::vec3> default_colors;

		virtual void OnEnable(Object* obj) override;
		virtual void OnDisable(Object* obj) override;
		virtual void OnStart(Object* obj) override;
		virtual void Update(Object* obj) override;
		virtual void OnShutdown(Object* obj) override;

		//Update the Runtime parameters for every animations in motion blending 
		void updateRuntimeParameters();

		//Sampler for animation
		struct Sampler {
			// Constructor, default initialization.
			Sampler() : weight(1.f),
			speed(1.f){}

			// Blending weight for the layer.
			float weight;

			float speed;

			// Runtime animation.
			ozz::animation::Animation animation;

			// Sampling context.
			ozz::animation::SamplingJob::Context context;

			// Buffer of local transforms as sampled from animation_.
			ozz::vector<ozz::math::SoaTransform> locals;
		};

		ozz::animation::Animation animation_;

		ozz::animation::Skeleton skeleton_;

		// Buffer of local transforms as sampled from animation_.
		ozz::vector<ozz::math::SoaTransform> locals_;

		ozz::animation::SamplingJob::Context context_;

		// Buffer of model space matrices.
		ozz::vector<ozz::math::Float4x4> models_;

		//Default file path(used for testing skeleton)
		std::string fbx_skeletion = "../Sandbox/res/ozz/skeleton_robot.ozz";
		std::string fbx_mesh = "../Sandbox/res/ozz/mesh_robot.ozz";

		std::string anim_click_path = "../Sandbox/res/ozz/anim_click.ozz";

		//Default file path(used for testing animation)
		std::string anim_idle_path = "../Sandbox/res/ozz/anim_idle.ozz";
		std::string anim_walk_path = "../Sandbox/res/ozz/anim_walk.ozz";
		std::string anim_sit_path = "../Sandbox/res/ozz/anim_sit.ozz";
 
		//delta time for testing (update later)
		float dt = 1.f / 60.f;

		float previous_time_ratio;

		float playback_speed;

		ozz::vector<Sphere*> joint_buffer;

		ozz::vector<Sampler*> samplers;

		float threshold_;

		int test_switch = 0;

		ozz::sample::ScratchBuffer scratch_buffer_;

		// The mesh used by the sample.
		ozz::vector<ozz::sample::Mesh> meshes_;

		// Buffer of local transforms which stores the blending result.
		ozz::vector<ozz::math::SoaTransform> blended_locals_;

		// Buffer of skinning matrices, result of the joint multiplication of the
		// inverse bind pose with the model space matrix.
		ozz::vector<ozz::math::Float4x4> skinning_matrices_;

		// Mesh rendering options.
		ozz::sample::Renderer::Options render_options_;

		MaterialProperties mesh_material_properties;

		std::vector<RendererComponent*> renders;
	};
}
