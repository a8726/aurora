#pragma once

#include "../thirdparty/stb/stb_image.h" // instead of CImg
#include "Core/ResourceManager/Resource.h"

#include "Core/Renderer/openglMacros.h"


namespace Aurora
{
	const unsigned int AURORA_TEXTURE_TYPE_DIFFUSE = 0x0;
	const unsigned int AURORA_TEXTURE_TYPE_NORMAL = 0x1;
	const unsigned int AURORA_TEXTURE_TYPE_SPECULAR = 0x2;
	const unsigned int AURORA_TEXTURE_TYPE_BUMP = 0x3;
	const unsigned int AURORA_TEXTURE_TYPE_OTHER = 0x4;
}

namespace Aurora
{
	class Texture : public Resource
	{
	public:

		Texture();
		~Texture();

		const unsigned char* getData();
		unsigned int getWidth()  const { return m_width; }
		unsigned int getHeight() const { return m_height; }
		void bind();
		void generate();

		void setType(unsigned type) { m_type = type; }
		void setMinFilter(unsigned filter) { m_min_filter = filter; }
		void setMagFilter(unsigned filter) { m_mag_filter = filter; }
		void setWrapping(unsigned wrapping) { m_wrapping = wrapping; }
		void setTexturePath(std::string path) { m_texture_path = path; }
		void setTextureScaleU(float scaleU) { m_scaleU = scaleU; }
		void setTextureScaleV(float scaleV) { m_scaleV = scaleV; }

		std::string getPath() { return m_texture_path; }

		//cimg_library::CImg<GLubyte>* m_image;
		stbi_uc* m_image;

		unsigned m_type = AURORA_TEXTURE_TYPE_DIFFUSE;
		unsigned m_wrapping = GL_REPEAT; // GL_REPEAT;

		// keep track how many instances of same texture is used:
		unsigned m_instances = 0;


		bool load(const std::string path) override;
		void load();
		void removeInstance() { m_instances--; };
		void addInstance() { m_instances++; };

		Texture* clone() const override;

	protected: // changed from private so I could write inheriting class 3DTexture

		int m_width = 0;
		int m_height = 0;
		int m_channels = 0;

		//CImg* p_qimage;
		std::string m_texture_path = "../Sandbox/res/Textures/texdefault.bmp";
		//GLubyte* m_data;
		unsigned char* m_data;

	private:
		unsigned m_ID = -1;
		unsigned m_min_filter = GL_LINEAR_MIPMAP_LINEAR; // 0x2600 = 0x2703;// 9987
		unsigned m_mag_filter = GL_LINEAR; // 0x2703;// 9987
		float    m_scaleU = 1.f;
		float	 m_scaleV = 1.f;
	};
}