#pragma once

#include "Framework/SampleECS/Object/Object.h"

class exampleObj : public Aurora::Object
{
public:
	exampleObj(int _id) : id(_id) {}

	//property_vtable()

	int getId() { return id; }
	virtual const char* getTypeDes() override
	{
		return "exampleObj";
	}

private:
	virtual void render() override;

	int id;

};
