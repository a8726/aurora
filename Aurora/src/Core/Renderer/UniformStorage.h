#pragma once

#include <string>
#include "../thirdparty/glm/glm.hpp"
#include <variant>

namespace Aurora
{
	class Shader;

	struct Uniform
	{
		const char* m_identifier;
		std::variant<int, float, glm::mat4> m_data;

		Uniform(const char* identifier, std::variant<int, float, glm::mat4> data)
			: m_identifier(identifier), m_data(data) {}
	};

	class UniformStorage
	{
	public:
		UniformStorage() {};
		//UniformStorage(const glm::mat4x4& value);
		~UniformStorage() = default; 

		void bind() const;
		void unbind() const;

		//void addUniform(const std::string& identifier, std::variant<int, float, glm::mat4> data)
		void addUniform(const char* identifier, std::variant<int, float, glm::mat4> data)
		{
			m_uniforms.emplace_back(identifier, data);
		}

		inline unsigned int getCount() const {  }
		//glm::mat4x4 u_model;
		std::vector<Uniform> m_uniforms;
		//std::unordered_map<const char*, std::variant<int, float, glm::mat4>> m_unif;

		//void setUniform(const char* identifier, std::variant<int, float, glm::mat4> data)
		//{
		//	if (m_unif.find(identifier) != m_unif.end())
		//	{
		//		// replace existing data for that uniform:
		//		m_unif[identifier] = data;
		//		return;
		//	}

		//	m_unif.emplace(identifier, data);
		//}

	private:
		// properties here:
	};

}