#include "aurora_pch.h"
#include "InspectorPanel.h"

#include "imgui.h"
#include "imgui_internal.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "GuiTypeSupport.h"

#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"
#include "Framework/SampleECS/Component/DefaultComponents/RendererComponent.h"
#include "Framework/SampleECS/Component/DefaultComponents/Camera.h"
#include "Functions/CameraMoveTest.h"
#include "Functions/ObjMoveTest.h"
#include "Functions/SceneDirector.h"
#include "Functions/SpinningComp.h"
#include "Framework/MotionSystem/MotionSystem.h"

namespace Aurora
{
	void InspectorPanel::draw(ImGuiContext* imguiContext, HierarchyPanel* hierarchy)
	{
        float indentSpace = imguiContext->Style.IndentSpacing / 2.0f;

        ImGui::Begin(LOCALIZATION("Inspector"), NULL, ImGuiWindowFlags_MenuBar);
        {
            Object* obj = (*Global::GetInstance().GetSceneObjects())[hierarchy->selectedId];

            ImGui::GetCurrentWindow()->DC.CursorPos.y += imguiContext->Style.ItemInnerSpacing.x / 2;
            ImGui::Text("Object Name: "); ImGui::SameLine();
            ImGui::GetCurrentWindow()->DC.CursorPos.y -= imguiContext->Style.ItemInnerSpacing.x / 2;

            char* nameChanged = const_cast<char*>(obj->name.c_str());
            nameChanged += '\0';

            ImGui::PushItemWidth(-1);
            ImGui::SetNextItemWidth(150);
            ImGui::InputText("##label", nameChanged, 20);
            ImGui::PopItemWidth();
            obj->name = nameChanged;

            for (size_t i = 0; i < obj->comps.size(); i++)
            {
                SwitchComponent(obj->getComp(i));
            }

            ImGui::End();
        }
	}

    void InspectorPanel::SwitchComponent(ComponentBase* comp)
    {
        std::string typeDes = comp->getTypeDes();

        // default components
        if (typeDes == "Transform")
        {
            Transform* tsm = static_cast<Transform*>(comp);
            if (ImGui::CollapsingHeader(typeDes.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
            {
                drawClass(tsm);
            }
        }
        else if (typeDes == "RendererComponent")
        {
            RendererComponent* renderer = static_cast<RendererComponent*>(comp);
            if (ImGui::CollapsingHeader(typeDes.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
            {
                drawClass(renderer);
            }
        }
        else if (typeDes == "Camera")
        {
            Camera* camera = static_cast<Camera*>(comp);
            if (ImGui::CollapsingHeader(typeDes.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
            {
                drawClass(camera);
            }
        }
        else if (typeDes == "CameraMoveTest")
        {
            CameraMoveTest* cameraMoveTest = static_cast<CameraMoveTest*>(comp);
            if (ImGui::CollapsingHeader(typeDes.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
            {
                drawClass(cameraMoveTest);
            }
        }
        else if (typeDes == "ObjMoveTest")
        {
            ObjMoveTest* objMoveTest = static_cast<ObjMoveTest*>(comp);
            if (ImGui::CollapsingHeader(typeDes.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
            {
                drawClass(objMoveTest);
            }
        }
        else if (typeDes == "SceneDirector")
        {
            SceneDirector* sceneDirector = static_cast<SceneDirector*>(comp);
            if (ImGui::CollapsingHeader(typeDes.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
            {
                drawClass(sceneDirector);
            }
        }
        else if (typeDes == "SpinningComp")
        {
            SpinningComp* spinningComp = static_cast<SpinningComp*>(comp);
            if (ImGui::CollapsingHeader(typeDes.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
            {
                drawClass(spinningComp);
            }
        }
        else if (typeDes == "MotionSystem")
        {
            MotionSystem* motionSystem = static_cast<MotionSystem*>(comp);
            if (ImGui::CollapsingHeader(typeDes.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
            {
                drawClass(motionSystem);
            }
        }
    }
}
