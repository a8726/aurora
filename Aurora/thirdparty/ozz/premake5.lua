project "ozz"
    kind "StaticLib"
    language "C++"
	staticruntime "off"

	location "%{wks.location}/build"

    targetdir (targetOutputDir .. "/%{prj.name}")
	objdir (objOutputDir .. "/%{prj.name}")
	
	includedirs
	{
		"include",
		"src",
		thirdparty.. "/FBXSDK/include",
	}
	
    files
	{
		"src/**.h",
		"src/**.cc",
		"include/**.h",
		"include/**.c",
		"include/**.cc",
        "**.lua",
	}

    filter "system:windows"
		systemversion "latest"
		cppdialect "C++17"

    filter "configurations:Debug"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		runtime "Release"
		optimize "off"