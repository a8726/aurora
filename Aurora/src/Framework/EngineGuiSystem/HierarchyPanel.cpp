#include "aurora_pch.h"
#include "HierarchyPanel.h"

#include "imgui.h"
#include "imgui_internal.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "Framework/SampleECS/System/Global.h"

namespace Aurora
{
	unsigned int HierarchyPanel::selectedId = 0;

	void HierarchyPanel::draw(ImGuiContext* imguiContext)
	{
		float indentSpace = imguiContext->Style.IndentSpacing / 2.0f;

		ImGui::Begin(LOCALIZATION("Hierarchy"), NULL, ImGuiWindowFlags_MenuBar);
		{
			if (ImGui::BeginPopupContextWindow("edit", ImGuiPopupFlags_MouseButtonRight))
			{
				ImGui::Text(" --- Editor --- ");
				ImGui::MenuItem("test");
				ImGui::MenuItem("test1");
				ImGui::MenuItem("test2");
				ImGui::EndPopup();
			}

			ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow
				| ImGuiTreeNodeFlags_OpenOnDoubleClick
				| ImGuiTreeNodeFlags_SpanAvailWidth;

			ObjectStore* objs = Global::GetInstance().GetSceneObjects();
			for (size_t i = 0; i < objs->size(); i++)
			{
				Object* obj = (*objs)[i];
				bool selected = (selectedId == i);

				if (obj->children.size() > 0)
				{
					ImGui::Unindent(indentSpace);
					if (ImGui::TreeNodeEx(obj->name.c_str(), flags, &selected))
					{
						ImGui::TreePop();
					}
					ImGui::Indent(indentSpace);
				}
				else
				{
					ImGui::Indent(indentSpace);
					ImGui::Selectable(obj->name.c_str(), &selected, ImGuiSelectableFlags_AllowDoubleClick);
					ImGui::Unindent(indentSpace);
				}

				if (selected)
				{
					selectedId = i;
				}
			}

			ImGui::End();
		}
	}
}
