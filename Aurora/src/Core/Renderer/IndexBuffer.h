#pragma once

namespace Aurora
{

	/*  Reference:
	 *  Title: Renderer
	 *  Author: Studio Cherno
	 *  Date: 2021
	 *  Availability: https://github.com/TheCherno/Hazel/tree/master/Hazel/src/Hazel/Renderer
	 *  Type: Tutorial/Notes
	 *  Renderer System was based on and extended upon the Hazel Game Engine Renderer ...
	 *  ... includes: Renderer, VertexArray, VertexBuffer, VertexBufferLayout, IndexBuffer and Shader
	 */

	class IndexBuffer 
	{
	private:
		unsigned m_RendererID;
		unsigned int m_Count;
	public:
		IndexBuffer() = default;
		IndexBuffer(const unsigned int* data, unsigned int count);
		IndexBuffer(const uint16_t* data, unsigned int count);
		~IndexBuffer();

		void bind() const;
		void unbind() const;

		inline unsigned int getCount() const { return m_Count;  }
	};

}
