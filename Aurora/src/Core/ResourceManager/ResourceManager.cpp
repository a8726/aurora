#include "aurora_pch.h"

#include "ResourceManager.h"

namespace Aurora
{

	std::shared_ptr<Resource> ResourceManager::getResource(const std::string& guid)
	{
		// Try to get the resource
		std::map<std::string, std::shared_ptr<Resource>>::iterator it;
		it = resources.find(guid);

		// If the resource was found
		if (it != resources.end())
		{
			AURORA_LOG_DEBUG("Resource " + guid + " has " + std::to_string(it->second.use_count()) + " references.");
			AURORA_LOG_DEBUG("Resource pointer returned for GUID " + guid);
			return it->second;
		}

		AURORA_LOG_ERROR("Resource with GUID " + guid + " was not found, returned nullptr.");
		// Resource isn't loaded, return nullptr
		return std::shared_ptr<Resource>(nullptr);
	}

	bool ResourceManager::unloadResource(const std::string& guid)
	{
		std::shared_ptr<Resource> resource = getResource(guid);
		// Check the resource exists at all
		if (resource == nullptr)
		{
			AURORA_LOG_DEBUG("Resource unload for GUID " + guid + " requested. The resource was not loaded.");
			// Return true if it never existed
			return true;
		}

		// Check nothing else is using the pointer, and remove it from the map if not
		if (resource.use_count() <= 2)	// 2 as this method will tempoarily increase the reference count by 1
		{
			AURORA_LOG_DEBUG("Resource with GUID " + guid + " was unloaded.");
			resources.erase(guid);
			return true;
		}

		AURORA_LOG_ERROR("Resource unload for GUID " + guid + " requested. The resource could not be unloaded as it is still in use.");
		// This resource is still in use, all other shared_ptr instances must be deleted first
		return false;
	}

}