#pragma once
#include <string_view>

// Pure virtual (interface) class for all resources
namespace Aurora
{

	class Resource
	{
	public:
		// Virtual destructor; subclasses may have dynamic memory and/or data in VRAM
		virtual ~Resource() = default;

		// Instantiate from file
		virtual bool load(const std::string path) = 0;

		// Return copy
		virtual Resource* clone() const = 0;
	};

}


