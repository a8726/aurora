#pragma once

// Standard Libraries
#include <map>
#include <string>
#include <memory>

// Custom Imports
#include <Core/Singleton.h>
#include <Core/ResourceManager/Resource.h>
#include <Core/ResourceManager/ResourceTypes/Model.h>

namespace Aurora
{

	class ResourceManager : public Singleton<ResourceManager>
	{
	private:
		/// <summary>
		///	Mapping from GUID to resource shared_ptr.
		///	Default GUID is the filepath. In case of duplicates, _duplicate_* is appended where * is the index of this duplicate.
		///	Note that attempting to load a file with the path {name}_duplicate_x where the file {name} has been duplicated x times will fail,
		///		as a resource with the same GUID will already exist, whether the files are unique or not.
		/// </summary>
		std::map<std::string, std::shared_ptr<Resource>> resources;
	public:
		// Calls load function on a resource of specified type

		/// <summary>
		///	Calls load function of the specified type.
		/// </summary>
		/// <typeparam name="T">The resource type. Must implement the Resource interface.</typeparam>
		/// <param name="path">The filepath of the resource. This will become the GUID of the resource in the resource map.</param>
		/// <returns>true if the resource exists in the resource map, or if it was loaded and added successfully. Returns false otherwise.</returns>
		template <typename T>
		bool loadResource(const std::string path);

		/// <summary>
		/// Gets a pointer to the resource specified.
		/// </summary>
		/// <param name="guid">The GUID of the resource to search for.</param>
		/// <returns>shared_ptr to the resource if found, nullptr otherwise.</returns>
		std::shared_ptr<Resource> getResource(const std::string& guid);

		/// <summary>
		/// Removes a resource from the resource map, iff nothing else is using it.
		/// </summary>
		/// <param name="guid">The GUID of the resource to try to remove.</param>
		/// <returns>true if nothing else was using the resource, thus it was removed, or if the resource otherwise was not present in the map. Returns false otherwise.</returns>
		bool unloadResource(const std::string& guid);
	};

	template<typename T>
	inline bool ResourceManager::loadResource(const std::string path)
	{
		// Check if path is blank
		if (path == "")
		{
			AURORA_LOG_ERROR("Resource load failed! Empty path specified");
			return false;
		}
		// Check if this resource has been loaded already
		if (resources.count(path))
		{
			AURORA_LOG_DEBUG("Resource load for " + path + " requested. The resource was already loaded.");
			return true;
		}
		// Try to load the resource
		std::shared_ptr<Resource> resource = std::make_shared<T>();

		if ((*resource).load(path))
		{
			AURORA_LOG_DEBUG("Resource " + path + " loaded successfully.");
			// Add to the map
			resources.emplace(path, resource);
			return true;
		}

		AURORA_LOG_ERROR("Resource " + path + " could not be loaded.");
		// Resource wasn't present, and couldn't be loaded
		
		return false;
	}

}