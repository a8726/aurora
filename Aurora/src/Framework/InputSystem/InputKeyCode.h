#pragma once

#define keycode unsigned long long
#define GetKeyCode(num) UINT64_MAX - num

// mouse 0 - 20
const keycode Mouse_LEFT_Press = GetKeyCode(0);
const keycode Mouse_LEFT_Release = GetKeyCode(1);
const keycode Mouse_LEFT_Keep = GetKeyCode(2);

const keycode Mouse_RIGHT_Press = GetKeyCode(3);
const keycode Mouse_RIGHT_Release = GetKeyCode(4);
const keycode Mouse_RIGHT_Keep = GetKeyCode(5);

const keycode Mouse_MIDDLE_Press = GetKeyCode(6);
const keycode Mouse_MIDDLE_Release = GetKeyCode(7);
const keycode Mouse_MIDDLE_Keep = GetKeyCode(8);


// keyboard 21 - ?
const keycode Key_Q_Press = GetKeyCode(21);
const keycode Key_Q_Release = GetKeyCode(22);
const keycode Key_Q_Keep = GetKeyCode(23);

const keycode Key_W_Press = GetKeyCode(24);
const keycode Key_W_Release = GetKeyCode(25);
const keycode Key_W_Keep = GetKeyCode(26);

const keycode Key_E_Press = GetKeyCode(27);
const keycode Key_E_Release = GetKeyCode(28);
const keycode Key_E_Keep = GetKeyCode(29);

const keycode Key_R_Press = GetKeyCode(30);
const keycode Key_R_Release = GetKeyCode(31);
const keycode Key_R_Keep = GetKeyCode(32);

const keycode Key_T_Press = GetKeyCode(33);
const keycode Key_T_Release = GetKeyCode(34);
const keycode Key_T_Keep = GetKeyCode(35);

const keycode Key_Y_Press = GetKeyCode(36);
const keycode Key_Y_Release = GetKeyCode(37);
const keycode Key_Y_Keep = GetKeyCode(38);

const keycode Key_U_Press = GetKeyCode(39);
const keycode Key_U_Release = GetKeyCode(40);
const keycode Key_U_Keep = GetKeyCode(41);

const keycode Key_I_Press = GetKeyCode(42);
const keycode Key_I_Release = GetKeyCode(43);
const keycode Key_I_Keep = GetKeyCode(44);

const keycode Key_O_Press = GetKeyCode(45);
const keycode Key_O_Release = GetKeyCode(46);
const keycode Key_O_Keep = GetKeyCode(47);

const keycode Key_P_Press = GetKeyCode(48);
const keycode Key_P_Release = GetKeyCode(49);
const keycode Key_P_Keep = GetKeyCode(50);

const keycode Key_A_Press = GetKeyCode(51);
const keycode Key_A_Release = GetKeyCode(52);
const keycode Key_A_Keep = GetKeyCode(53);

const keycode Key_S_Press = GetKeyCode(54);
const keycode Key_S_Release = GetKeyCode(55);
const keycode Key_S_Keep = GetKeyCode(56);

const keycode Key_D_Press = GetKeyCode(57);
const keycode Key_D_Release = GetKeyCode(58);
const keycode Key_D_Keep = GetKeyCode(59);

const keycode Key_F_Press = GetKeyCode(60);
const keycode Key_F_Release = GetKeyCode(61);
const keycode Key_F_Keep = GetKeyCode(62);

const keycode Key_G_Press = GetKeyCode(63);
const keycode Key_G_Release = GetKeyCode(64);
const keycode Key_G_Keep = GetKeyCode(65);

const keycode Key_H_Press = GetKeyCode(66);
const keycode Key_H_Release = GetKeyCode(67);
const keycode Key_H_Keep = GetKeyCode(68);

const keycode Key_J_Press = GetKeyCode(69);
const keycode Key_J_Release = GetKeyCode(70);
const keycode Key_J_Keep = GetKeyCode(71);

const keycode Key_K_Press = GetKeyCode(72);
const keycode Key_K_Release = GetKeyCode(73);
const keycode Key_K_Keep = GetKeyCode(74);

const keycode Key_L_Press = GetKeyCode(75);
const keycode Key_L_Release = GetKeyCode(76);
const keycode Key_L_Keep = GetKeyCode(77);

const keycode Key_Z_Press = GetKeyCode(78);
const keycode Key_Z_Release = GetKeyCode(79);
const keycode Key_Z_Keep = GetKeyCode(80);

const keycode Key_X_Press = GetKeyCode(81);
const keycode Key_X_Release = GetKeyCode(82);
const keycode Key_X_Keep = GetKeyCode(83);

const keycode Key_C_Press = GetKeyCode(84);
const keycode Key_C_Release = GetKeyCode(85);
const keycode Key_C_Keep = GetKeyCode(86);

const keycode Key_V_Press = GetKeyCode(87);
const keycode Key_V_Release = GetKeyCode(88);
const keycode Key_V_Keep = GetKeyCode(89);

const keycode Key_B_Press = GetKeyCode(90);
const keycode Key_B_Release = GetKeyCode(91);
const keycode Key_B_Keep = GetKeyCode(92);

const keycode Key_N_Press = GetKeyCode(93);
const keycode Key_N_Release = GetKeyCode(94);
const keycode Key_N_Keep = GetKeyCode(95);

const keycode Key_M_Press = GetKeyCode(96);
const keycode Key_M_Release = GetKeyCode(97);
const keycode Key_M_Keep = GetKeyCode(98);
