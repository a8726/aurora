#pragma once

#define MOUSE_SOLVE(keycode) \
case GLFW_MOUSE_BUTTON_ ## keycode: \
{ \
	double xpos, ypos; \
	glfwGetCursorPos(window, &xpos, &ypos); \
	if (action == GLFW_PRESS) \
	{ \
		MouseInteractionResponse mouseclick(xpos, ypos); \
		EventSystem::GetInstance().BroadcastEvent(Mouse_ ## keycode ## _Press, &mouseclick); \
		InputSystem::GetInstance().SetMouseKeep(Mouse_ ## keycode ## _Keep, true); \
	} \
	else if (action == GLFW_RELEASE) \
	{ \
		MouseInteractionResponse mouseclick(xpos, ypos); \
		EventSystem::GetInstance().BroadcastEvent(Mouse_ ## keycode ## _Release, &mouseclick); \
		InputSystem::GetInstance().SetMouseKeep(Mouse_ ## keycode ## _Keep, false); \
	} \
	break; \
} \

MOUSE_SOLVE(LEFT)
MOUSE_SOLVE(RIGHT)
MOUSE_SOLVE(MIDDLE)

#define MOUSE_SOLVE