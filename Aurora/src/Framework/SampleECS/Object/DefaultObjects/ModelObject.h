#pragma once

// Stdlib
#include <string>
#include <memory>

// Custom
#include "Framework/SampleECS/Object/Object.h"
#include "Core/ResourceManager/ResourceTypes/Model.h"

namespace Aurora
{

	class ModelObject : public Object
	{
	private:
		std::shared_ptr<Model> m_Model;

		void render() override;
		void render(Transform* transform);
	public:
		// Must reference a loaded model
		ModelObject(const std::shared_ptr<Model> model);


		// Default destructor is used which handles cleaning the shared_ptr
		// Note that cleanup of actual models is not handled here but rather by he ResourceManager
		
		const char* getTypeDes() override
		{
			return "ModelObject";
		}

		std::vector<RenderParameters*> getRenderParameters();
	};

}