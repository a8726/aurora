#include "aurora_pch.h"
#include "Material.h"
#include "MaterialProperties.h"
#include "Core/ResourceManager/ResourceManager.h"

// include the C++ standard libraries we want
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <algorithm>

#define MAXIMUM_LINE_LENGTH 1024

namespace Aurora
{
    Material::~Material()
    {
        //delete m_shader;
        //for (auto i : m_textures)
        //{
        //	m_textures.pop_back();
        //}
    }

	bool Material::load(const std::string path)
	{
        /*
          The load() function contains modified code provided from:
          COMP5812M Foundations of Modelling and Rendering - 
          Coursework 2 Raytracing - TexturedObject.cpp
          University of Leeds, COMP 5812M Foundations of Modelling & Rendering
          September, 2020
        */

        // open the input files for the geometry & texture
        std::ifstream materialFile(path.c_str());
        // create a read buffer
        char readBuffer[MAXIMUM_LINE_LENGTH];

        // temporary variables to read data into from the file:
        float vec1st, vec2nd, vec3rd, vec4th;

        if (!materialFile.is_open())
        {
            AURORA_LOG_ERROR("Could not open material file");
            return false;
        }

        // the rest of this is a loop reading lines & adding them in appropriate places
        while (materialFile.good() && materialFile.peek() != EOF)
        {   // character to read
            char firstChar = materialFile.get();

            // check for eof() in case we've run out
            if (materialFile.eof())
                break;

            // otherwise, switch on the character we read
            switch (firstChar)
            { // switch on first character
            case '#':       
            {   // comment line:
                // read and discard the line
                materialFile.getline(readBuffer, MAXIMUM_LINE_LENGTH);
                break;
            }

            case 'm':
            {
                // possible maps (* = supported):
                // map_Ka map_Kd* map_Ks map_Ns map_d
                // disp decal bump* - called map_Bump* ...
                // ... default output from Blender

                materialFile.get(); // a
                materialFile.get(); // p
                materialFile.get(); // _

                MaterialProperties::TEXTURE textype = MaterialProperties::TEXTURE::DIFFUSE;

                char secondChar = materialFile.get();

                // bail if we ran out of file
                if (materialFile.eof())
                    break;

                // now use the second character to choose branch
                switch (secondChar)
                { // switch on second character
                case 'B':
                { // map_Bump
                    textype = MaterialProperties::TEXTURE::NORMAL;
                    // if a normal texture is provided, switch shader to use normal map:
                    this->setShader("../Sandbox/res/Shaders/textured_normal.shader");
                    break;
                } // map_Bump
                case 'K': 
                { // map_Kd
                    textype = MaterialProperties::TEXTURE::DIFFUSE;
                    break;//
                } // map_Kd
                default:
                    break;
                } // switch on second character


                std::string temp;
                std::stringstream ss;
                materialFile.getline(readBuffer, MAXIMUM_LINE_LENGTH);
                ss << readBuffer;

                ss >> temp; // ignore the rest of the keyword
                ss >> temp; // write the name of the texture file

                Aurora::ResourceManager::GetInstance().loadResource<Texture>(temp);
                auto texptr = Aurora::ResourceManager::GetInstance().getResource(temp);

                // 
                for (unsigned i = 0; i < m_textures.size(); i++)
                {
                    if (m_textures[i]->m_type == textype)
                    {
                        // get path before erasing the pointer:
                        std::string unload_path = m_textures[i]->getPath();
                        if (m_textures[i]->m_instances == 0)
                        {
                            Aurora::ResourceManager::GetInstance().unloadResource(unload_path);
                        }
                        m_textures[i]->removeInstance();
                        m_textures.erase(m_textures.begin() + i);
                    }
                }

                if (nullptr != texptr)
                {// make sure the file path is loaded in ...
                 // ... and available in the resource-path map
                    Texture* texture = static_cast<Texture*>(texptr.get());
                    texture->setTexturePath(temp);
                    texture->setType(textype);
                    texture->addInstance();
                    // For now, keep the texture properties default when loading
                    //texture->setFilter(properties.texture_filter);
                    //texture->setWrapping(properties.texture_wrapping);
                    //texture->setTextureScaleU(properties.tex_width);
                    //texture->setTextureScaleV(properties.tex_height);

                    m_textures.push_back(texture);
                }

                // generate all textures on OpenGL associated with this object:
                for (unsigned i = 0; i < m_textures.size(); i++)
                {
                    m_textures[i]->generate();
                }

                break;
            }

            case 'n':
            { // newmtl
                std::string temp;
                std::stringstream ss;
                materialFile.getline(readBuffer, MAXIMUM_LINE_LENGTH);
                ss << readBuffer;

                ss >> temp; // ignore the newmtl keyword
                ss >> temp; // write the name or the material file

                this->m_name = temp;

                break;
            } // newmtl
            case 'd':
            { // opacity
                float opacity;
                materialFile >> opacity;
                this->update(Material::d, opacity);
                break;
            } // opacity
            case 'i': 
            { // illum
                std::string temp;
                int illum;
                std::stringstream ss;
                materialFile.getline(readBuffer, MAXIMUM_LINE_LENGTH);
                ss << readBuffer;

                ss >> temp; // ignore the illum keyword
                ss >> illum; // write the illum value

                float illum_fl = (float)illum;

                this->update(Material::illum, illum_fl);
                break;
            } // illum
            case 'N':      
            { // can be: Ns, Ni
                // retrieve another character
                char secondChar = materialFile.get();

                // bail if we ran out of file
                if (materialFile.eof())
                    break;

                // now use the second character to choose branch
                switch (secondChar)
                { // switch on second character
                case 's':       
                { // specular read
                    float specular;
                    materialFile >> specular;
                    this->update(Material::Ns, specular);
                    break;
                } // specular read
                case 'i':       // n indicates normal vector
                { // optical density read
                    float density;
                    materialFile >> density;
                    this->update(Material::Ni, density);
                    break;//
                } // optical density read
                default:
                    break;
                } // switch on second character
                break;
            } // some sort of vertex data

            case 'K': 
            { // can be: Ka Kd Ks Ke
                // retrieve another character
                char secondChar = materialFile.get();

                // bail if we ran out of file
                if (materialFile.eof())
                    break;

                // guaranteed to be looking for rgb or rgba:
                std::vector<float> rgb_or_rgba = { 0.f, 0.f, 0.f, 1.f };
                float value;
                int i = 0;
                materialFile.getline(readBuffer, MAXIMUM_LINE_LENGTH);
                std::stringstream ss;
                ss << readBuffer;
                while (ss >> value)
                {
                    rgb_or_rgba[i] = value;
                    i++;
                    if (i > 3) break;
                }
                glm::vec4 col_value = glm::vec4(rgb_or_rgba[0], rgb_or_rgba[1], rgb_or_rgba[2], rgb_or_rgba[3]);

                // now use the second character to choose branch
                switch (secondChar)
                { // switch on second character
                case 'a': 
                { // ambient
                    this->update(Material::Ka, col_value);
                    break;
                } // ambient
                case 'd':
                { // diffuse
                    this->update(Material::Kd, col_value);
                    break;
                } // diffuse
                case 's':
                { // specular
                    this->update(Material::Ks, col_value);
                    break;
                } // specular
                case 'e':
                { // emissive
                    this->update(Material::Ke, col_value);
                    break;
                } // emissive
                default:
                    break;
                } // switch on second character
                break;
            }

            // default processing: do nothing
            default:
                break;
            }
        }

        return true;
	}

	Material* Material::clone() const
	{
		return new Material(*this);
	}
	Material::Material()
	{
		m_name = "";
		// Load just the default diffuse checkerboard texture and no other textures:
		Aurora::ResourceManager::GetInstance().loadResource<Texture>("texdefault");
		auto texptr = Aurora::ResourceManager::GetInstance().getResource("texdefault");

		if (nullptr != texptr)
		{// make sure the file path is loaded in ...
		 // ... and available in the resource-path map
			/*m_textures.emplace_back(TextureStorage::TYPE::DIFFUSE,
									 static_cast<Texture*>(texptr.get()));			*/
			m_textures.emplace_back(static_cast<Texture*>(texptr.get()));
            m_textures.back()->addInstance();
			m_textures.back()->generate();
		}

		m_shader = new Shader("../Sandbox/res/Shaders/default.shader");

	}

	Material::Material(MaterialProperties properties)
	{
		m_data.m_diffuse_col	 = properties.base_colour;
		m_data.m_ambient_col	 = properties.ambient_colour;
		m_data.m_specular_col	 = properties.specular_colour;
		m_data.m_emission_col	 = properties.emission_colour;
		m_data.m_specular_coef	 = properties.specular_coef;
		m_data.m_optical_density = properties.optical_density;
		m_data.m_opacity		 = properties.opacity;
		m_data.m_illum			 = properties.illum;
		m_name					 = properties.name;
        m_matPath                = properties.matPath;


		// Set-up and initialize 
		for (unsigned i = 0; i < properties.asset_sources.size(); i++)
		{
			switch (properties.asset_sources[i].asset_type)
			{
			case MaterialProperties::ASSET::SHADER:
				m_shader = new Shader(properties.asset_sources[i].asset_source);
				break;

			default:
				break;
			}
		}

		// Set-up and initialize 
		for (unsigned i = 0; i < properties.texture_sources.size(); i++)
		{
			// Load the required texture:
            // Parse the path: if it's absolute, do nothing, if its relative, use the working directory from the material
            std::string path = properties.texture_sources[i].texture_source;
            if (path[1] != ':' && path[0] != '/')
            {
                // Relative path
                std::string rootPath = m_matPath;
                size_t rightSlashPos, leftSlashPos;
                rightSlashPos = rootPath.rfind('/');
                leftSlashPos = rootPath.rfind('\\');
                if (rightSlashPos == std::string::npos)
                {
                    rightSlashPos = 0;
                }
                if (leftSlashPos == std::string::npos)
                {
                    leftSlashPos = 0;
                }
                size_t rootPos = std::max(leftSlashPos, rightSlashPos);
                rootPath = rootPath.substr(0, rootPos + 1);
                path = rootPath + path;
            }
			Aurora::ResourceManager::GetInstance().loadResource<Texture>(path);
			auto texptr = Aurora::ResourceManager::GetInstance().getResource(path);

			// instead of:
			//m_textures.back()->load((properties.asset_sources[i].asset_source).c_str());
			if (nullptr != texptr)
			{// make sure the file path is loaded in ...
			 // ... and available in the resource-path map
				Texture* texture = static_cast<Texture*>(texptr.get());
				texture->setTexturePath(properties.texture_sources[i].texture_source);
				texture->setType(properties.texture_sources[i].texture_type);
				texture->setMinFilter(properties.min_filter);
				texture->setMagFilter(properties.mag_filter);
				texture->setWrapping(properties.texture_wrapping);
				texture->setTextureScaleU(properties.tex_width);
				texture->setTextureScaleV(properties.tex_height);

                texture->addInstance();

				//m_textures.emplace_back(properties.texture_sources[i].texture_type,
				//						static_cast<Texture*>(texptr.get()));
				//m_textures.emplace_back(static_cast<Texture*>(texptr.get()));
				m_textures.push_back(texture);
			}
		}

		// generate all textures on OpenGL associated with this object:
		for (unsigned i = 0; i < m_textures.size(); i++)
		{
			m_textures[i]->generate();
		}
	}
}