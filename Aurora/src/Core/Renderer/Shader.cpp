#include "aurora_pch.h"

#include "Shader.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
    
#include "Renderer.h"
#include "glad/glad.h"
#include <filesystem>

namespace Aurora
{
    Shader::Shader(const std::string& filepath)
	    : m_Filepath(filepath), m_RendererID(0)
    {
        ShaderProgramSource source = parseShader(filepath);
        m_RendererID = createShader(source.VertexSource, source.FragmentSource);
        AURORA_LOG_DEBUG("new renderId: " + std::to_string(m_RendererID));
    }

    Shader::Shader(const std::string& filepath, TYPE shader_type)
        : m_Filepath(filepath), m_RendererID(0), m_type(shader_type)
    {
        if (shader_type == Shader::COMBINED)
        {
            ShaderProgramSource source = parseShader(filepath);
            m_RendererID = createShader(source.VertexSource, source.FragmentSource);
        }

        //if (shader_type == Shader::COMPUTE)
        //{
        //    m_RendererID = createShader(source);
        //}

    }

    Shader::~Shader()
    {
        GLCall(glDeleteProgram(m_RendererID));
    }

    void Shader::bind() const
    {
        GLCall(glUseProgram(m_RendererID));
    }

    void Shader::unbind() const
    {
        GLCall(glUseProgram(0));
    }

    //void Shader::setUniform1i(const std::string& name, int v0)
    void Shader::setUniform1i(const char* name, int v0)
    {
        GLCall(glUniform1i(getUniformLocation(name), v0));
    }

    //void Shader::setUniform1f(const std::string& name, float v0)
    void Shader::setUniform1f(const char* name, float v0)
    {
        GLCall(glUniform1f(getUniformLocation(name), v0));
    }

    //void Shader::setUniform4f(const std::string& name, float v0, float v1, float v2, float v3)
    //{
    //    GLCall(glUniform4f(getUniformLocation(name), v0, v1, v2, v3));
    //}

    void Shader::setUniform3f(const char* name, float v0, float v1, float v2)
    {
        GLCall(glUniform3f(getUniformLocation(name), v0, v1, v2));
    }

    //void Shader::setUniformMat4f(const std::string& name, const glm::mat4& mat4x4)
    void Shader::setUniformMat4f(const char* name, const glm::mat4& mat4x4)
    {
        GLCall(glUniformMatrix4fv(getUniformLocation(name),
                                  /* count of matrices     */ 1,
                                  /* transpose?            */ GL_FALSE,
                                  /* pointer to first lmnt */ &mat4x4[0][0])); // mat4x4.data()));
    }

    //void Shader::setUniformRawMat4f(const std::string& name, const float* raw4x4) // const glm::mat4& mat4x4)
    //{
    //    GLCall(glUniformMatrix4fv(getUniformLocation(name),
    //                                /* count of matrices     */ 1,
    //                                /* transpose?            */ GL_FALSE,
    //                                /* pointer to first lmnt */ raw4x4));//&mat4x4[0][0]));
    //}

    // send the material data to glsl block ...
    // ... and template here allows any struct type
    template<typename T>
    void Shader::setUniformBlock(const char* name, T* data)
    {
        // 1.
        unsigned int uniformBlockIndex = glGetUniformBlockIndex(m_RendererID, name);
        glUniformBlockBinding(m_RendererID, uniformBlockIndex, 0);

        // 2.
        unsigned int uboMatrices;
        glGenBuffers(1, &uboMatrices);

        glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
        glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(T), NULL, GL_STATIC_DRAW);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);

        glBindBufferRange(GL_UNIFORM_BUFFER, 0, uboMatrices, 0, 2 * sizeof(T));

        glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(T), data);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }

    // forward declare it here to allow use template function in .cpp
    template void Shader::setUniformBlock<int>(const char* name, int* data);
    template void Shader::setUniformBlock<MaterialData>(const char* name, MaterialData* data);

    void Shader::pushTexture(unsigned textureID)
    {
        m_textureQ.push_back(textureID);
    }

    void Shader::popTexture()
    {
        m_textureQ.pop_front();
    }


    ShaderProgramSource Shader::parseShader(const std::string& filepath)
    {
        std::ifstream stream(filepath.c_str());

        if (!stream)
        {
            throw std::runtime_error("Cannot open shader file\n");
        }
        // go through the file line by line to see ...
        // ... where shaders are split up
        enum class ShaderType
        {
            NONE = -1,
            VERTEX = 0,
            FRAGMENT = 1
        };
        ShaderType type = ShaderType::NONE;
        std::string line;
        std::stringstream ss[2];

        while (getline(stream, line))
        {
            if (line.find("#shader") != std::string::npos)
            {
                // has found '#shader' in the current line
                // ... find which type of shader it is:
                if (line.find("vertex") != std::string::npos)
                {
                    // set type to 'vertex'
                    type = ShaderType::VERTEX;

                }
                else if (line.find("fragment") != std::string::npos)
                {
                    // set mode to 'fragment'
                    type = ShaderType::FRAGMENT;
                }
            }

            else if (line.find("#include") != std::string::npos)
            {
                std::string path;
                std::stringstream include_stream;
                include_stream.str(line.substr(line.find("#include") + 8));
                include_stream >> path;
                path.erase(std::remove(path.begin(), path.end(), '"'), path.end());
                path.erase(std::remove(path.begin(), path.end(), '<'), path.end());
                path.erase(std::remove(path.begin(), path.end(), '>'), path.end());
                //std::cout << path << std::endl;

                std::ifstream stream(path.c_str());

                if (!stream)
                {
                    throw std::runtime_error("Cannot open #include file\n");
                }
                std::string include_line;
                // add include code to the source code:
                while (getline(stream, include_line))
                {
                    ss[(int)type] << include_line << '\n';
                }

                //this->m_uniforms.emplace_back(std::move(ss.str()));
            }

            else
            {
                if (line.find("uniform") != std::string::npos)
                {
                    std::stringstream ss;
                    ss.str(line.substr(line.find("uniform")+7));
                    ShaderUniformLayout uniform;
                    ss >> uniform.type;
                    ss >> uniform.name;
                    //this->m_uniforms.emplace_back(std::move(ss.str()));
                    this->m_uniforms.push_back(uniform);
                }
                ss[(int)type] << line << '\n';
            }
        }
        // return vertex and fragment shaders as strings:
        return { ss[0].str(), ss[1].str() };
    }


    unsigned int Shader::compileShader(unsigned int type,
                                       const std::string& source)
    {
        unsigned int shaderID = glCreateShader(type);
        const char* src = source.c_str();

        // Set the source code in 'shader' ...
        // ... to the source code in the array of strings.
        // The number of strings in the array is specified in 'count', ...
        // ... while the 'length' = NULL assumes the entire string ...
        // ... is the source.
        glShaderSource(/* shader ID          */ shaderID,
                       /* count of src codes */ 1,
                       /* ptr to ptr of src  */ &src,
                       /* src length         */ NULL);
        glCompileShader(shaderID);

        // Query the result of the compilation:
        int compilation_result;
        // iv - types - i = int, v = vector (array/pointer)
        glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compilation_result);
        if (compilation_result == GL_FALSE)
        {
            // Shader did not compile successfully
            int length;
            glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &length);
            char* error_message = (char*)alloca(length * sizeof(char));

            glGetShaderInfoLog(shaderID, length, &length, error_message);

            std::cout << "ERROR: Failed to compile" <<
                (type == GL_VERTEX_SHADER ? "Vertex" : "Fragment") << "Shader:"
                << std::endl;
            std::cout << error_message << std::endl;

            glDeleteShader(shaderID);
            return 0;
        }

        return shaderID;
    }

    //unsigned int Shader::getUniformLocation(const std::string& name)
    unsigned int Shader::getUniformLocation(const char* name)
    {
        if (m_UniformLocationCache.find(name) != m_UniformLocationCache.end())
        {
            return m_UniformLocationCache[name];
        }
            
        GLCall(int location = glGetUniformLocation(m_RendererID, name));// .c_str()));
        if (location == -1)
        {
            AURORA_LOG_INFO("NOTE: uniform '" + std::string(name) + "' does not exist.")
        }
        // caching the location
        m_UniformLocationCache[name] = location;
        return location;
    }

    /* Provide OpenGL with our Shader Sourcecode,
       ... and OpenGL to compile it and link it, ...
       ... giving an ID back so that we can bind it. */
    unsigned int Shader::createShader(const std::string& vertexShader,
                                      const std::string& fragmentShader)
    {
        unsigned int programID = glCreateProgram();

        unsigned int vsID = compileShader(GL_VERTEX_SHADER, vertexShader);
        unsigned int fsID = compileShader(GL_FRAGMENT_SHADER, fragmentShader);

        glAttachShader(programID, vsID);
        glAttachShader(programID, fsID);

        glLinkProgram(programID);
        glValidateProgram(programID);

        // Once the shaders have been linked to the program, ...
        // ... it is safe to delete their intermediates:
        glDeleteShader(vsID);
        glDeleteShader(fsID);

        return programID;
    }

}