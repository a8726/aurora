#pragma once

#include "VertexBuffer.h"
//#include "VertexBufferLayout.h"

namespace Aurora
{
	/*  Reference:
	 *  Title: Renderer
	 *  Author: Studio Cherno
	 *  Date: 2021
	 *  Availability: https://github.com/TheCherno/Hazel/tree/master/Hazel/src/Hazel/Renderer
	 *  Type: Tutorial/Notes
	 *  Renderer System was based on and extended upon the Hazel Game Engine Renderer ...
	 *  ... includes: Renderer, VertexArray, VertexBuffer, VertexBufferLayout, IndexBuffer and Shader
	 */

	class VertexBufferLayout;

	class VertexArray
	{
	private:
		unsigned int m_RendererID;

	public:
		VertexArray();
		~VertexArray();

		void addBuffer(const VertexBuffer& vb, const VertexBufferLayout& layout);
		void bind() const;
		void unbind() const;

	};

}