#pragma once

//#include "glad/glad.h" // instead of <GL/glew.h>
#include "VertexArray.h"
#include "IndexBuffer.h"
#include "Shader.h"
#include "Primitives.h"

#include "../ResourceManager/ResourceTypes/Material.h"
#include "Core/Renderer/UniformStorage.h"

namespace Aurora
{
    /*  Reference:
     *  Title: Renderer
     *  Author: Studio Cherno
     *  Date: 2021
     *  Availability: https://github.com/TheCherno/Hazel/tree/master/Hazel/src/Hazel/Renderer
     *  Type: Tutorial/Notes
     *  Renderer System was based on and extended upon the Hazel Game Engine Renderer ...
     *  ... includes: Renderer, VertexArray, VertexBuffer, VertexBufferLayout, IndexBuffer and Shader
     */

    // Visual Studio can insert a break point for us ...
    // ... so that when compilation fails, ...
    // ... it automatically takes us ... 
    // ... to the place in code where it failed!
    #define ASSERT(x) if (!(x)) __debugbreak();

    // We wrap our OpenGL function calls ...
    // ... so that we are guaranteed to get ...
    // ... an error message rather than ...
    // ... a pure blank black screen ...
    // ... with no explanation
    #define GLCall(x) GLClearError();\
        x;\
        ASSERT(GLFunctionLog(#x, __FILE__, __LINE__))

    void GLClearError();
    bool GLFunctionLog(const char* function, const char* file, int line);

    // set variant visit with function callbacks:
    // reference: https://en.cppreference.com/w/cpp/utility/variant/visit
    using var_t = std::variant<int, float, glm::mat4>;
    template<class> inline constexpr bool always_false_v = false;

    template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
    template<class... Ts> overloaded(Ts...)->overloaded<Ts...>;

    class Renderer
    {
    public:
        Renderer();
        void initFramebuffer();
        void resizeFramebuffer(uint32_t new_width, uint32_t new_height);
        void clear() const;
        void setDrawingMode(int mode);
        //void draw(const VertexArray& va, const IndexBuffer& ib, Shader& shader) const;
        //void draw(const VertexArray& va, const IndexBuffer& ib, Material& material) const;
        void draw(const VertexArray& va, const IndexBuffer& ib,
            Material& material, UniformStorage& us);// const;

        void processUniform(Uniform& us, Shader& shader_instance);// const;


        float m_current_x = 0.f;
        float m_current_y = 0.f;
        float m_current_z = 0.f;

        uint32_t m_RendererID;
        uint32_t m_ColorAttachment, m_DepthAttachment;

        uint32_t m_framebuffer_width, m_framebuffer_height;

    private:
        int m_drawing_mode; // GL_POLYGON, GL_POINTS, GL_TRIANGLEs ... 

    };
}

