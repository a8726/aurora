#include "aurora_pch.h"
#include "Sphere.h"

#include "../Core/ResourceManager/ResourceTypes/Texture.h"
#include "Framework/SampleECS/System/RenderSystem/RenderSystem.h"
#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"
#include "Core/ResourceManager/ResourceManager.h"

#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "Core/ResourceManager/ResourceTypes/Material.h"

#include "Core/Renderer/openglMacros.h"

Aurora::Sphere::Sphere()
{
}

Aurora::Sphere::~Sphere()
{
}

Aurora::Sphere::Sphere(PrimitiveObjectProperties properties)
{
	// Get vertex layout (what components the vertex holds ...
	// ... in addition to position (colour, normal, etc...)
	VertexBufferLayout layout = PrimitiveObject::setVertexBufferLayout();
	// Generate the Primitive surface itself:
	m_primitive_surface = this->generateSurface(properties);

	m_vertex_array = new VertexArray();
	// passing parameters: size, data
	m_vertex_buffer = new VertexBuffer(&m_primitive_surface.point_vector[0],
		m_primitive_surface.point_vector.size() * sizeof(float));

	// add the vertex buffer we obtain from the Surface points ...
	// ... and also the associated layout ...
	// ... to our VertexArray:
	m_vertex_array->addBuffer(*m_vertex_buffer, layout);

	// passing parameters: data, count
	m_index_buffer = new IndexBuffer(&m_primitive_surface.index_vector[0],
		m_primitive_surface.index_vector.size());
}

void Aurora::Sphere::render()
{
}

/*  Reference:
 *  Title: OpenGL Sphere
 *  Author: Song Ho Ahn
 *  Date: 2018 - 2021
 *  Availability: http://www.songho.ca/opengl/gl_sphere.html
 *  Type: Tutorial/Notes
 *  Code adapted to write to Surface instance, ...
 ...variable names changed to fit the code-style of other classes
 */
Surface Aurora::Sphere::generateSurface(PrimitiveObjectProperties properties)
{   
    // Sphere Vertices are generated in a triangle strip
    render_mode = GL_TRIANGLE_STRIP;

    constexpr float PI = glm::pi<float>(); 
    float sector_step = 2 * PI / properties.sector_count;
    float stack_step = PI / properties.stack_count;
    float sector_angle, stack_angle;
    float base_xy;                              
    float length_inv = 1.0f / properties.radius;
    glm::vec3 pos = glm::vec3(0.f);

    for (uint32_t i = 0; i <= properties.stack_count; i++)
    {
        // starting from pi/2 to -pi/2
        stack_angle = PI / 2 - i * stack_step;
        // at each stack we draw a circle ...
        // ... so the base r*cos(u) is constant ...
        // ... for the inner loop:
        base_xy = properties.radius * cosf(stack_angle); // r * cos(u)
        // for a particular stack, z is constant:
        pos.z = properties.radius * sinf(stack_angle); // r * sin(u)

        // the first and last vertices have same position and normal, but different tex coords
        for (uint32_t j = 0; j <= properties.sector_count; j++)
        {
            sector_angle = j * sector_step; // starting from 0 to 2pi

            // vertex position (x, y, z)
            pos.x = base_xy * cosf(sector_angle);
            pos.y = base_xy * sinf(sector_angle);
            this->addAttributeData(pos);
            // vertex normal
            this->addAttributeData(pos * length_inv);
            // vertex tangent
            this->addAttributeData(-sinf(sector_angle), 0.f, cosf(sector_angle));
            // vertex texture coordinates UV
            this->addAttributeData((float)(i) / (properties.sector_count),
                           (float)(j) / (properties.stack_count));
        }
    }

    uint32_t k1, k2;
    for (uint32_t stack_num = 0; stack_num <= properties.stack_count; stack_num++)
    {
        // beginning of current stack
        k1 = stack_num * (properties.sector_count + 1);     
        // beginning of next stack
        k2 = k1 + properties.sector_count + 1;      

        for (uint32_t j = 0; j <= properties.sector_count; j++, k1++, k2++)
        {
            // 2 triangles per sector excluding first and last stacks
            // k1 => k2 => k1+1
            if (stack_num != 0) { this->addTriangle(k1, k2, k1 + 1); }
            // k1+1 => k2 => k2+1
            if (stack_num != (properties.stack_count - 1)) {
                this->addTriangle(k1 + 1, k2, k2 + 1); }
            // store indices for lines
            this->addLine(k1, k2);
            // horizontal lines except 1st stack, k1 => k+1
            if (stack_num != 0) { this->addLine(k1, k1 + 1); }
        }
    }

    return surface_data;
}

