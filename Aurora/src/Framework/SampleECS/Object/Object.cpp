#include "aurora_pch.h"
#include "Object.h"

namespace Aurora
{
	void Object::Attach(ComponentBase* comp)
	{
		comps.push_back(comp);
		comp->OnAttach(this);
		comp->OnStart(this);

		if (comp->getValid())
		{
			comp->OnEnable(this);
		}
	}

	void Object::Detach(ComponentBase* comp)
	{
		comp->OnDisable(this);
		comp->OnShutdown(this);

		comps.remove(comp);
	}

	ComponentBase* Object::getComp(unsigned int compIndex)
	{
		int count = 0;
		for each (auto var in comps)
		{
			if (compIndex == count++)
			{
				return var;
			}
		}
	}

	void Object::update()
	{
		for (auto it = comps.begin(); it != comps.end(); it++)
		{
			if ((*it)->getValid())
			{
				(*it)->Update(this);
			}
		}
	}
}
