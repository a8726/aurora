#include "aurora_pch.h"
#include "CameraMoveTest.h"

#include "aurora_pch.h"
#include "ObjMoveTest.h"
#include "Framework/SampleECS/Object/Object.h"
#include "Framework/SampleECS/System/RenderSystem/RenderSystem.h"
#include "Framework/InputSystem/InputSystem.h"
#include "Framework/EventSystem/EventSystem.h"
#include "Framework/SampleECS/Component/DefaultComponents/Camera.h"

void CameraMoveTest::OnEnable(Aurora::Object* obj)
{
}

void CameraMoveTest::OnDisable(Aurora::Object* obj)
{
}

void CameraMoveTest::OnStart(Aurora::Object* obj)
{
	moveSpeed = 0.5f;
	transform = obj->GetComponent<Aurora::Transform>(0);

	Aurora::Camera* cam = obj->GetComponent<Aurora::Camera>(1);
	Aurora::RenderSystem::GetInstance().SetMainCamera(cam);

	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_W_Press, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::keyWKeepCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_W_Keep, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::keyWKeepCallback));

	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_A_Press, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::keyAKeepCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_A_Keep, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::keyAKeepCallback));

	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_S_Press, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::keySKeepCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_S_Keep, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::keySKeepCallback));

	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_D_Press, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::keyDKeepCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_D_Keep, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::keyDKeepCallback));

	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_Q_Press, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::keyQKeepCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_Q_Keep, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::keyQKeepCallback));

	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_E_Press, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::keyEKeepCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Key_E_Keep, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::keyEKeepCallback));
	

	Aurora::InputSystem::GetInstance().RegisterInputCode(Mouse_RIGHT_Press, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::mouseRightClickCallback));
	Aurora::InputSystem::GetInstance().RegisterInputCode(Mouse_RIGHT_Keep, Aurora::NewEventCallback<CameraMoveTest>(this, &CameraMoveTest::mouseRightKeepCallback));

}

void CameraMoveTest::Update(Aurora::Object* obj)
{
}

void CameraMoveTest::OnShutdown(Aurora::Object* obj)
{
}

void CameraMoveTest::keyWKeepCallback()
{
	transform->Position() += transform->Front() * moveSpeed * 0.15f;
}

void CameraMoveTest::keyAKeepCallback()
{
	transform->Position() += transform->Right() * -moveSpeed * 0.15f;
}

void CameraMoveTest::keySKeepCallback()
{
	transform->Position() += transform->Front() * -moveSpeed * 0.15f;
}

void CameraMoveTest::keyDKeepCallback()
{
	transform->Position() += transform->Right() * moveSpeed * 0.15f;
}

void CameraMoveTest::keyQKeepCallback()
{
	transform->Position() += transform->Up() * moveSpeed * 0.15f;
}


void CameraMoveTest::keyEKeepCallback()
{
	transform->Position() += transform->Up() * -moveSpeed * 0.15f;
}

void CameraMoveTest::mouseRightClickCallback(Aurora::Response* mouse)
{
	double xpos, ypos;
	Aurora::MouseInteractionResponse* res = static_cast<Aurora::MouseInteractionResponse*>(mouse);
	res->getPos(&xpos, &ypos);
	// Remember where the mouse was clicked ...
	// ... so that the view does not jump ...
	// ... when mouse is moved and camera is rotated
	m_mouseX = xpos;
	m_mouseY = ypos;
}

void CameraMoveTest::mouseRightKeepCallback(Aurora::Response* mouse)
{
	// do the logic here is enough
	double xpos, ypos;
	Aurora::MouseInteractionResponse* res = static_cast<Aurora::MouseInteractionResponse*>(mouse);
	res->getPos(&xpos, &ypos);

	float MouseSensitivity = 0.1f;

	float xoffset = float(m_mouseX) - (float)xpos;
	float yoffset = float(m_mouseY) - (float)ypos;

	xoffset *= MouseSensitivity;
	yoffset *= MouseSensitivity;

	transform->Rotation().x += yoffset; // Yaw
	transform->Rotation().y += xoffset; // Pitch

	m_mouseX = xpos;
	m_mouseY = ypos;

	//mouse
}
