#pragma once
#include "Framework/SampleECS/Object/Object.h"
#include "PrimitiveObjectProperties.h"
#include "Core/Renderer/Primitives.h"
#include "Core/Renderer/Renderer.h"
#include "Core//Renderer/VertexBufferLayout.h"

#include "PrimitiveObject.h"

namespace Aurora
{
    class Sphere : public PrimitiveObject
    {
	public:

		Sphere();
		~Sphere();
		Sphere(PrimitiveObjectProperties properties);
		// Implement Virtual Function getTypeDes() from Parent Class:
		const char* getTypeDes() override { return "Sphere"; }

		Surface		m_primitive_surface;

	protected:
		// Implement Virtual Function render() from Parent Class:
		void render() override;
		Surface generateSurface(PrimitiveObjectProperties properties) override;

    };
}
