#pragma once

#include "GLFW/glfw3.h"
#include "Core/Window.h"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "Framework/EngineGuiSystem/EngineGuiSystem.h"
#include "Framework/DebugTools/MemoryTracker.h"

#include "Framework/InputSystem/InputSystem.h"
#include "Framework/EventSystem/EventSystem.h"
#include "Framework/EngineGuiSystem/EngineGuiSystem.h"
#include "Framework/SampleECS/System/Global.h"

#include "Framework/I18N/I18N.h"
#include "Core/Log.h"

// include the renderer object (added 2022-02-21)
#include "Core/Renderer/Renderer.h"
#include "Core/StaticReflection.h"

namespace Aurora
{

	class WindowsWindow : public Aurora::Window
	{
	public:
		WindowsWindow(const WindowInitProps& props);

		virtual ~WindowsWindow() = default;
		unsigned m_width, m_height;

		GLFWwindow* getGlfwWindowHandle() { return m_glfwWindow; }
		
	private:
		GLFWwindow* m_glfwWindow;
		EngineGuiSystem m_engineGUI;
		bool show_panel_example = true;
		ImGuiIO io;

		void init(const WindowInitProps& props);
		void controllerInit();

		ImGuiIO imguiInit();

		void renderAPIHintSet();
		void imguiLogic();
		void gameLogic();
		void rendererRender();
		void imguiRender(ImGuiIO io);
		void imguiShutdown();

		
	};
}

