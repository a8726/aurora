#pragma once

#include <vector>
#include "Renderer.h"
//#include "glad/glad.h" // instead of <GL/glew.h>
//#include "../thirdparty/Glad/include/glad/glad.h"


namespace Aurora
{
	/*  Reference:
	 *  Title: Renderer
	 *  Author: Studio Cherno
	 *  Date: 2021
	 *  Availability: https://github.com/TheCherno/Hazel/tree/master/Hazel/src/Hazel/Renderer
	 *  Type: Tutorial/Notes
	 *  Renderer System was based on and extended upon the Hazel Game Engine Renderer ...
	 *  ... includes: Renderer, VertexArray, VertexBuffer, VertexBufferLayout, IndexBuffer and Shader
	 */

	struct VertexBufferElement
	{	
		#define GL_FLOAT 0x1406
		#define GL_UNSIGNED_BYTE 0x1401
		#define GL_UNSIGNED_INT 0x1405

		#define GL_FALSE 0
		#define GL_TRUE 1

		unsigned int type;
		unsigned int count;
		unsigned char normalized;

		static unsigned int getSizeOfType(unsigned int type)
		{
			switch (type)
			{
				case GL_FLOAT:			return 4;
				//case 0x1406:			return 4;
				case GL_UNSIGNED_INT:	return 4;
				//case 0x1405:	return 4;
				case GL_UNSIGNED_BYTE:	return 1;
				//case 0x1401:	return 1;
			}       
			ASSERT(false);
			return 0;
		}
	};

	class VertexBufferLayout
	{
	private:
		std::vector<VertexBufferElement> m_Elements;
		unsigned int m_Stride;

	public:
		VertexBufferLayout()
			: m_Stride(0)
		{

		}

		template<typename T>
		void push(unsigned int count)
		{
			static_assert(false);
		}
		template<>
		void push<float>(unsigned int count)
		{
			m_Elements.push_back( {GL_FLOAT, count, GL_FALSE} );
			m_Stride += count * VertexBufferElement::getSizeOfType(GL_FLOAT); //sizeof(GLfloat);
		}
		template<>
		void push<unsigned int>(unsigned int count)
		{
			m_Elements.push_back({ GL_UNSIGNED_INT, count, GL_FALSE });
			m_Stride += count * VertexBufferElement::getSizeOfType(GL_UNSIGNED_INT); //sizeof(GLuint);
		}
		template<>
		//bytes
		void push<unsigned char>(unsigned int count)
		{
			m_Elements.push_back({ GL_UNSIGNED_BYTE, count, GL_TRUE });
			m_Stride += count * VertexBufferElement::getSizeOfType(GL_UNSIGNED_BYTE); //sizeof(GLbyte);
		}

		// a way to get stride:
		inline unsigned int getStride() const { return m_Stride; }
		inline const std::vector<VertexBufferElement> getElements() const
		{
			return m_Elements;
		};
	
	
	};

}
