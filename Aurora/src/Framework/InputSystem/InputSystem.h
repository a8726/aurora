#pragma once
#include "InputKeyCode.h"
#include "Core/Window.h"
#include "GLFW/glfw3.h"

#include "Framework/EventSystem/EventSystem.h"
#include "Core/Singleton.h"
#include "Core/Application.h"

#include "Platform/Windows/WindowsWindow.h"

namespace Aurora
{

	class MouseInteractionResponse : public EventResponseTypeBase
	{
	public:
		MouseInteractionResponse(double _posX, double _posY) :
			posX(_posX), posY(_posY) {};

		void getPos(double* posX_, double* posY_)
		{
			*posX_ = posX;
			*posY_ = posY;
		}

	private:
		double posX;
		double posY;
	};

	class InputSystem : public Singleton<InputSystem>
	{
	public:
		~InputSystem() = default;

		void init(GLFWwindow* _window);
		void TriggerKeep();

		uint64 RegisterInputCode(keycode keyCode, Closure* callback);


		void SetKeyKeep(keycode key, bool keep);
		void SetMouseKeep(keycode key, bool keep);
	private:
		std::map<keycode, bool> keyKeep;
		std::map<keycode, bool> mouseKeep;

		GLFWwindow* window;
	};

}

