#pragma once

#include "Singleton.h"

namespace Aurora
{

	class Time : public Singleton<Time>
	{
	public:
		void init();
		void update();

		time_t GetSystemTime();
		time_t GetRuntimeTime();

		time_t GetDeltaTime();
		time_t GetFixedDeltaTime();


	private:
		time_t startFrom;
		time_t lastFrameDeltaTime;
		time_t fixedDeltaTime;
	};

}