#include "aurora_pch.h"
#include "glad/glad.h"

#include "MotionSystem.h"
#include "ozz/base/io/archive.h"
#include "ozz/base/io/stream.h"
//#include "Core/ResourceManager/ResourceTypes/OzzAnimation.h"
//#include "Core/ResourceManager/ResourceTypes/OzzSkeleton.h"
#include "ozz/base/maths/simd_math.h"
#include "ozz/base/maths/soa_transform.h"
#include "ozz/base/maths/vec_float.h"
#include "ozz/animation/runtime/blending_job.h"
#include "ozz/geometry/runtime/skinning_job.h"
#include "ozz/mesh.h"
#include "ozz/renderer.h"


#include "Functions/exampleObj.h"
#include "Framework/SampleECS/Object/PrimitiveObjects/PrimitiveObject.h"
#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"
#include "Framework/SampleECS/Component/DefaultComponents/Camera.h"
#include "Framework/MotionSystem/ScratchBuffer.h"
#include "Core/Singleton.h"
#include "Core/ResourceManager/ResourceManager.h"
#include "Framework\SampleECS\System\Global.h"
#include "../thirdparty/glm/glm.hpp"


namespace Aurora
{
	MotionSystem::MotionSystem() :
		time_ratio(0.f),
		previous_time_ratio(0.f),
		blend_ratio(0.0f),
		playback_speed(1.f),
		threshold_(0.1f)
	{}

	const float kDefaultUVsArray[][2] = {
	{0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f},
	{0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f},
	{0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f},
	{0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f},
	{0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f},
	{0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f},
	{0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f},
	{0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f},
	{0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f},
	{0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f},
	{0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.f} };

	const uint8_t kDefaultColorsArray[][4] = {
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255},
	{255, 255, 255, 255}, {255, 255, 255, 255}, {255, 255, 255, 255} };

	void MotionSystem::test()
	{
		AURORA_LOG_INFO("Motion System test success!");
	}

	bool MotionSystem::loadSkeleton(const char* _filename, ozz::animation::Skeleton* _skeleton)
	{
		//assert(_filename);

		ozz::io::File file(_filename, "rb");

		std::string path(_filename);

		if (!file.opened()) {
			AURORA_LOG_ERROR("Failed to open skeleton file " + path + ".");
			return false;
		}

		ozz::io::IArchive archive(&file);

		if (!archive.TestTag<ozz::animation::Skeleton>()) {
			AURORA_LOG_ERROR("Failed to load skeleton instance from file " + path + ".");
			return false;
		}

		// Once the tag is validated, reading cannot fail.
		archive >> *_skeleton;

		return true;
	}

	void MotionSystem::PlayIdleAndWalk(Object* obj, float blender_ratio)
	{
		drawBlendingJob(obj, 1, 2, blender_ratio);
	}

	void MotionSystem::PlayIdleAndSit(Object* obj, float blender_ratio)
	{
		drawBlendingJob(obj, 1, 3, blender_ratio, false, true);
	}

	void MotionSystem::PlayClick(Object* obj)
	{
		drawJoint(obj, 0);
	}

	void MotionSystem::StopAnim()
	{
		isStoped = true;
	}

	void MotionSystem::StartAnim()
	{
		isStoped = true;
	}

	void MotionSystem::SetTimeRatio(float time_ratio)
	{
		this->time_ratio = time_ratio;
	}

	bool MotionSystem::loadAnimation(const char* _filename, ozz::animation::Animation* _animation)
	{

		ozz::io::File file(_filename, "rb");

		std::string path(_filename);

		if (!file.opened())
		{
			AURORA_LOG_ERROR("Failed to open animation file " + path + ".");
			return false;
		}

		ozz::io::IArchive archive(&file);

		if (!archive.TestTag<ozz::animation::Animation>())
		{
			AURORA_LOG_ERROR("Failed to load animation instance from file " + path + ".");
			return false;
		}

		// Once the tag is validated, reading cannot fail.
		archive >> *_animation;

		return true;
	}
	bool MotionSystem::LoadMeshes(const char* _filename, ozz::vector<ozz::sample::Mesh>* _meshes) {
		//assert(_filename && _meshes);

		ozz::io::File file(_filename, "rb");

		std::string path(_filename);

		if (!file.opened()) {
			AURORA_LOG_ERROR("Failed to open mesh file " + path + ".");
			return false;
		}
		ozz::io::IArchive archive(&file);

		while (archive.TestTag<ozz::sample::Mesh>()) {
			_meshes->resize(_meshes->size() + 1);
			archive >> _meshes->back();
		}

		return true;
	}


	bool MotionSystem::loadAnimation(const char* _filename)
	{
		return false;
	}

	bool MotionSystem::initialize()
	{
		if (!loadSkeleton(fbx_skeletion.c_str(), &skeleton_) ||
			!LoadMeshes(fbx_mesh.c_str(), &meshes_))
		{
			return false;
		}

		locals_.resize(skeleton_.num_soa_joints());
		models_.resize(skeleton_.num_joints());
		context_.Resize(skeleton_.num_joints());
		joint_buffer.resize(skeleton_.num_joints());

		samplers.resize(4);
		for (int i = 0; i < samplers.size(); i++)
		{
			samplers[i] = new Sampler();
			samplers[i]->context.Resize(skeleton_.num_joints());
			samplers[i]->locals.resize(skeleton_.num_soa_joints());
			samplers[i]->speed = 1.f;
		}

		loadAnimation(anim_click_path.c_str(), &samplers[0]->animation);
		loadAnimation(anim_idle_path.c_str(), &samplers[1]->animation);
		loadAnimation(anim_walk_path.c_str(), &samplers[2]->animation);
		loadAnimation(anim_sit_path.c_str(), &samplers[3]->animation);
		//loadAnimation(default_file_a_3.c_str(), &samplers[3]->animation);

		// Computes the number of skinning matrices required to skin all meshes.
		// A mesh is skinned by only a subset of joints, so the number of skinning
		// matrices might be less that the number of skeleton joints.
		// Mesh::joint_remaps is used to know how to order skinning matrices. So
		// the number of matrices required is the size of joint_remaps.
		size_t num_skinning_matrices = 0;
		for (const ozz::sample::Mesh& mesh : meshes_) {
			/*num_skinning_matrices =
				ozz::math::Max(num_skinning_matrices, mesh.joint_remaps.size());*/
			if (num_skinning_matrices >= mesh.joint_remaps.size())
			{
				num_skinning_matrices = num_skinning_matrices;
			}
			else {
				num_skinning_matrices = mesh.joint_remaps.size();
			}
		}

		// Allocates skinning matrices.
		skinning_matrices_.resize(num_skinning_matrices);
		mesh_material_properties.base_colour = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
		mesh_material_properties.asset_sources = {
			//{Aurora::MaterialProperties::ASSET::SHADER, "../Sandbox/res/Shaders/spinning.shader"},
			{Aurora::MaterialProperties::ASSET::SHADER, "./res/Shaders/textured.shader"},
		};

		// Check the skeleton matches with the mesh, especially that the mesh
		// doesn't expect more joints than the skeleton has.
		for (const ozz::sample::Mesh& mesh : meshes_) {
			if (skeleton_.num_joints() < mesh.highest_joint_index()) {
				//ozz::log::Err() << "The provided mesh doesn't match skeleton "
				//	"(joint count mismatch)."
				//	<< std::endl;
				return false;
			}
		}

		// TURN OFF JOINTS / SPHERES
		//PrimitiveObjectProperties sphere_test_prop;
		//sphere_test_prop.radius = 0.05f;

		//Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/pink.mtl");
		//auto matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/pink.mtl");
		//Material* sphere_material = static_cast<Material*>(matptr.get());
		//sphere_material->setTextureMinFilter(GL_LINEAR_MIPMAP_LINEAR);
		//sphere_material->setTextureMagFilter(GL_LINEAR);

		//for (int i = 0; i < skeleton_.num_joints(); i++)
		//{
		//	joint_buffer[i] = new Sphere(sphere_test_prop);
		//	joint_buffer[i]->Attach(new Transform(glm::vec3(0, 0, 0)));
		//	joint_buffer[i]->Attach(new RendererComponent(joint_buffer[i]->getVAO(), joint_buffer[i]->getEBO(), sphere_material));
		//	Global::GetInstance().RegisterObject(joint_buffer[i]);
		//}
		return true;
	}

	bool MotionSystem::drawBlendingJob(Object* obj, int animation_num_l, int animation_num_r, float blend_ratio,
		bool isLOp, bool isROp)
	{
		if (animation_num_l >= samplers.size() || animation_num_r >= samplers.size())
		{
			AURORA_LOG_ERROR("animation Samplers don't have that animation clip id. Max samplers size: " + samplers.size());
			return false;
		}
		UpdateRuntimeParameters(animation_num_l, animation_num_r, blend_ratio);
		updateRatio(samplers[animation_num_l]->animation, samplers[animation_num_r]->animation, blend_ratio, dt, 3.5f);

		ozz::animation::SamplingJob sampling_job;
		sampling_job.animation = &samplers[animation_num_l]->animation;
		sampling_job.context = &samplers[animation_num_l]->context;
		sampling_job.ratio = isLOp ? 1 - time_ratio : time_ratio;
		sampling_job.output = make_span(samplers[animation_num_l]->locals);
		sampling_job.Run();

		sampling_job.animation = &samplers[animation_num_r]->animation;
		sampling_job.context = &samplers[animation_num_r]->context;
		sampling_job.ratio = isROp ? 1 - time_ratio : time_ratio;
		sampling_job.output = make_span(samplers[animation_num_r]->locals);
		sampling_job.Run();

		// Prepares blending layers.
		ozz::animation::BlendingJob::Layer layers[2];
		//for (int i = 0; i < 2; ++i) {
		//	layers[i].transform = make_span(samplers_[i].locals);
		//	layers[i].weight = samplers_[i].weight;
		//}
		layers[0].transform = make_span(samplers[animation_num_l]->locals);
		layers[0].weight = samplers[animation_num_l]->weight;
		layers[1].transform = make_span(samplers[animation_num_r]->locals);
		layers[1].weight = samplers[animation_num_r]->weight;

		// Setups blending job.
		ozz::animation::BlendingJob blend_job;
		blend_job.threshold = threshold_;
		blend_job.layers = layers;
		blend_job.rest_pose = skeleton_.joint_rest_poses();
		blend_job.output = make_span(locals_);

		// Blends.
		blend_job.Run();


		// Converts from local space to model space matrices.
		// Gets the output of the blending stage, and converts it to model space.

		// Setup local-to-model conversion job.
		ozz::animation::LocalToModelJob ltm_job;
		ltm_job.skeleton = &skeleton_;
		ltm_job.input = make_span(locals_);
		ltm_job.output = make_span(models_);

		ltm_job.Run();

		ozz::math::SimdFloat4 root = ozz::math::simd_float4::Load(0, 0, 0, 1);
		for (int i = 0; i < skeleton_.num_joints(); i++)
		{

			ozz::math::SimdFloat4 transform = ozz::math::TransformPoint(models_[i], root);
			float x = ozz::math::GetX(transform);
			float y = ozz::math::GetY(transform);
			float z = ozz::math::GetZ(transform);

			//Transform* transform_ = joint_buffer[i]->GetComponent<Aurora::Transform>(0);
			//transform_->Position() = glm::vec3(x, y, z);
		}

		const ozz::math::Float4x4 transform = ozz::math::Float4x4::identity();

		for (size_t i = 0; i < renders.size(); i++)
		{
			obj->Detach(renders[i]);
		}

		unsigned int index = 0;
		for (const ozz::sample::Mesh& mesh : meshes_) {
			for (size_t i = 0; i < mesh.joint_remaps.size(); ++i) {
				skinning_matrices_[i] =
					models_[mesh.joint_remaps[i]] * mesh.inverse_bind_poses[i];
			}

			renders.resize(renders.size() + 1);
			// Renders skin.
			/*success &= _renderer->DrawSkinnedMesh(
				mesh, make_span(skinning_matrices_), transform, render_options_);*/
			drawSkinnedMesh(index, obj, mesh, make_span(skinning_matrices_), transform, render_options_);
			index++;
		}
	}

	void MotionSystem::initializeBlendingJob()
	{
	}

	bool MotionSystem::drawJoint(Object* obj, int animation_id)
	{
		if (animation_id >= samplers.size())
		{
			AURORA_LOG_ERROR("animation Samplers don't have that animation clip id. Max samplers size: " + samplers.size());
			return false;
		}
		updateRatio(samplers[animation_id]->animation, samplers[animation_id]->animation, 1, dt, 3);

		ozz::animation::SamplingJob sampling_job;
		//sampling_job.animation = &animation_;
		sampling_job.animation = &samplers[animation_id]->animation;
		sampling_job.context = &samplers[animation_id]->context;
		sampling_job.ratio = time_ratio;
		sampling_job.output = make_span(samplers[animation_id]->locals);
		sampling_job.Run();

		ozz::animation::LocalToModelJob ltm_job;
		ltm_job.skeleton = &skeleton_;
		ltm_job.input = make_span(samplers[animation_id]->locals);
		ltm_job.output = make_span(models_);
		ltm_job.Run();

		Transform* tsm = obj->GetComponent<Transform>(0);
		ozz::math::Float4 test(0, 0, 0, 1);
		ozz::math::SimdFloat4 root = ozz::math::simd_float4::Load(0, 0, 0, 1);
		for (int i = 0; i < skeleton_.num_joints(); i++)
		{

			ozz::math::SimdFloat4 transform = ozz::math::TransformPoint(models_[i], root);
			float x = ozz::math::GetX(transform);
			float y = ozz::math::GetY(transform);
			float z = ozz::math::GetZ(transform);

			//Transform* transform_ = joint_buffer[i]->GetComponent<Aurora::Transform>(0);
			//transform_->Position() = glm::vec3(tsm->Position().x + x, tsm->Position().y + y, tsm->Position().z + z);
		}

		const ozz::math::Float4x4 transform = ozz::math::Float4x4::identity();
		
		for (size_t i = 0; i < renders.size(); i++)
		{
			obj->Detach(renders[i]);
		}

		unsigned int index = 0;
		for (const ozz::sample::Mesh& mesh : meshes_) {
			for (size_t i = 0; i < mesh.joint_remaps.size(); ++i) {
				skinning_matrices_[i] =
					models_[mesh.joint_remaps[i]] * mesh.inverse_bind_poses[i];
			}

			renders.resize(renders.size() + 1);
			// Renders skin.
			/*success &= _renderer->DrawSkinnedMesh(
				mesh, make_span(skinning_matrices_), transform, render_options_);*/
			drawSkinnedMesh(index, obj, mesh, make_span(skinning_matrices_), transform, render_options_);
			index++;
		}

		return true;

	}

	void MotionSystem::setTimeRatio(float ratio_)
	{
		previous_time_ratio = time_ratio;
		//if (loop_) {
		//	// Wraps in the unit interval [0:1], even for negative values (the reason
		//	// for using floorf).
		//	time_ratio_ = _ratio - floorf(_ratio);
		//}
		//else {
		//	// Clamps in the unit interval [0:1].
		//	time_ratio_ = math::Clamp(0.f, _ratio, 1.f);
		//}
		float min = ratio_ < 1.f ? ratio_ : 1.f;
		time_ratio = min < 0.f ? 0.f : min;

		if (time_ratio == 1.f)
		{
			time_ratio = 0.f;
		}
	}

	void MotionSystem::updateRatio(const ozz::animation::Animation& _animation1, const ozz::animation::Animation& _animation2, float blend_ratio, float _dt, float speed)
	{
		if (isStoped)
		{
			return;
		}

		float new_time = time_ratio;

		bool play_ = true;

		float minV = std::min(_animation1.duration(), _animation2.duration());
		float maxV = std::max(_animation1.duration(), _animation2.duration());

		float dura = maxV - minV;
		if (_animation1.duration() > _animation2.duration())
		{
			dura *= (1 - blend_ratio);
		}
		else
		{
			dura *= blend_ratio;
		}
		dura += minV;

		if (play_) {
			new_time = time_ratio + _dt * speed / dura;
		}

		// Must be called even if time doesn't change, in order to update previous
		// frame time ratio. Uses set_time_ratio function in order to update
		// previous_time_ an wrap time value in the unit interval (depending on loop
		// mode).


		setTimeRatio(new_time);
	}

	void MotionSystem::UpdateRuntimeParameters(int animation_num_l, int animation_num_r, float blend_ratio) {
		// Computes weight parameters for all samplers.
		int num[2] = { animation_num_l,animation_num_r };
		float kNumLayers = 2;
		const float kNumIntervals = kNumLayers - 1; //1
		const float kInterval = 1.f / kNumIntervals;//1
		for (int i = 0; i < kNumLayers; ++i) {//2 0,1
			const float med = i * kInterval;//0,1
			const float x = blend_ratio - med;//0.3, -0.7
			const float y = ((x < 0.f ? x : -x) + kInterval) * kNumIntervals;//-0.3 0.3
			//samplers[i]->weight = ozz::math::Max(0.f, y);//0  0.3
			if (y > 0)
			{
				samplers[num[i]]->weight = y;
			}
			else
			{
				samplers[num[i]]->weight = 0.f;
			}
		}

		// Interpolates animation durations using their respective weights, to
		// find the loop cycle duration that matches blend_ratio_.
		const float loop_duration =
			samplers[animation_num_l]->animation.duration() * samplers[animation_num_l]->weight +
			samplers[animation_num_r]->animation.duration() * samplers[animation_num_r]->weight;

		// Finally finds the speed coefficient for all samplers.
		const float inv_loop_duration = 1.f / loop_duration;

		samplers[animation_num_l]->speed = samplers[animation_num_l]->animation.duration() * inv_loop_duration;
		samplers[animation_num_r]->speed = samplers[animation_num_r]->animation.duration() * inv_loop_duration;

		//for (int i = 0; i < kNumLayers; ++i) {
		//	Sampler& sampler = samplers_[i];
		//	const float speed = sampler.animation.duration() * inv_loop_duration;
		//	sampler.controller.set_playback_speed(speed);
		//}
	}

	void MotionSystem::updateJoint()
	{
	}

	void MotionSystem::OnEnable(Object* obj)
	{
	}

	void MotionSystem::OnDisable(Object* obj)
	{
	}

	void MotionSystem::OnStart(Object* obj)
	{
		default_colors.push_back(glm::vec3(0.25f, 0.47f, 0.64f));
		default_colors.push_back(glm::vec3(0.5f, 0.5f, 0.5f));
		default_colors.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
		default_colors.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
		//default_colors.push_back(glm::vec3(1.0f, 1.0f, 1.0f));
		//default_colors.push_back(glm::vec3(1.0f, 1.0f, 1.0f));
		//default_colors.push_back(glm::vec3(1.0f, 1.0f, 1.0f));
		//default_colors.push_back(glm::vec3(1.0f, 1.0f, 1.0f));
		//default_colors.push_back(glm::vec3(1.0f, 1.0f, 1.0f));

		id = 0;

		light_pos = glm::vec3(0.8f, 0.1f, 0.f);

		initialize();
	}

	void MotionSystem::Update(Object* obj)
	{
		if (id == 0)
		{
			PlayClick(obj);
		}
		else if (id == 1)
		{
			PlayIdleAndSit(obj, blend_ratio);
		}
		else
		{
			PlayIdleAndWalk(obj, blend_ratio);
		}

		//if (test_switch == 0)
		//{
		//	blend_ratio = blend_ratio += 0.01;
		//}
		//if (blend_ratio >= 1)
		//{
		//	test_switch = 1;
		//}
		//if (test_switch == 1)
		//{
		//	blend_ratio = blend_ratio -= 0.01;
		//}
		//if (blend_ratio <= 0)
		//{
		//	test_switch = 0;
		//}
		//drawBlendingJob(1, 2);
	}

	void MotionSystem::OnShutdown(Object* obj)
	{
	}

	void MotionSystem::updateRuntimeParameters()
	{

	}

	bool MotionSystem::drawSkinnedMesh(unsigned int idx, Object* obj,
		const ozz::sample::Mesh& _mesh, const ozz::span<ozz::math::Float4x4> _skinning_matrices,
		const ozz::math::Float4x4& _transform, const ozz::sample::Renderer::Options& _options) {
		// Forward to DrawMesh function is skinning is disabled.
		//if (_options.skip_skinning || !_mesh.skinned()) {
		//  return DrawMesh(_mesh, _transform, _options);
		//}

	  //  if (_options.wireframe) {
	  //#ifndef EMSCRIPTEN
	  //    GL(PolygonMode(GL_FRONT_AND_BACK, GL_LINE));
	  //#endif  // EMSCRIPTEN
	  //  }

		const int vertex_count = _mesh.vertex_count();

		// Positions and normals are interleaved to improve caching while executing
		// skinning job.

		const GLsizei positions_offset = 0;
		const GLsizei positions_stride = sizeof(float) * 3;
		const GLsizei normals_offset = vertex_count * positions_stride;
		const GLsizei normals_stride = sizeof(float) * 3;
		const GLsizei tangents_offset =
			normals_offset + vertex_count * normals_stride;
		const GLsizei tangents_stride = sizeof(float) * 3;
		const GLsizei skinned_data_size =
			tangents_offset + vertex_count * tangents_stride;

		// Colors and uvs are contiguous. They aren't transformed, so they can be
		// directly copied from source mesh which is non-interleaved as-well.
		// Colors will be filled with white if _options.colors is false.
		// UVs will be skipped if _options.textured is false.
		const GLsizei colors_offset = skinned_data_size;
		const GLsizei colors_stride = sizeof(uint8_t) * 4;
		const GLsizei colors_size = vertex_count * colors_stride;
		const GLsizei uvs_offset = colors_offset + colors_size;
		const GLsizei uvs_stride = sizeof(float) * 2;
		const GLsizei uvs_size = vertex_count * uvs_stride;
		const GLsizei fixed_data_size = colors_size + uvs_size;

		// Reallocate vertex buffer.
		const GLsizei vbo_size = skinned_data_size + fixed_data_size;
		void* vbo_map = scratch_buffer_.Resize(vbo_size);

		// Iterate mesh parts and fills vbo.
		// Runs a skinning job per mesh part. Triangle indices are shared
		// across parts.
		size_t processed_vertex_count = 0;
		for (size_t i = 0; i < _mesh.parts.size(); ++i) {
			const ozz::sample::Mesh::Part& part = _mesh.parts[i];

			// Skip this iteration if no vertex.
			const size_t part_vertex_count = part.positions.size() / 3;
			if (part_vertex_count == 0) {
				continue;
			}

			// Fills the job.
			ozz::geometry::SkinningJob skinning_job;
			skinning_job.vertex_count = static_cast<int>(part_vertex_count);
			const int part_influences_count = part.influences_count();

			// Clamps joints influence count according to the option.
			skinning_job.influences_count = part_influences_count;

			// Setup skinning matrices, that came from the animation stage before being
			// multiplied by inverse model-space bind-pose.
			skinning_job.joint_matrices = _skinning_matrices;

			// Setup joint's indices.
			skinning_job.joint_indices = make_span(part.joint_indices);
			skinning_job.joint_indices_stride =
				sizeof(uint16_t) * part_influences_count;

			// Setup joint's weights.
			if (part_influences_count > 1) {
				skinning_job.joint_weights = make_span(part.joint_weights);
				skinning_job.joint_weights_stride =
					sizeof(float) * (part_influences_count - 1);
			}

			// Setup input positions, coming from the loaded mesh.
			skinning_job.in_positions = make_span(part.positions);
			skinning_job.in_positions_stride =
				sizeof(float) * ozz::sample::Mesh::Part::kPositionsCpnts;

			// Setup output positions, coming from the rendering output mesh buffers.
			// We need to offset the buffer every loop.
			float* out_positions_begin = reinterpret_cast<float*>(ozz::PointerStride(
				vbo_map, positions_offset + processed_vertex_count * positions_stride));
			float* out_positions_end = ozz::PointerStride(
				out_positions_begin, part_vertex_count * positions_stride);
			skinning_job.out_positions = { out_positions_begin, out_positions_end };
			skinning_job.out_positions_stride = positions_stride;

			// Setup normals if input are provided.
			float* out_normal_begin = reinterpret_cast<float*>(ozz::PointerStride(
				vbo_map, normals_offset + processed_vertex_count * normals_stride));
			float* out_normal_end = ozz::PointerStride(
				out_normal_begin, part_vertex_count * normals_stride);

			if (part.normals.size() / ozz::sample::Mesh::Part::kNormalsCpnts ==
				part_vertex_count)
			{
				// Setup input normals, coming from the loaded mesh.
				skinning_job.in_normals = make_span(part.normals);
				skinning_job.in_normals_stride =
					sizeof(float) * ozz::sample::Mesh::Part::kNormalsCpnts;

				// Setup output normals, coming from the rendering output mesh buffers.
				// We need to offset the buffer every loop.
				skinning_job.out_normals = { out_normal_begin, out_normal_end };
				skinning_job.out_normals_stride = normals_stride;
			}
			else
			{
				// Fills output with default normals.
				for (float* normal = out_normal_begin; normal < out_normal_end;
					normal = ozz::PointerStride(normal, normals_stride))
				{
					normal[0] = 0.f;
					normal[1] = 1.f;
					normal[2] = 0.f;
				}
			}

			// Setup tangents if input are provided.
			float* out_tangent_begin = reinterpret_cast<float*>(ozz::PointerStride(
				vbo_map, tangents_offset + processed_vertex_count * tangents_stride));
			float* out_tangent_end = ozz::PointerStride(
				out_tangent_begin, part_vertex_count * tangents_stride);

			if (part.tangents.size() / ozz::sample::Mesh::Part::kTangentsCpnts ==
				part_vertex_count) {
				// Setup input tangents, coming from the loaded mesh.
				skinning_job.in_tangents = make_span(part.tangents);
				skinning_job.in_tangents_stride =
					sizeof(float) * ozz::sample::Mesh::Part::kTangentsCpnts;

				// Setup output tangents, coming from the rendering output mesh buffers.
				// We need to offset the buffer every loop.
				skinning_job.out_tangents = { out_tangent_begin, out_tangent_end };
				skinning_job.out_tangents_stride = tangents_stride;
			}
			else {
				// Fills output with default tangents.
				for (float* tangent = out_tangent_begin; tangent < out_tangent_end;
					tangent = ozz::PointerStride(tangent, tangents_stride)) {
					tangent[0] = 1.f;
					tangent[1] = 0.f;
					tangent[2] = 0.f;
				}
			}

			// Execute the job, which should succeed unless a parameter is invalid.
			if (!skinning_job.Run()) {
				return false;
			}

			//Handles colors which aren't affected by skinning.
			if (part_vertex_count == part.colors.size() / ozz::sample::Mesh::Part::kColorsCpnts) {
				// Optimal path used when the right number of colors is provided.
				memcpy(
					ozz::PointerStride(
						vbo_map, colors_offset + processed_vertex_count * colors_stride),
					array_begin(part.colors), part_vertex_count * colors_stride);
			}
			else {

				for (size_t j = 0; j < part_vertex_count;
					j += OZZ_ARRAY_SIZE(kDefaultColorsArray)) {
					size_t this_loop_count = 0;
					if (OZZ_ARRAY_SIZE(kDefaultUVsArray) < (part_vertex_count - j))
					{
						this_loop_count = OZZ_ARRAY_SIZE(kDefaultUVsArray);
					}
					else {
						this_loop_count = part_vertex_count - j;
					}
					memcpy(ozz::PointerStride(
						vbo_map, colors_offset +
						(processed_vertex_count + j) * colors_stride),
						kDefaultColorsArray, colors_stride * this_loop_count);
				}
			}

			// Copies uvs which aren't affected by skinning.
			if (part_vertex_count ==
				part.uvs.size() / ozz::sample::Mesh::Part::kUVsCpnts) {
				// Optimal path used when the right number of uvs is provided.
				memcpy(ozz::PointerStride(
					vbo_map, uvs_offset + processed_vertex_count * uvs_stride),
					array_begin(part.uvs), part_vertex_count * uvs_stride);
			}
			else {
				// Un-optimal path used when the right number of uvs is not provided.
				assert(sizeof(kDefaultUVsArray[0]) == uvs_stride);
				for (size_t j = 0; j < part_vertex_count;
					j += OZZ_ARRAY_SIZE(kDefaultUVsArray)) {
					/*const size_t this_loop_count = ozz::math::Min(
						OZZ_ARRAY_SIZE(kDefaultUVsArray), part_vertex_count - j);*/

					size_t this_loop_count = 0;

					if (OZZ_ARRAY_SIZE(kDefaultUVsArray) < (part_vertex_count - j))
					{
						this_loop_count = OZZ_ARRAY_SIZE(kDefaultUVsArray);
					}
					else {
						this_loop_count = part_vertex_count - j;
					}

					memcpy(ozz::PointerStride(
						vbo_map,
						uvs_offset + (processed_vertex_count + j) * uvs_stride),
						kDefaultUVsArray, uvs_stride * this_loop_count);
				}
			}

			// Some more vertices were processed.
			processed_vertex_count += part_vertex_count;
			//std::cout << _meshes[i].triangle_indices();
		}

		std::vector<Vertex> vertex;
		vertex.resize(vertex_count);

		void* pos_map = ozz::PointerStride(vbo_map, positions_offset);
		const float* pos = static_cast<float*>(pos_map);
		for (size_t i = 0, vert_count = 0; i < vertex_count * 3; i += 3, vert_count++)
		{
			vertex[vert_count].position.x = *(pos + i);
			vertex[vert_count].position.y = *(pos + i + 1);
			vertex[vert_count].position.z = *(pos + i + 2);
		}

		void* normal_map = ozz::PointerStride(vbo_map, normals_offset);
		const float* normal = static_cast<float*>(normal_map);
		for (size_t i = 0, vert_count = 0; i < vertex_count * 3; i += 3, vert_count++)
		{
			vertex[vert_count].normal.x = *(normal + i);
			vertex[vert_count].normal.y = *(normal + i + 1);
			vertex[vert_count].normal.z = *(normal + i + 2);
		}

		void* tangent_map = ozz::PointerStride(vbo_map, tangents_offset);
		const float* tangent = static_cast<float*>(tangent_map);
		for (size_t i = 0, vert_count = 0; i < vertex_count * 3; i += 3, vert_count++)
		{
			vertex[vert_count].tangent.x = *(tangent + i);
			vertex[vert_count].tangent.y = *(tangent + i + 1);
			vertex[vert_count].tangent.z = *(tangent + i + 2);
		}

		void* color_map = ozz::PointerStride(vbo_map, colors_offset);
		const uint8_t* color = static_cast<uint8_t*>(color_map);
		for (size_t i = 0, vert_count = 0; i < vertex_count * 4; i += 4, vert_count++)
		{
			if (idx < default_colors.size())
			{
				vertex[vert_count].color = default_colors[idx];
			}
			else
			{
				vertex[vert_count].color = glm::vec3(1);
			}
			
		}

		void* uv_map = ozz::PointerStride(vbo_map, uvs_offset);
		const float* uv = static_cast<float*>(uv_map);
		for (size_t i = 0, vert_count = 0; i < vertex_count * 2; i += 2, vert_count++)
		{
			vertex[vert_count].texCoords.x = *(uv + i);
			vertex[vert_count].texCoords.y = *(uv + i + 1);
		}

		std::vector<unsigned int> index;
		for (size_t i = 0; i < _mesh.triangle_indices.size(); i++)
		{
			uint16_t a = _mesh.triangle_indices.data()[i];
			index.push_back(static_cast<unsigned int>(a));
		}

		//Aurora::ResourceManager::GetInstance().loadResource<Material>("../Sandbox/res/Materials/motion.mtl");
		//auto matptr = Aurora::ResourceManager::GetInstance().getResource("../Sandbox/res/Materials/motion.mtl");
		//Material* mat = static_cast<Material*>(matptr.get());

		Mesh* mesh_r = new Mesh(vertex, index, mesh_material_properties, true);
		RenderParameters*  paras = mesh_r->getRenderParameters();

		RendererComponent* render = new RendererComponent();
		obj->Attach(render);
		renders[renders.size() - 1] = render;

		render->SetParameters(paras);
		render->light_pos = light_pos;

		return true;
	}

	MotionSystem::~MotionSystem()
	{
		for (size_t i = 0; i < samplers.size(); i++)
		{
			delete(samplers.at(i));
		}
	}
}