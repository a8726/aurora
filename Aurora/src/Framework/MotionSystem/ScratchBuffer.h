#pragma once
namespace ozz
{
    namespace sample
    {
        class ScratchBuffer {
        public:
            ScratchBuffer();
            ~ScratchBuffer();

            // Resizes the buffer to the new size and return the memory address.
            void* Resize(size_t _size);

        private:
            void* buffer_;
            size_t size_;
        };
    }
}