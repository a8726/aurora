project "LuaJit"
	language "C++"
	cppdialect "c++17"
	architecture "x86_64"
	kind "StaticLib"
	staticruntime "off"
	
	location "%{wks.location}/build"

	targetdir (targetOutputDir .. "/%{prj.name}")
	objdir (objOutputDir .. "/%{prj.name}")

	files
	{
		"**.h",
		"**.c",
		
		"**.lua",
	}
	
	defines {
		"_CRT_SECURE_NO_DEPRECATE",
		"_CRT_STDIO_INLINE=__declspec(dllexport)__inline"
	}

	filter "configurations:Debug"
		defines "AURORA_GENERATE_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "AURORA_GENERATE_RELEASE"
		runtime "Release"
		optimize "off"