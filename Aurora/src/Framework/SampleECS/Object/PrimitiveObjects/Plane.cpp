#include "aurora_pch.h"
#include "Plane.h"

#include "../Core/ResourceManager/ResourceTypes/Texture.h"
#include "Framework/SampleECS/System/RenderSystem/RenderSystem.h"
#include "Framework/SampleECS/Component/DefaultComponents/Transform.h"
#include "Core/ResourceManager/ResourceManager.h"

#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "Core/ResourceManager/ResourceTypes/Material.h"

#define GLM_SWIZZLE
#define GLM_FORCE_SWIZZLE
#include "../thirdparty/glm/glm.hpp"
#include "../thirdparty/glm/gtc/matrix_transform.hpp"
#include "../thirdparty/glm/gtc/type_ptr.hpp"


Aurora::Plane::Plane()
{
}

Aurora::Plane::~Plane()
{
}

Aurora::Plane::Plane(PrimitiveObjectProperties properties)
{
    // Get vertex layout (what components the vertex holds ...
    // ... in addition to position (colour, normal, etc...)
	VertexBufferLayout layout = PrimitiveObject::setVertexBufferLayout();
    // Generate the Primitive surface itself:
    m_primitive_surface = this->generateSurface(properties);

	m_vertex_array = new VertexArray();
	// passing parameters: size, data
	m_vertex_buffer = new VertexBuffer(&m_primitive_surface.point_vector[0],
		m_primitive_surface.point_vector.size() * sizeof(float));

	// add the vertex buffer we obtain from the Surface points ...
	// ... and also the associated layout ...
	// ... to our VertexArray:
	m_vertex_array->addBuffer(*m_vertex_buffer, layout);

	// passing parameters: data, count
	m_index_buffer = new IndexBuffer(&m_primitive_surface.index_vector[0],
		                            m_primitive_surface.index_vector.size());
}

Surface Aurora::Plane::generatePlane(const float& plane_height, 
    const unsigned& segments1, const unsigned& segments2, 
    const float& width, const float& height, const float& tex_width, 
    const float& tex_height, const int& flat_index)
{
    PrimitiveObjectProperties temp;
    temp.plane_offset = plane_height;
    temp.segments_x = segments1;
    temp.segments_y = segments2;
    temp.width = width;
    temp.height = height;
    temp.tex_width = tex_width;
    temp.tex_height = tex_height;
    temp.plane_flat_index = flat_index;

    return this->generatePlane(temp);

}

Surface Aurora::Plane::generatePlane(PrimitiveObjectProperties properties)
{// draw a plane, ...
 // ... which is split to ...
 // ... as many triangle segments ...
 // ... as user specifies

    std::vector<float>    point_vector;
    std::vector<unsigned> index_vector;

    surface_data.point_vector.clear();
    surface_data.index_vector.clear();

    surface_data.num_points = 0; // point_count;
    surface_data.num_elements = 0;
    surface_data.num_triangles = 0;

    // normal will be constant:
    float const_normal[3] = { 0.f, 0.f, 0.f };
    // tangent will also be constant on a plane:
    float const_tangent[3] = { 0.f, 0.f, 0.f };

    const_normal[properties.plane_flat_index] = 1.f;
    const_tangent[(properties.plane_flat_index+1)%3] = 1.f;

    float start_coord[2] = { -properties.width, -properties.height };   
    //float start_coord[2] = { properties.plane_offset *4.f * properties.width, -properties.plane_offset * 4.f * properties.height };
    //float end_coord[2]   = { -properties.plane_offset * 4.f * properties.width, properties.plane_offset * 4.f * properties.height };
    float end_coord[2] = { properties.width, properties.height };

    float stepX = (end_coord[0] - start_coord[0]) / (float)properties.segments_x;
    float stepZ = (end_coord[1] - start_coord[1]) / (float)properties.segments_y;

    int num_points = 0;
    int element_count = 0;
    int index_counter = 0;

    float point1[2], point2[2], point3[2], point4[2];

    // 4 points for one face
    float pointsA[4], pointsB[4];

    glm::vec3 point;

    uint8_t offsetsA[4] = { 0, 1, 1, 0 };
    uint8_t offsetsB[4] = { 0, 0, 1, 1 };

    for (int i = 0; i < properties.segments_x; i++)
    {
        for (int j = 0; j < properties.segments_y; j++)
        {
            // generate 4 points:
            for (int k = 0; k < 4; k++)
            {
                pointsA[k] = start_coord[0] + (i + offsetsA[k]) * stepX;
                pointsB[k] = start_coord[1] + (j + offsetsB[k]) * stepZ;
            }


            // now connect the generated 4 points with triangles:
            for (int k = 0; k < 4; k++)
            {
                point = glm::vec3(properties.plane_offset, pointsA[k], pointsB[k]);
                // layout = 0 (vec 3 = 3D )
                switch (properties.plane_flat_index)
                {
                case 0:
                    point = glm::vec3(point.x, point.y, point.z);
                    break;

                case 1:
                    point = glm::vec3(point.y, point.x, point.z);
                    break;

                case 2:
                    point = glm::vec3(point.y, point.z, point.x);
                    break;
                default:
                    AURORA_LOG_ERROR("Invalid flat index given, "
                        "needs to be in range [0,2], given:"
                        + std::to_string(properties.plane_flat_index));
                    break;
                }
                // add vertex position
                this->addAttributeData(point);
                // add vertex normal
                this->addAttributeData(const_normal[0], const_normal[1], const_normal[2]);
                // add vertex tangent
                this->addAttributeData(const_tangent[0], const_tangent[1], const_tangent[2]);
                
                // adding the texture coordinates to the planes are somewhat tricker ...
                // ... since it depends on which way we want our textures to face
                // the following has been adjusted to work with the Skybox:
                float ix = (float)(i + offsetsA[k]) / properties.segments_x;
                float jy = (float)(j + offsetsB[k]) / properties.segments_y;

                float tex_coords[2] = { ix, jy };

                // layout = 2 ( vec2 = 2D )
                switch (properties.plane_flat_index)
                {
                case 0:
                    if (properties.plane_offset > 0){tex_coords[0] = jy; tex_coords[1] = 1.f - ix;}
                    else{tex_coords[0] = 1.f - jy; tex_coords[1] = 1.f - ix;}
                    break;

                case 1:
                    // top
                    if (properties.plane_offset > 0){tex_coords[0] = ix; tex_coords[1] = 1.f - jy;}
                    // bottom
                    else{}
                    break;

                case 2:
                    if (properties.plane_offset > 0){tex_coords[0] = 1.f - ix; tex_coords[1] = 1.f - jy; }
                    else { tex_coords[0] = ix; tex_coords[1] = 1.f - jy; }
                    break;

                default:
                    AURORA_LOG_ERROR("Invalid flat index given, "
                        "needs to be in range [0,2], given:"
                        + std::to_string(properties.plane_flat_index));
                    break;
                }

                this->addAttributeData(properties.tex_width * tex_coords[0],
                                       properties.tex_height* tex_coords[1]);

            }

            //this->addTriangle(num_points + 0, num_points + 1, num_points + 2);
            //this->addTriangle(num_points + 0, num_points + 3, num_points + 2);
            //this->endTriangle();
            this->addTriangle(num_points + 0, num_points + 1, num_points + 2);
            //this->endTriangle();
            this->addTriangle(num_points + 2, num_points + 0, num_points + 3);
            //this->endTriangle();

            //this->addSquare(num_points + 0, num_points + 1, num_points + 2, num_points + 3);
            //this->addSquare(num_points + 0, num_points + 3, num_points + 2, num_points + 1);
            //index_vector.push_back(num_points + 0);
            //index_vector.push_back(num_points + 1);
            //index_vector.push_back(num_points + 2);

            //index_vector.push_back(num_points + 0);
            //index_vector.push_back(num_points + 3);
            //index_vector.push_back(num_points + 2);
            

            num_points += 4;
            //element_count += 4 * 11; // 4 * 8;// 4 * 6;
            index_counter += 2;

        }
    }

    //Surface surface;

    //surface.point_vector = point_vector;
    //surface.index_vector = index_vector;
    //surface.num_points = num_points; // point_count;
    //surface.num_elements = element_count;
    //surface.num_triangles = index_counter;

    this->setNumPoints(num_points);
    //this->setNumElements(element_count);
    //this->setNumIndices(index_counter);
    return surface_data;
}

void Aurora::Plane::render()
{
	// Rendering of primitives moved to RendererComponent::Update
}

Surface Aurora::Plane::generateSurface(PrimitiveObjectProperties properties)
{
    return generatePlane(properties);
}

