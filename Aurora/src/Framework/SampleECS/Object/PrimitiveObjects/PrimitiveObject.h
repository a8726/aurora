#pragma once

#include "Framework/SampleECS/Object/Object.h"
#include "Core/Renderer/Primitives.h"
#include "Core/Renderer/Renderer.h"
#include "Core/Renderer/VertexBufferLayout.h"

#include "PrimitiveObjectProperties.h"

namespace Aurora
{
	class PrimitiveObject : public Object
	{
	public:
		VertexBufferLayout setVertexBufferLayout();
		// Each primitive object will have its own Surface generation function
		virtual Surface generateSurface(PrimitiveObjectProperties properties) = 0;

		Surface surface_data;

		VertexArray* getVAO();
		IndexBuffer* getEBO();
	protected:
		VertexArray* m_vertex_array;
		VertexBuffer* m_vertex_buffer;
		IndexBuffer* m_index_buffer;

		void addAttributeData(const glm::vec3& point3d);
		void addAttributeData(const float& x, const float& y, const float& z);
		void addAttributeData(const float& x, const float& y);
		void endTriangle();
		void addTriangle(const glm::uvec3& triangle_indices);
		void addTriangle(const float& x, const float& y, const float& z);
		void addSquare(const unsigned& x, const unsigned& y, const unsigned& z, const unsigned& w);
		void addLine(const float& x, const float& y);
		void addNumPoints(unsigned to_add);
		void setNumPoints(unsigned to_set);
		void addNumElements(unsigned to_add);
		void setNumElements(unsigned to_set);
		void addNumIndices(unsigned to_add);
		void setNumIndices(unsigned to_set);
	};

}