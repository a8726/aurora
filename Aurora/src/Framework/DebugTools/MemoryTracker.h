#pragma once

#include "Core/Singleton.h"

namespace Aurora
{
	class MemoryTracker : public Singleton<MemoryTracker>
	{
	public:
		void init();

		void Allocate(size_t size);
		void Free(size_t size);

		size_t getCurMemoryCost();
		size_t getMemoryAllocated();
		size_t getMemoryFreed();

		static bool IsInit() { return isInit; }
	private:
		static bool isInit;

		size_t memoryAllocated;
		size_t memoryFreed;
	};
}