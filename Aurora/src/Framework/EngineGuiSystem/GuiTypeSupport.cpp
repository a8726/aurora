#include "aurora_pch.h"
#include "GuiTypeSupport.h"

#include "imgui.h"
#include "imgui_internal.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

namespace Aurora
{
	void GuiTypeSupport::drawProps(int& data, const std::string& name)
	{
		ImGui::GetCurrentWindow()->DC.CursorPos.y += imguiContext->Style.ItemInnerSpacing.x / 2;
		ImGui::SetNextItemWidth(100);
		ImGui::TextDisabled(name.c_str()); ImGui::SameLine();
		ImGui::GetCurrentWindow()->DC.CursorPos.y -= imguiContext->Style.ItemInnerSpacing.x / 2;

		ImGui::PushItemWidth(-1);
		ImGui::InputInt(name.c_str(), &data);
		ImGui::PopItemWidth();
	}

	void GuiTypeSupport::drawProps(float& data, const std::string& name)
	{
		ImGui::GetCurrentWindow()->DC.CursorPos.y += imguiContext->Style.ItemInnerSpacing.x / 2;
		ImGui::TextDisabled(name.c_str()); ImGui::SameLine();
		ImGui::GetCurrentWindow()->DC.CursorPos.y -= imguiContext->Style.ItemInnerSpacing.x / 2;

		ImGui::PushItemWidth(-1);
		ImGui::DragFloat(name.c_str(), &data, 0.01f);
		ImGui::PopItemWidth();
	}

	void GuiTypeSupport::drawProps(double& data, const std::string& name)
	{
		ImGui::GetCurrentWindow()->DC.CursorPos.y += imguiContext->Style.ItemInnerSpacing.x / 2;
		ImGui::TextDisabled(name.c_str()); ImGui::SameLine();
		ImGui::GetCurrentWindow()->DC.CursorPos.y -= imguiContext->Style.ItemInnerSpacing.x / 2;

		ImGui::PushItemWidth(-1);
		ImGui::InputDouble(name.c_str(), &data);
		ImGui::PopItemWidth();
	}

	void GuiTypeSupport::drawProps(glm::vec2& vec2, const std::string& name)
	{
		ImGui::GetCurrentWindow()->DC.CursorPos.y += imguiContext->Style.ItemInnerSpacing.x / 2;
		ImGui::TextDisabled(name.c_str()); ImGui::SameLine();
		ImGui::GetCurrentWindow()->DC.CursorPos.y -= imguiContext->Style.ItemInnerSpacing.x / 2;

		float v[2] = { vec2.x, vec2.y };

		ImGui::PushItemWidth(-1);
		ImGui::InputFloat3(name.c_str(), v);
		ImGui::PopItemWidth();

		vec2.x = v[0];
		vec2.y = v[1];
	}

	void GuiTypeSupport::drawProps(glm::vec3& vec3, const std::string& name)
	{
		ImGui::GetCurrentWindow()->DC.CursorPos.y += imguiContext->Style.ItemInnerSpacing.x / 2;
		ImGui::TextDisabled(name.c_str()); ImGui::SameLine();
		ImGui::GetCurrentWindow()->DC.CursorPos.y -= imguiContext->Style.ItemInnerSpacing.x / 2;

		float v[3] = { vec3.x, vec3.y, vec3.z };

		ImGui::PushItemWidth(-1);
		
		ImGui::DragFloat3(name.c_str(), v, 0.01);
		ImGui::PopItemWidth();

		vec3.x = v[0];
		vec3.y = v[1];
		vec3.z = v[2];
	}

	void GuiTypeSupport::drawProps(glm::vec4& vec4, const std::string& name)
	{
		ImGui::GetCurrentWindow()->DC.CursorPos.y += imguiContext->Style.ItemInnerSpacing.x / 2;
		ImGui::TextDisabled(name.c_str()); ImGui::SameLine();
		ImGui::GetCurrentWindow()->DC.CursorPos.y -= imguiContext->Style.ItemInnerSpacing.x / 2;

		float v[4] = { vec4.x, vec4.y, vec4.z, vec4.w };

		ImGui::PushItemWidth(-1);
		ImGui::InputFloat4(name.c_str(), v);
		ImGui::PopItemWidth();

		vec4.x = v[0];
		vec4.y = v[1];
		vec4.z = v[2];
		vec4.w = v[3];
	}

	void GuiTypeSupport::drawProps(std::string& str, const std::string& name)
	{
		ImGui::GetCurrentWindow()->DC.CursorPos.y += imguiContext->Style.ItemInnerSpacing.x / 2;
		ImGui::TextDisabled(name.c_str()); ImGui::SameLine();
		ImGui::GetCurrentWindow()->DC.CursorPos.y -= imguiContext->Style.ItemInnerSpacing.x / 2;

		char* str_char = const_cast<char*>(str.c_str());

		ImGui::PushItemWidth(-1);
		ImGui::InputText(name.c_str(), str_char, 25);
		ImGui::PopItemWidth();

		str = *str_char;
	}

	void GuiTypeSupport::drawProps(char* str, const std::string& name)
	{
		ImGui::GetCurrentWindow()->DC.CursorPos.y += imguiContext->Style.ItemInnerSpacing.x / 2;
		ImGui::TextDisabled(name.c_str()); ImGui::SameLine();
		ImGui::GetCurrentWindow()->DC.CursorPos.y -= imguiContext->Style.ItemInnerSpacing.x / 2;

		ImGui::PushItemWidth(-1);
		ImGui::InputText(name.c_str(), str, 25);
		ImGui::PopItemWidth();
	}
}