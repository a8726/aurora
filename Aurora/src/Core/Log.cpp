#include "aurora_pch.h"
#include "log.h"

#include <spdlog/sinks/stdout_color_sinks.h>

namespace Aurora
{
	std::shared_ptr<spdlog::logger> Log::auroraLogger;
	std::shared_ptr<spdlog::logger> Log::debugLogger;

	std::shared_ptr<ImGuiTextBuffer> Log::imgui_text_buffer;
	std::shared_ptr<ImVector<int>> Log::imgui_text_lineoffsets;
	
	void Log::init()
	{
		spdlog::sink_ptr logSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();

		logSink->set_pattern("%^[%n] [%l]: %v%$");
		auroraLogger = std::make_shared<spdlog::logger>("Aurora", logSink);
		spdlog::register_logger(auroraLogger);

		auroraLogger->set_level(spdlog::level::trace);
		auroraLogger->flush_on(spdlog::level::critical);

		logSink->set_pattern("%^[%n] [%l]: %v%$");
		debugLogger = std::make_shared<spdlog::logger>("Game", logSink);
		spdlog::register_logger(debugLogger);

		debugLogger->set_level(spdlog::level::trace);
		debugLogger->flush_on(spdlog::level::critical);
	}

	void Log::init_imgui_log()
	{
		imgui_text_buffer = std::make_shared<ImGuiTextBuffer>();
		imgui_text_lineoffsets = std::make_shared<ImVector<int>>();

		ClearLog();
		
		imgui_log_init = true;
	}

	void Log::AddLog(std::string log_text)
	{
		if (imgui_log_init)
		{
			log_text.append("\n");
                
			int old_size = imgui_text_buffer->size();
			imgui_text_buffer->appendfv(log_text.data(), log_text.data() + log_text.length());
			for (int new_size = imgui_text_buffer->size(); old_size < new_size; old_size++)
				if (imgui_text_buffer->Buf[old_size] == '\n')
					imgui_text_lineoffsets->push_back(old_size + 1);
		}
	}

	void Log::ClearLog()
	{
		imgui_text_buffer->clear();
		imgui_text_lineoffsets->clear();
		imgui_text_lineoffsets->push_back(0);
	}
}
