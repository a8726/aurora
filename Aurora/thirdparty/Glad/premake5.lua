project "Glad"
	language "C"
	kind "StaticLib"
	staticruntime "off"

	location "%{wks.location}/build"

	targetdir (targetOutputDir .. "/%{prj.name}")
	objdir (objOutputDir .. "/%{prj.name}")

	includedirs
	{
		"src",
		"include",
	}
	
	files
	{
		"**.h",
		"**.c",

		"**.lua",
	}

	filter "system:windows"
		systemversion "latest"

	filter "configurations:Debug"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		runtime "Release"
		optimize "off"