project "glm"
    kind "StaticLib"
    language "C++"

	location "%{wks.location}/build"

    targetdir (targetOutputDir .. "/%{prj.name}")
	objdir (objOutputDir .. "/%{prj.name}")

    files
	{
		"**.inl",
		"**.h",
		"**.hpp",
        "**.lua",
	}

    filter "system:windows"
		systemversion "latest"
		cppdialect "C++17"
		staticruntime "On"

    filter "configurations:Debug"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		runtime "Release"
		optimize "off"
